<?php
require '/var/www/job_board/vendor/autoload.php';

$htmlPConfig = HTMLPurifier_Config::createDefault();
$htmlPConfig->set('HTML.AllowedElements', array());
$htmlPConfig->set('HTML.AllowedAttributes', array());
$htmlPConfig->set('CSS.AllowedProperties', array());
$htmlPConfig->set('AutoFormat.RemoveEmpty.RemoveNbsp', true); 
$htmlPConfig->set('Core.LexerImpl', true);
$htmlPurifier = new HTMLPurifier($htmlPConfig);

$file = file_get_contents('/var/www/job_board/job_board/app/files/jobs2.json');
$json = json_decode($file);

$ES_OBJ = new stdClass();

$pattern = '/\<\?xml([^\>\/]*)\/\>/';

$output = '';
foreach($json as $data){
	$tmp = $data->value;
	$tmp->description = preg_replace("#<p>(\s|&nbsp;|</?\s?br\s?/?>)*</?p>#",'',$htmlPurifier->purify(str_replace('Brief Posting Description', '',preg_replace($pattern, '',str_replace('*LI-WRAP-PE','',str_replace('*LI-WRAP','',str_replace('*li-wrap','',str_replace( chr( 194 ) . chr( 160 ), ' ',$data->value->description))))))));
	$output .= '{"index":{"_index":"abjb", "_type":"jobs", "_id":"'.$tmp->referencenumber.'"}}';
	$output .= "\r\n";
	$output .= print_r(json_encode($tmp), true);
	$output .= "\r\n";
}

file_put_contents('/var/www/job_board/job_board/app/files/jobs2.json',print_r($output, true));

