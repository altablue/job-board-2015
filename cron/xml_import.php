<?php
require '/var/www/job_board/vendor/autoload.php';

$xml = file_get_contents('/tmp/jobs.xml');

$company = array();
$country = array();
$check = array();
$disc = array();
$category = array();

$reader = new Sabre\Xml\Reader();
$reader->elementMap = [
    '{}job' => function($reader) {

        global $company;
        global $country;
        global $category;
        global $disc;
        global $check;

        $job = array();
        // Borrowing a parser from the KeyValue class.
        $keyValue = Sabre\Xml\Element\KeyValue::xmlDeserialize($reader);

        $precheck = false; 

        if (isset($keyValue['{}custom_fields'])){     
            foreach($keyValue['{}custom_fields'] as $custom){
                if(in_array('Altablue',$custom)){
                    if(isset($keyValue['{}country'])){
                        if($keyValue['{}country'] == 'AU'){
                            $precheck = false;
                        }else{
                            $precheck = true;
                        }
                    }
                }
            }
        }

        if($precheck):

            if (isset($keyValue['{}title'])) {
                $job['title'] = $keyValue['{}title'];
            }
            if (isset($keyValue['{}date'])) {
                $job['date'] = date('Ymd',strtotime($keyValue['{}date']));
            }
            if (isset($keyValue['{}description'])) {
                $job['description'] = $keyValue['{}description'];
            }
            if (isset($keyValue['{}referencenumber'])) {
                $job['referencenumber'] = $keyValue['{}referencenumber'];
            }
            if (isset($keyValue['{}postalcode'])) {
                $job['postalcode'] = $keyValue['{}postalcode'];
            }
            /*if (isset($keyValue['{}category'])) {
                $job['category'] = $keyValue['{}category'];
                if(!in_array($keyValue['{}category'], $check)){
                    $check[] = $keyValue['{}category'];
                    $category[] = array('name' => $keyValue['{}category'], 'value' => strtolower(stripslashes(str_replace(' ', '', $keyValue['{}category']))));
                }
            }*/
            if (isset($keyValue['{}company'])) {
                $job['company'] = $keyValue['{}company'];
            }
            if (isset($keyValue['{}custom_fields'])) {
                $job['discipline'] = $keyValue['{}custom_fields'][0]['value'];
                if(!in_array($keyValue['{}custom_fields'][0]['value'], $check)){
                    $check[] = $keyValue['{}custom_fields'][0]['value'];
                    $disc[] = array('name' => $keyValue['{}custom_fields'][0]['value'], 'value' => strtolower(stripslashes(str_replace(' ', '', $keyValue['{}custom_fields'][0]['value']))));
                }
                $job['company'] = $keyValue['{}custom_fields'][1]['value'];
                if(!in_array($keyValue['{}custom_fields'][1]['value'], $check)){
                    $check[] = $keyValue['{}custom_fields'][1]['value'];
                    $company[] = array('name' => $keyValue['{}custom_fields'][1]['value'], 'value' => strtolower(stripslashes(str_replace(' ', '', $keyValue['{}custom_fields'][1]['value']))));
                }
            }
            if (isset($keyValue['{}country'])) {
                $job['country'] = $keyValue['{}country'];
                if(!in_array($keyValue['{}country'], $check)){
                    $check[] = $keyValue['{}country'];
                    $country[] = array('name' => $keyValue['{}country'], 'value' => strtolower(stripslashes(str_replace(' ', '', $keyValue['{}country']))));
                }
            }
            if (isset($keyValue['{}city'])) {
                $job['city'] = $keyValue['{}city'];
                if(!in_array($keyValue['{}city'], $check)){
                    $check[] = $keyValue['{}city'];
                    $city[] = array('name' => $keyValue['{}city'], 'value' => strtolower(stripslashes(str_replace(' ', '', $keyValue['{}city']))));
                }
            }
            if(!empty($job['postalcode'])){
            	if(!empty($keyValue['{}country']) && strtolower($keyValue['{}country']) == 'gb'){
            		$latDat = json_decode(file_get_contents('https://api.postcodes.io/postcodes/'.str_replace(' ', '',$job['postalcode'])));
            		if(!empty($latDat->status) && $latDat->status != '404'){
            			$job['location'] = $latDat->result->latitude.','.$latDat->result->longitude;
            		}else{
    					$latDat = json_decode(file_get_contents('https://nominatim.openstreetmap.org/search?q='.$job['postalcode'].'&countrycodes='.$keyValue['{}country'].'&featuretype=city,county&format=json&bounded=1&addressdetails=1&limit=1'));
    	        		if(!empty($latDat)){
    	        			$job['location'] = $latDat[0]->lat.','.$latDat[0]->lon;
    	        		}else if(!empty($keyValue['{}city'])){
    		        		$latDat = json_decode(file_get_contents('https://nominatim.openstreetmap.org/search?q='.$keyValue['{}city'].'&countrycodes='.$keyValue['{}country'].'&featuretype=city,county&format=json&bounded=1&addressdetails=1&limit=1'));
    		        		if(!empty($latDat)){
    		        			$job['location'] = $latDat[0]->lat.','.$latDat[0]->lon;
    		        		}
    	        		}
            		}
            	}else if(!empty($keyValue['{}country'])){
            		$latDat = json_decode(file_get_contents('https://nominatim.openstreetmap.org/search?q='.$job['postalcode'].'&countrycodes='.$keyValue['{}country'].'&featuretype=city,county&format=json&bounded=1&addressdetails=1&limit=1'));
            		if(!empty($latDat)){
            			$job['location'] = $latDat[0]->lat.','.$latDat[0]->lon;
            		}else if(!empty($keyValue['{}city'])){
    	        		$latDat = json_decode(file_get_contents('https://nominatim.openstreetmap.org/search?q='.$keyValue['{}city'].'&countrycodes='.$keyValue['{}country'].'&featuretype=city,county&format=json&bounded=1&addressdetails=1&limit=1'));
    	        		if(!empty($latDat)){
    	        			$job['location'] = $latDat[0]->lat.','.$latDat[0]->lon;
    	        		}
            		}
            	}
        	}else if(!empty($keyValue['{}city'])){
        		$latDat = json_decode(file_get_contents('https://nominatim.openstreetmap.org/search?q='.$keyValue['{}city'].'&countrycodes='.$keyValue['{}country'].'&featuretype=city,county&format=json&bounded=1&addressdetails=1&limit=1'));
        		if(!empty($latDat)){
        			$job['location'] = $latDat[0]->lat.','.$latDat[0]->lon;
        		}
        	}

            return $job;
        endif;

    },
];
$reader->xml($xml);

$tmp = $reader->parse();

$tmp2 = array();
foreach($tmp['value'] as $id => $job){
    if(!empty($job['value']) && $job['name'] == '{}job'){
        $tmp2[] = $job;
    }
}

touch("/var/www/job_board/job_board/app/files/jobs2.php");
$file = fopen("/var/www/job_board/job_board/app/files/jobs2.php", "w") or die("Unable to open file!");
$txt = "";
fwrite($file, $txt);
fclose($file);

touch("/var/www/job_board/job_board/app/files/companies.php");
$file = fopen("/var/www/job_board/job_board/app/files/companies.php", "w") or die("Unable to open file!");
fwrite($file, '<?php $company = ');
fclose($file);
$pre = '<?php $company= ';
$pre .= stripslashes(var_export($company, true));
$pre .= ';';
file_put_contents('/var/www/job_board/job_board/app/files/companies.php',$pre);

touch("/var/www/job_board/job_board/app/files/categories.php");
$file = fopen("/var/www/job_board/job_board/app/files/categories.php", "w") or die("Unable to open file!");
fwrite($file, '<?php $category = ');
fclose($file);
$pre = '<?php $category= ';
$pre .= stripslashes(var_export($category, true));
$pre .= ';';
file_put_contents('/var/www/job_board/job_board/app/files/categories.php',$pre);

touch("/var/www/job_board/job_board/app/files/disciplines.php");
$file = fopen("/var/www/job_board/job_board/app/files/disciplines.php", "w") or die("Unable to open file!");
fwrite($file, '<?php $disc = ');
fclose($file);
$pre = '<?php $disc= ';
$pre .= stripslashes(var_export($disc, true));
$pre .= ';';
file_put_contents('/var/www/job_board/job_board/app/files/disciplines.php',$pre);


touch("/var/www/job_board/job_board/app/files/countries.php");
$file = fopen("/var/www/job_board/job_board/app/files/countries.php", "w") or die("Unable to open file!");
fwrite($file, '<?php $company = ');
fclose($file);
$pre = '<?php $country= ';
$pre .= stripslashes(var_export($country, true));
$pre .= ';';
file_put_contents('/var/www/job_board/job_board/app/files/countries.php',$pre);

$output = $tmp2;

$output = array_values($output);

$pattern = '/\<\?xml([^\>\/]*)\/\>/';

$htmlPConfig = HTMLPurifier_Config::createDefault();
$htmlPConfig->set('HTML.AllowedElements', array('strong', 'a', 'br','b', 'em', 'ol', 'i', 'li', 'ul', 'h1', 'h2', 'h3', 'h4', 'h5', 'hr', 'table', 'thead','tbody','tfoot','tr','td', 'p'));
$htmlPConfig->set('HTML.AllowedAttributes', array());
$htmlPConfig->set('CSS.AllowedProperties', array());

$htmlPurifier = new HTMLPurifier($htmlPConfig);

foreach($output as $num => $job){
	foreach($job['value'] as $key => $fields){
		$output[$num]['value'][$key] = preg_replace("#<p>(\s|&nbsp;|</?\s?br\s?/?>)*</?p>#",'',$htmlPurifier->purify(preg_replace($pattern, '',preg_replace('/\s+/', ' ',str_replace('·','',str_replace('*LI-WRAP-PE','',str_replace('*LI-WRAP','',str_replace('*li-wrap','',$fields))))))));
	}
}

file_put_contents('/var/www/job_board/job_board/app/files/jobs2.php',var_export($output, true));
$json = strip_tags(strip_tags(json_encode($output), '<br><br/>'), '<br \/>');
touch("/var/www/job_board/job_board/app/files/jobs2.json");
file_put_contents('/var/www/job_board/job_board/app/files/jobs2.json',print_r($json, true));

$file_data = "<?php \n ";
$file_data .= '$array =';
$file_data .= file_get_contents('/var/www/job_board/job_board/app/files/jobs2.php');
$file_data .= ";";
file_put_contents('/var/www/job_board/job_board/app/files/jobs2.php', $file_data);