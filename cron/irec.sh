#! /bin/bash

spinner()
{
    local pid=$1
    local delay=0.1
    local spinstr='|/-\'
    while [ "$(ps a | awk '{print $1}' | grep $pid)" ]; do
        local temp=${spinstr#?}
        printf " [%c]  " "$spinstr"
        local spinstr=$temp${spinstr%"$temp"}
        sleep $delay
        printf "\b\b\b\b\b\b"
    done
    printf "    \b\b\b\b"
}

curl -XPUT 'localhost:9200/_snapshot/abjb' -d '{"type": "fs", "settings": {"location": "/home/callum/global/jobsbkup", "compress": true } }'

curl -XPUT "localhost:9200/_snapshot/abjb/pre-update?wait_for_completion=true";

rm /var/www/job_board/cron/es_check.txt;
rm /tmp/jobs.xml
#mv /var/www/job_board/job_board/app/files/jobs2.php /var/www/job_board/job_board/app/files/jobs3.php;
#mv /var/www/job_board/job_board/app/files/jobs2.json /var/www/job_board/job_board/app/files/jobs3.json;

wget "http://broadcast.talemetry.com/feed/custom_fields_xml/1038" --output-document=/tmp/jobs.xml;

chmod 777 /tmp/jobs.xml;

/usr/bin/php /var/www/job_board/cron/xml_import.php 2> /dev/null &

echo -n "[generating PHP from XML] "

chmod 777 /var/www/job_board/job_board/app/files/jobs2.json

spinner $!

/usr/bin/php /var/www/job_board/cron/es_import.php 2> /dev/null &

echo '';
echo -n "[generating JSON from PHP] "
spinner $!
echo '';

#chmod 777 /var/www/job_board/cron/es_ch
#chmod 777 /var/www/job_board/job_board/app/files/jobs2.json

curl -XDELETE 'localhost:9200/abjb/'

sleep 1

curl -XPUT 'localhost:9200/abjb/' -d '{"mappings": {"jobs": {"properties": {"discipline":{"type":"string"}, "description":{"type":"string", "analyzer": "whitespace"}, "referencenumber":{"type" : "string"}, "country":{"type" : "string"}, "location":{"type":"geo_point"}, "postcode":{"type" : "string" }, "date":{"type":"date", "format": "basic_date"}, "company":{"type":"string"}, "title":{"type":"string"}, "category":{"type":"string"}, "city":{"type":"string"} } } } }'

echo "repo made"

sleep 2

curl -XPOST 'localhost:9200/_bulk' --data-binary @/var/www/job_board/job_board/app/files/jobs2.json

echo "repo import"

sleep 2

status=$(curl -s -o "/var/www/job_board/cron/es_check.txt" "localhost:9200/abjb/jobs/_count")

touch /var/www/job_board/cron/es_check.txt;

chmod 777 /var/www/job_board/cron/es_check.txt

check=$(cat /var/www/job_board/cron/es_check.txt | jq '.count');

if [ "$check" -lt 1 ]; then
	curl -XDELETE 'localhost:9200/abjb/' 
	curl -XPOST 'localhost:9200/.kibana/_close'
	curl -XPOST "localhost:9200/_snapshot/abjb/pre-update/_restore"
	curl -XPOST 'localhost:9200/.kibana/_open'
	echo "ES RESTORED"
else
	rm /var/www/job_board/job_board/app/files/jobs.php
	rm /var/www/job_board/job_board/app/files/jobs.json
	mv /var/www/job_board/job_board/app/files/jobs2.php /var/www/job_board/job_board/app/files/jobs.php
	mv /var/www/job_board/job_board/app/files/jobs2.json /var/www/job_board/job_board/app/files/jobs.json
	curl -XDELETE "localhost:9200/_snapshot/abjb/pre-update"
fi
