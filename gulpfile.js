// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var autoprefixer= require('gulp-autoprefixer');
var rename = require('gulp-rename');
var notify = require('gulp-notify');
var cache = require('gulp-cache');
var del = require('del');
var minifycss = require('gulp-minify-css');
var iconify = require('gulp-iconify');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');

// Lint Task
gulp.task('lint', function() {
    return gulp.src('job_board/public/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('job_board/public/scss/*.scss')
        .pipe(sass(
            { 
                style: 'expanded',
                errLogToConsole: false,
                onError: function(err) {
                    return notify().write(err);
                }
            }
        ))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(gulp.dest('job_board/public/css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss({ compatibility: 'ie8', noAdvanced: true }))
        .pipe(gulp.dest('job_board/public/css'))
        .pipe(notify({ message: 'Styles task complete' }));
});

// Concatenate & Minify JS
gulp.task('libscripts', function() {
    return gulp.src([
            'job_board/public/js/libs/jquery/dist/jquery.js',
            'job_board/public/js/libs/ladda/js/spin.js',
            'job_board/public/js/libs/ladda/js/ladda.js',
            'job_board/public/js/libs/bootstrap/dist/js/bootstrap.js',
            'job_board/public/js/libs/moment/min/moment-with-locales.min.js',
            'job_board/public/js/libs/angular/angular.js',
            'job_board/public/js/libs/angular-animate/angular-animate.js',
            'job_board/public/js/libs/angular-bootstrap/ui-bootstrap.js',
            'job_board/public/js/libs/angular-bootstrap/ui-bootstrap-tpls.min.js',
            'job_board/public/js/libs/angular-cookies/angular-cookies.js',
            'job_board/public/js/libs/angular-holderjs/src/holder.js',
            'job_board/public/js/libs/angular-ladda/dist/angular-ladda.js',
            'job_board/public/js/libs/angular-mocks/angular-mocks.js',
            'job_board/public/js/libs/angular-resource/angular-resource.js',
            'job_board/public/js/libs/angular-route/angular-route.js',
            'job_board/public/js/libs/angular-sanitize/angular-sanitize.js',
            'job_board/public/js/libs/angular-touch/angular-touch.js',
            'job_board/public/js/libs/angular-ui-router/release/angular-ui-router.js',
            'job_board/public/js/libs/angular-ui-select/dist/select.js',
            'job_board/public/js/libs/angular-ui-utils/ui-utils.js',
            'job_board/public/js/libs/angular-ui-utils/ui-utils-ieshiv.js',
            'job_board/public/js/libs/ng-file-upload/ng-file-upload-all.js',
            'job_board/public/js/libs/ng-file-upload/ng-file-upload-shim.js',
            'job_board/public/js/libs/ngmap/build/scripts/ng-map.js',
            'job_board/public/js/libs/angular-loading-bar/build/loading-bar.js',
            'job_board/public/js/libs/angular-moment/angular-moment.js',
            'job_board/public/js/libs/he/he.js',
            'job_board/public/js/libs/ngstorage/ngStorage.js',
            'job_board/public/js/libs/mapbox.js/mapbox.js',
            'job_board/public/js/libs/leaflet.markercluster/dist/leaflet.markercluster.js',
            'job_board/public/js/libs/iso-3166-country-codes-angular/src/iso-3166-country-codes-angular.js',
            'job_board/public/js/libs/es5-shim/es5-sham.js',
            'job_board/public/js/libs/footable/dist/footable.min.js',
            'job_board/public/js/libs/angular-footable/dist/angular-footable.js',
            'job_board/public/js/libs/rpage/responsive-paginate.js',
            'job_board/public/js/libs/webReflect.min.js'
        ])
        .pipe(jshint())
        .pipe(concat('libs.js'))
        .pipe(gulp.dest('job_board/public/js/dist'))
        .pipe(uglify().on('error', function(e) { console.log('\x07',e.message); return this.end(); }))
        .pipe(concat('libs.min.js'))
        .pipe(gulp.dest('job_board/public/js/dist'))
        .pipe(notify({ message: 'Library Scripts task complete' }));
});


gulp.task('libcss', function() {
    return gulp.src([
            'job_board/public/js/libs/bootstrap/dist/css/bootstrap.min.css',
            'job_board/public/js/libs/ladda/dist/ladda-themeless.min.css',
            'job_board/public/js/libs/angular-ui-select/dist/select.min.css',
            'job_board/public/js/libs/mapbox.js/mapbox.css',
            'job_board/public/js/libs/footable/css/footable.core.min.css',
            'job_board/public/js/libs/leaflet.markercluster/dist/MarkerCluster.css',
            'job_board/public/js/libs/leaflet.markercluster/dist/MarkerCluster.Default.css',
            'job_board/public/js/libs/angular-loading-bar/build/loading-bar.css'
        ])
        .pipe(concat('_libs.min.css'))
        .pipe(gulp.dest('job_board/public/css/'))
});

gulp.task('scripts', function() {
    return gulp.src(
            'job_board/public/js/modules/**/**.js'
        )
        .pipe(concat('all.js'))
        .pipe(gulp.dest('job_board/public/js/dist'))
        .pipe(rename('all.min.js'))
        .pipe(gulp.dest('job_board/public/js/dist'))
        .pipe(notify({ message: 'Scripts task complete' }));
});


gulp.task('images', function() {
  return gulp.src('/job_board/public/imgs')
    .pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
    .pipe(gulp.dest('/job_board/public/imgs'))
    .pipe(notify({ message: 'Images task complete' }));
});

gulp.task('svgs', function() {
    return iconify({
        errLogToConsole: false,
        onError: function(err) {
            return notify().write(err);
        },
        src: 'job_board/public/imgs/svgs/*.svg',
        pngOutput: 'job_board/public/imgs/svgs-pngs',
        scssOutput: 'job_board/public/scss/svgs',
        cssOutput:  'job_board/public/css/svgs'
    })
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('job_board/public/js/modules/**', ['scripts']);
    gulp.watch('job_board/public/scss/*.scss', ['sass']);
    gulp.watch('job_board/public/imgs/svgs/*.svg', ['svgs']);
    gulp.watch('job_board/public/imgs/*', ['images']);
});

// Default Task
gulp.task('default', ['lint', 'libcss', 'sass', 'images', 'libscripts', 'scripts','svgs', 'watch']);