'use strict';

angular.module('admin').controller('QuestionsController', ['$scope', '$stateParams', '$state', '$location', 'dataPassage', '$localStorage', '$sessionStorage', '$sce', '$cookies', 'dataPassage', 'questions', 'Auth',

	function($scope, $stateParams, $state, $location, $dataPassage, $localStorage, $sessionStorage, $sce, $cookies, dataPassage, questions, Auth) {

		$scope.formData = {}

		$scope.alerts = [];

		$scope.$storage = $sessionStorage;

		$scope.header.adminHeader = true;
		$scope.header.adminAccess = true;

		$scope.siteBaseUrl = siteBaseUrl;

		$scope.newQuestions = [];

		$scope.questions = questions.data.questions;

		console.log($scope.questions);

		$scope.deleteQuestion = function(qid){
			$scope.apiLoading = true;
			var createLoading = true;
			var passedData = {};
			passedData.data = {};
			passedData.apiController = 'questions';

			passedData.method = 'DELETE';

			passedData.params = '?qid='+qid; //+'&access_token='+acToken;
			var create = dataPassage.apiQuery(passedData);
				create.then(function(result){
				if(result.data.status == 'success'){
                    delete $scope.questions[result.data.question];
					$scope.apiLoading = false;
				}
			});

			//var acToken = Auth.getAccessToken('access_token')
			/*if(!acToken){
				newAccess = false;
				newAccess = Auth.getNewAccessToken();
				newAccess.then(function(result) {
					passedData.params = '?qid='+qid+'&access_token='+result.data.access_token;
					var create = dataPassage.apiQuery(passedData);
					create.then(function(result){
						if(result.data.status == 'success'){
		                   	delete $scope.questions[result.data.question];
							$scope.apiLoading = false;
						}
					})
				})
			}else{*/

			//}
		}

		$scope.saveQuestion = function(qdata) {
			if(typeof qdata.id != 'undefined'){
				$scope.questionAPI('put', qdata);
			}else{
				$scope.questionAPI('post', qdata);
			}
		}	

		$scope.questionAPI = function(method, question){
			$scope.alerts = [];
			var formData = {};
			formData.data = {};
			$scope.searchLoading = true;
			formData.method = method;
			formData.apiController = 'questions';
			formData.data.question = question;
			//formData.data.access_token = token;
            dataPassage.apiQuery(formData)
                .then(function(data){
                    $scope.searchLoading = false;
                    //$scope.statement = data.data.statement;
                    $scope.alerts.push({type: 'success', msg: data.data.message});
                }, function(data){
                    $scope.searchLoading = false;
                    $scope.alerts.push({type: 'danger', msg: data.data.error_description});
                   // $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
		}

	}
])