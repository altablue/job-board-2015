'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('pages').factory('routeCheck', ['$http', '$cookies',

    function ($http, $cookies) {

    	return {

    		routeJsonCheck: function(json, value, key)
    		{
    			var hasMatch =false;
    			var state = false;

				for (var index = 0; index < json.length; ++index) {

					var route = json[index];

					//console.log(route.url);

					if(route[key] == value){
					    hasMatch = true;
					    state = route
						break;
					}
				}
				var returned = {'hasMatch': hasMatch, 'state': state}
				return returned;
    		}

    	}

    }
])

.directive('footableDraw', function() {
  return function(scope, element, attrs) {
    if (scope.$last){
        $('table').footable();
        $('table').trigger('footable_redraw');
        $(".pagination-md").rPage();
    }
  };
})

.directive('dateformat', ['$http', function ($http) {
	return {
	    restrict : 'E',
	    replace: true,
	    scope: {
		    tehdate: '@'
		},
	    link: function($scope, element, $attrs) {
			var tempdate = $attrs.date;
			var pattern = /(\d{4})(\d{2})(\d{2})/;
  			$scope.tehdate = moment(tempdate.replace(pattern, '$1-$2-$3')).format('DD/MM/YYYY');
	    },
	    template: '<time ng-bind="tehdate"></time>' //You could define your template in an external file if it is complex (use templateUrl instead to provide a URL to an HTML file)
	}
}]).filter('escape', function () {
    return function(text){
        var escaped = unescape(text);
        return escaped.replace(/\s+/g, '').replace(/[^\w\s]/gi, '').replace(/&amp;/g, '').replace(/[^\w\s]/gi, '').toLowerCase();
    }
});