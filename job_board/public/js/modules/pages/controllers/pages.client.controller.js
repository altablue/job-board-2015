'use strict';

angular.module('pages').controller('HomepageController', ['$scope', '$stateParams', '$state', '$location', '$localStorage', '$sessionStorage', '$sce', '$cookies', '$cookieStore',

	function($scope, $stateParams, $state, $location, $localStorage, $sessionStorage, $sce, $cookies, $cookieStore) {

		$scope.header.adminHeader = false;

		$scope.slider = function()
		{
			var slides = $scope.slides = [];
			var i = 0
			while(i < 5) {
				var newWidth = 600 + slides.length + 1;
				var newLen = slides.length + 1;
			    slides.push({
			      image: 'holder.js/100%x300/auto/#0099cc:#007AA3/text: campaign #'+ newLen,
			      text: 'campaign #'+(slides.length + 1)
			    });
			    i++;
			}
		}
	}

]).controller('CampaignPageController', ['$scope', '$stateParams', '$state', '$location', '$localStorage', '$sessionStorage', '$sce', '$cookies', 'dataPassage',

	function($scope, $stateParams, $state, $location, $localStorage, $sessionStorage, $sce, $cookies, dataPassage) {

		$scope.header.adminHeader = false;

		$scope.campData = $stateParams.campData;

		$scope.tempModules = {};


		for(var i = 0; i < $stateParams.campData.template_data.modules.length || function(){ $scope.modules = $stateParams.campData.template_data.modules; return false; }(); i++){
			$scope.tempModules[$stateParams.campData.template_data.modules[i].name] = $stateParams.campData.template_data.modules[i].params.front[0];
			$scope.tempModules[$stateParams.campData.template_data.modules[i].name].path = $scope.campData.template_data.path+'/'+$scope.tempModules[$stateParams.campData.template_data.modules[i].name].path
		}

		$scope.getJobs = function(query){
			query = typeof query !== 'undefined' ? query : false;
			var formData = {};
			formData.params = '?';
			if(typeof query != 'undefined'){
				for (var i = 0; i < query.length; i++) {
					switch(query[i].query){
						case 'is not':
							var querySyn = '!=';
							break;
						default:
							var querySyn = '=';
					}
					formData.params += query[i].attr+querySyn+query[i].value+'&';
				}

				$scope.alerts = [];
				formData.data = {};
				$scope.searchLoading = true;
				formData.method = 'get';
				formData.apiController = 'search';
				formData.params=formData.params.slice(0, - 1);
				
	            dataPassage.apiQuery(formData)
	                .then(function(data){
	                    $scope.searchLoading = false;
	                    $scope.jobs = data.data.results;
	                }, function(data){
	                    $scope.searchLoading = false;
	                    $scope.alerts.push({type: 'danger', msg: data.data.error_description});
	                });
			}
		}

	}

]).controller('SearchPageController', ['$scope', '$stateParams', '$state', '$location', '$localStorage', '$sessionStorage', '$sce', '$cookies', 'dataPassage', 'Auth', '$http', '$timeout', '$anchorScroll',

	function($scope, $stateParams, $state, $location, $localStorage, $sessionStorage, $sce, $cookies, dataPassage, Auth, $http, $timeout, $anchorScroll) {

		$scope.header.adminHeader = false;

		$scope.tempModules = {};

		$scope.searchLoading = false;

		$scope.search = {};

		$scope.mapEnabled = false;

		$scope.map = false;

		var markCluster = '';

        var mapSearchQ = false;

        $scope.teh = {};

        $scope.viewMap = false;

        var searchButton = 0;

        $scope.teh.s = {name:10, val:10};

            var formData = {};
            formData.data = {};

        $scope.sorts = [{name: "Relevancy", val: "rel"},{name: "Newest First", val: "new"},{name: "Oldest First", val: "old"}]
        $scope.showNums = [{name: 10, val: 10},{name: 25, val: 25},{name: 50, val: 50},{name: 75, val: 75},{name: 100, val: 100}]

        $scope.niceAddy  = '';

        if($location.search().keyword){
            $scope.search.keyword = $location.search().keyword;
        }

        if($location.search().disciplines){
            $scope.teh.disciplines = {value: $location.search().disciplines, name: decodeURIComponent($location.search().disciplines)};
            $scope.search.disciplines = $location.search().disciplines;
        }

        if($location.search().geo){
            $scope.search.geo = decodeURIComponent($location.search().geo);
        }

        if($location.search().address){
            $scope.address = decodeURIComponent($location.search().address);
        }

        if($location.search().location){
            $scope.search.location = decodeURIComponent($location.search().location);
        }

        var fireSearch = false;

        $(".pagination-md").rPage();

		//console.log(Auth.getOAuthEncryption());

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

		$scope.placeChanged = function() {
           	var place = this.getPlace();
            var elems = Object.getOwnPropertyNames(place.geometry.viewport);
            var childParms = Object.getOwnPropertyNames(place.geometry.viewport[elems[1]]);
            var bb = place.geometry.viewport[elems[1]][childParms[0]]+','+place.geometry.viewport[elems[0]][childParms[1]]+','+place.geometry.viewport[elems[1]][childParms[1]]+','+place.geometry.viewport[elems[0]][childParms[0]];
            var elems2 = Object.getOwnPropertyNames(place.geometry.viewport);
			var gl = place.geometry.location[elems[1]]+','+place.geometry.location[elems[0]];
            $scope.niceAddy = place.formatted_address;
            if(place.geometry.viewport){
                $scope.search.geo = bb;
				$scope.search.location = 'bounding_box';
           	}else{
           		$scope.search.location = 'geo_distance';
           		$scope.search.geo = gl;
           	}
        }

        $scope.listTab = function() {
        }

        $scope.fourOhSearch = function() {
            var formData = {};
            formData.data = {};
            formData.method = 'GET';
            formData.apiController = 'search';
            formData.data.search = '';
            formData.params = '?s=100&keyword=&sb=new';
            dataPassage.apiQuery(formData)
                .then(function(data){
                    $('.footable').footable();
                    $scope.searchResults = data.data.results;
                }, function(){
                    $scope.alerts.push({type: 'danger', msg: 'No available jobs at this moment'});
                });
        }

		$scope.searchResults = {};

        var formData = {};
        formData.method = 'get';
        formData.apiController = 'searchFields';
        dataPassage.apiQuery(formData)
            .then(function(data){
                $scope.disciplines = $.map(JSON.parse(data.data.disciplines), function(value, index) {
                    return [value];
                });
                //console.log(JSON.parse(data.data.countries));
                //console.log(JSON.parse(data.data.companies));
            });

		$scope.activateMap = function(mapEnabled)
		{	
            if(!mapSearchQ){            
                $scope.mapSearch();
                mapSearchQ = true;
            }
			$timeout(function () {
				if(!mapEnabled){
					L.mapbox.accessToken = 'pk.eyJ1IjoiYWx0YWJsdWUiLCJhIjoiZTExYjgyYTNlOWQ0Y2Q3OWJiZDIwYjM3ZWViMjNmNGEifQ.S2_ECSlRsa7ow0EhINffOg';
					$scope.map = L.mapbox.map('map', 'mapbox.emerald',{worldCopyJump: true, spiderfyOnMaxZoom:false, minZoom: 1, maxZoom: 18})
					    .setView([40, -74.50], 10)
						.invalidateSize(true);
					$scope.mapEnabled = true;
				}
				var jobLoc = L.layerGroup().addTo($scope.map);
				var markerGroup = [];

				markCluster = new L.markerClusterGroup({
					iconCreateFunction: function(cluster) {
				        return L.mapbox.marker.icon({
				          	'marker-symbol': cluster.getChildCount(),
				          	'marker-color': '#0099CC',
				          	'marker-size': 'large',
				        });
			      	}
				});

				for (var i = 0; i < $scope.mapResults.length; i++) {
					if($scope.mapResults[i]._source.location){
						var venue = $scope.mapResults[i];
						var jsonLat = $scope.mapResults[i]._source.location.split(',');
						//var latlng = L.latLng(jsonLat[0],jsonLat[1]);
						var marker = L.marker(new L.LatLng(jsonLat[0],jsonLat[1]), {
						    icon: L.mapbox.marker.icon({
							    'marker-symbol': 'circle',
							    'marker-size': 'large',
						        'marker-color': '#0099CC'
						    })
						});
						marker.bindPopup('<strong><a href="/job/'+$scope.mapResults[i]._source.category+'/'+$scope.mapResults[i]._source.referencenumber+'">' + $scope.mapResults[i]._source.title + '</a></strong><br/><span>Reference Id: '+$scope.mapResults[i]._source.referencenumber+'<br/>Location: '+$scope.mapResults[i]._source.country+'</span>');
						markerGroup[i] = [jsonLat[0],jsonLat[1]];
						markCluster.addLayer(marker);
					}
				}

				//var bounds = markCluster.getBounds();
				//$scope.map.fitBounds(bounds);

				var latlngbounds = new L.latLngBounds(markerGroup);
				$scope.map.fitBounds(latlngbounds);
				$scope.map.addLayer(markCluster);
			});
		}

		$scope.submitSearch = function($page,$autoSearch)
		{
            searchButton  = 1;
            mapSearchQ = false;
			$page = typeof $page !== 'undefined' ? $page : false;
			$scope.searchLoading = true;
			formData.method = 'get';
			formData.apiController = 'search';
			var search = $scope.search;
            if(typeof $scope.teh.disciplines == 'object'){
                if('value' in $scope.teh.disciplines){
                    $scope.search.disciplines = $scope.teh.disciplines.name;
                }
            }else{
                delete $scope.search.disciplines;
            }
            if(typeof $scope.teh.s == 'object'){
                $scope.search.s = $scope.teh.s.val;
            }
            if(typeof $scope.teh.sb == 'object'){
                $scope.search.sb = $scope.teh.sb.val;
            }
			var str = "";
			for (var key in search) {
			    if (str != "") {
			        str += "&";
			    }
			    str += key + "=" + encodeURIComponent(search[key]);
			}
			if($page){
				str += '&f='+$page;		
			}
			formData.params = '?'+str;
            if(!$autoSearch){
                if($scope.search.geo){
                    str += '&address='+$scope.niceAddy+'&show=true';
                }else{
                    str += '&show=true';
                }
                window.history.pushState("", "", '?'+str);
            }
            var state = {};
            dataPassage.apiQuery(formData)
                .then(function(data){
                    $scope.currentPage = 1;
                    $scope.searchEmpty = false;
                	if($scope.mapEnabled){
                		$scope.map.removeLayer(markCluster);
                		$scope.activateMap(true);
                	}
                    $scope.searchLoading = false;
                    //$scope.statement = data.data.statement;
                    if(data.data.status == 'error'){
                        $scope.searchEmpty = true;
                        //$scope.alerts.push({type: 'danger', msg: 'Sorry, no jobs can be found for that search term. Try searching for all jobs using the button above.'});
                    }else{
                        if(!mapSearchQ){
                            if($scope.mapEnabled){
                                $scope.map.removeLayer(markCluster);
                                $scope.activateMap(true);
                            }
                            $scope.mapResults = data.data.results;
                        }
                        $scope.searchEmpty = false;
                        $scope.searchResults = data.data.results;
                        $scope.searchTotal = data.data.total;
                        if($page){$scope.currentPage = $page;}
                    }
                    $location.hash('results');
                    $anchorScroll();
                }, function(data){
                    $scope.currentPage = 1;
                    $scope.searchTotal = 0;
                    $scope.searchLoading = false;
                    $scope.alerts.push({type: 'danger', msg: data.data.error_description});
                   // $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
		}

        $scope.mapSearch = function($page)
        {
            searchButton  = 3;
            $scope.viewMap = true;
            $page = typeof $page !== 'undefined' ? $page : false;
            $scope.alerts = [];
            var formData = {};
            formData.data = {};
            $scope.searchLoading = true;
            formData.method = 'get';
            formData.apiController = 'search';
            var search = $scope.search;
            if(typeof $scope.teh.disciplines == 'object'){
                if('value' in $scope.teh.disciplines){
                    $scope.search.disciplines = $scope.teh.disciplines.name;
                }
            }else{
                delete $scope.search.disciplines;
            }
            if(typeof $scope.teh.s == 'object'){
                $scope.search.s = $scope.teh.s.val;
            }
            if(typeof $scope.teh.sb == 'object'){
                $scope.search.sb = $scope.teh.sb.val;
            }
            var str = "";
            for (var key in search) {
                if (str != "") {
                    str += "&";
                }
                str += key + "=" + encodeURIComponent(search[key]);
            }
            if($page){
                str += '&s='+search.s+'&f='+$page;     
            }
            formData.params = '?'+str;
            if(!$autoSearch){
                if($scope.search.geo){
                    str += '&address='+$scope.niceAddy+'&show=true';
                }else{
                    str += '&show=true';
                }
                window.history.pushState("", "", '?'+str);
            }
            dataPassage.apiQuery(formData)
                .then(function(data){
                    $scope.currentPage = 1;
                    $scope.searchEmpty = false;
                    if($scope.mapEnabled){
                        $scope.map.removeLayer(markCluster);
                        $scope.activateMap(true);
                    }
                    $scope.searchLoading = false;
                    //$scope.statement = data.data.statement;
                    $scope.mapResults = data.data.results;
                    $scope.searchTotal = data.data.total;
                    if($page){$scope.currentPage = $page;}
                    $location.hash('results');
                    $anchorScroll();
                }, function(data){
                    $scope.currentPage = 1;
                    $scope.searchTotal = 0;
                    $scope.searchLoading = false;
                    $scope.alerts.push({type: 'danger', msg: data.data.message});
                   // $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
        }

        $scope.submitSearchAll = function($page){
            searchButton  = 2;
            $page = typeof $page !== 'undefined' ? $page : false;
            $scope.alerts = [];
            var formData = {};
            formData.data = {};
            $scope.searchLoading = true;
            formData.method = 'get';
            formData.apiController = 'search';
            var search = $scope.search;
            var str = "keyword=";
            if(typeof $scope.teh.s == 'object'){
                str += '&s='+$scope.teh.s.val;
            }
            if(typeof $scope.teh.sb == 'object'){
                str += '&sb='+$scope.teh.sb.val;
            }
            if($page){
                str += '&f='+$page;   
            }
            formData.params = '?'+str;
            dataPassage.apiQuery(formData)
                .then(function(data){
                    $scope.currentPage = 1;
                    $scope.searchEmpty = false;
                    if($scope.mapEnabled){
                        $scope.map.removeLayer(markCluster);
                        $scope.activateMap(true);
                    }
                    $scope.searchLoading = false;
                    //$scope.statement = data.data.statement;
                    $scope.searchResults = data.data.results;
                    $scope.searchTotal = data.data.total;
                    if($page){$scope.currentPage = $page;}
                    $location.hash('results');
                    $anchorScroll();
                }, function(data){
                    $scope.currentPage = 1;
                    $scope.searchTotal = 0;
                    $scope.searchLoading = false;
                    $scope.alerts.push({type: 'danger', msg: data.data.error_description});
                   // $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });            
        }

		$scope.pageChanged = function(num) {
            if(searchButton > 2){
                $scope.mapSearch($scope.currentPage);  
            }else if(searchButton == 2){
                $scope.submitSearchAll($scope.currentPage);    
            }else if(searchButton < 2 && searchButton > 0 ){
                $scope.submitSearch($scope.currentPage);    
            }
		};

        $scope.updateLoc = function(place){
            delete $scope.search.geo;
            delete $scope.search.location;
        }

        $scope.filterChange = function(value) {
            if(searchButton > 2){
                $scope.mapSearch();  
            }else if(searchButton == 2){
                $scope.submitSearchAll();    
            }else if(searchButton < 2 && searchButton > 0 ){
                $scope.submitSearch();    
            }
        };


        if($location.search().show){
            if($location.search().show == "true"){
                $scope.submitSearch(0,true);
            }
        }

	}

]).controller('JobPageController', ['$scope', '$stateParams', '$routeParams', '$state', '$location', '$localStorage', '$sessionStorage', '$sce', '$cookies', 'dataPassage', 'job', '$modal', '$anchorScroll',

	function($scope, $stateParams, $routeParams, $state, $location, $localStorage, $sessionStorage, $sce, $cookies, dataPassage, job, $modal, $anchorScroll) {

		$anchorScroll();

		$scope.header.adminHeader = false;

		$scope.job = job.data.job;

		var pattern = /(\d{4})(\d{2})(\d{2})/;

        if(typeof $scope.job.date != 'undefined'){
  		    var tehdate = moment($scope.job.date.replace(pattern, '$1-$2-$3')).format('DD/MM/YYYY');
        }else{
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();

            if(dd<10) {
                dd='0'+dd
            } 

            if(mm<10) {
                mm='0'+mm
            } 

            var tehdate = dd+'/'+mm+'/'+yyyy;
        }

		$scope.job.date = tehdate

		$scope.jobdesc = $sce.trustAsHtml(decodeURIComponent(he.decode($scope.job.description)));

		$scope.applyLoading = false;

		$scope.applyDisabled = false;

        var applied = false;
        var linkedIn = false;
        var iOS = /iPad|iPhone|iPod/.test( navigator.userAgent );

        var modalInstance = '';

        $scope.cancel = function () {
           $modalInstance.dismiss('cancel');
        };

        $scope.linkedInAccessGrant = function()
        {
            if(!applied){
                linkedIn = true;
                IN.User.authorize(function() { $scope.linkedInApply(); });
            }
        }

        $scope.checkReturn = function()
        {
            if($location.search().apply && iOS){
                linkedIn = false;
                IN.User.authorize(function() { $scope.linkedInApply(); });
            }
        }

        function liIOS()
        {
            if(linkedIn){
                linkedIn = false;
                window.location.href = location.href + "?apply=true";
            }
        }

        if(iOS){
            window.onfocus = liIOS;
        }
        
        $scope.linkedSuccess = function(data)
        {
            //$state.go('public.questions', {'job': $scope.job});
            var passedData = {}
            passedData.method = 'post';
            passedData.apiController = 'apply';
            passedData.data = {};
            passedData.data.profile = data;
            passedData.data.client = 'jobboardsite';
            passedData.data.irc = $scope.job.referencenumber;
            if($location.search().bid){
                passedData.data.track = $location.search().bid
            }else{
                passedData.data.track = false
            }
            //passedData.data.access_token = token;
            dataPassage.apiQuery(passedData)
                .then(function(result){
                    modalInstance.dismiss();
                    linkedIn = false;
                    $scope.applyLoading = false;
                    applied = false;
                    $state.go('public.relatedJobs', {'job': $scope.job});
                });
        }

        $scope.cvApply = function()
        {
            modalInstance = $modal.open({
                templateUrl: "js/modules/pages/views/cvUpload.modal.client.view.html",
                backdrop: 'static',
                keyboard: false,
                size: 'lg',
                resolve: {
                    items: function() { return job; }
                },
                controller: 'CvUploadModalController',
            });
            modalInstance.result.then(function (data) {
                modalInstance.dismiss();
                $state.go('public.relatedJobs', {'job': $scope.job});
                //$scope.go('admin.editCampaigns');
                //$state.go('public.relatedJobs', {'job': $stateParams.job});
            }, function (data) {
                modalInstance.dismiss();
            });
        }

        $scope.linkedError = function(error)
        {
            $scope.alerts.push({type: 'danger', msg: error});
            applied = false;
        }

        $scope.linkedInApply = function()
        {
            if(!applied){
                modalInstance = $modal.open({
                    templateUrl: "js/modules/pages/views/processing.modal.client.view.html",
                    backdrop: 'static',
                    keyboard: false,
                    size: 'lg',
                    resolve: {
                        items: function() { return {'message': 'Applying to this position using LinkedIn, please wait.' }}
                    },
                    controller: 'ProcessModalController',
                });
                applied = true;
                $scope.applyLoading = true;
                IN.API.Raw("/people/~:(id,first-name,last-name,headline,email-address,phone-numbers,summary,skills,educations,languages,positions,interests)").result($scope.linkedSuccess).error($scope.linkedError);
            }           
        }

	}

]).controller('CvUploadModalController', ['$scope', 'items', '$modalInstance', '$resource','$http', 'Auth', 'dataPassage', 'Upload', '$cookies', '$state', '$location',
	
	function($scope, items, $modalInstance, $resource,$http, Auth, dataPassage, Upload, $cookies, $state, $location) {

		$scope.progress = false;
		$scope.dataUrls = [];

	    var fileReader = new FileReader();

		$scope.uploadCV = function (files) {
	        $scope.formUpload = true;
	        if (files != null) {
	            upload(files[0])
	        }
	    };

        $scope.cancel = function () {
           $modalInstance.dismiss('cancel');
        };

	    $scope.upload = function (files) {
            if($location.search().bid){
                var track = $location.search().bid
            }else{
                var track = false
            }
	        if (files && files.length) {
	            for (var i = 0; i < files.length; i++) {
	                var file = files[i];
	                $scope.progress = true;
	                Upload.upload({
	                    url: 'api/1.0/apply/upload/',
	                    headers: {'Authorization':'Bearer '+$cookies.publicAccessToken},
	                    sendFieldsAs: 'json',
	                    fields: {'irc': items.data.job.referencenumber, 'email': $scope.email, 'track': track},
	                    file: file
	                }).progress(function (evt) {
	                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	                    console.log('progress: ' + progressPercentage + '% ');
	                }).success(function (data, status, headers, config) {
	                	$modalInstance.close('ok');
	                    //console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
	                }).error(function (data, status, headers, config) {
                        $modalInstance.close('cancel');
                    });
	            }
	        }
    	};	

	}

]).controller('CandQuestionsController', ['$scope', '$stateParams', '$state', '$location', '$localStorage', '$sessionStorage', '$modal', 
	
	function($scope, $stateParams, $state, $location, $localStorage, $sessionStorage, $modal) {

		var item = ['123','hello world'];

		var modalInstance = $modal.open({
            templateUrl: "js/modules/pages/views/questions.modal.client.view.html",
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
			resolve: {
            	items: function() { return $stateParams.job; }
            },
            controller: 'CandQuestionsModalController',
        });
	    modalInstance.result.then(function () {
            modalInstance.dismiss();
            //$scope.go('admin.editCampaigns');
            $state.go('public.relatedJobs', {'job': $stateParams.job});
        }, function () {
            modalInstance.dismiss();
            //$scope.go('admin.editCampaigns');
        });

	}
]).controller('CandQuestionsModalController', ['$scope', 'items', '$modalInstance', '$resource','$http', 'Auth', 'dataPassage',

	function($scope, items, $modalInstance, $resource,$http, Auth, dataPassage) {

		$scope.answer = {};

		$scope.alerts = [];
		var formData = {};
		formData.data = {};
		formData.method = 'get';
		formData.apiController = 'questions';
	    dataPassage.apiQuery(formData)
	        .then(function(data){
	            $scope.questions = data.data.questions;
	            for(var key in data.data.questions){
	            	if($scope.questions[key].query.second === 'is'){
		            	if(items[$scope.questions[key].query.first] != $scope.questions[key].query.third){
		            		delete $scope.questions[key];
		            	}
	            	}else{
		            	if(items[$scope.questions[key].query.first] == $scope.questions[key].query.third){
		            		delete $scope.questions[key];
		            	}
	            	}
	            }
	        }, function(data){
	            $scope.alerts.push({type: 'danger', msg: data.data.error_description});
	           // $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
	        });

		$scope.dismiss = function() {
			$scope.$dismiss();
		};

		$scope.ok = function () {
            $modalInstance.close('ok');
        };

        $scope.cancel = function () {
           $modalInstance.dismiss('cancel');
        };

	    $scope.additionalQsSubmit = function(question){
	    	if(question.$valid){
				$scope.alerts = [];
				var formData = {};
				formData.data = {};
				formData.data.answers = $scope.answer;
				formData.method = 'post';
				formData.apiController = 'questions';
				formData.data.response_type = 'token';
				formData.data.client_id = 'jobboardsite';
				//formData.params = '?access_token='+token; 
			    dataPassage.apiQuery(formData)
			        .then(function(data){
			        	$scope.$close(true);
			        }, function(){
			        	console.log('error');
			        });
	    	}
	    }

	}
]).controller('RelJobsController', ['$scope', '$stateParams', '$resource','$http', 'Auth', 'dataPassage', '$modal', '$timeout', '$anchorScroll',
	
	function($scope, $stateParams, $resource, $http, Auth, dataPassage, $modal, $timeout, $anchorScroll) {

		$scope.theJob = $stateParams.job;

		$scope.alerts = [];

        var markCluster = '';

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

		var modalInstance = $modal.open({
            templateUrl: "js/modules/pages/views/marketing.modal.client.view.html",
            backdrop: true,
            keyboard: true,
            size: 'lg',
			resolve: {
            	items: function() { return $stateParams.job; }
            },
            controller: 'MarketingModalController',
        });
	    modalInstance.result.then(function () {
            modalInstance.dismiss();
        }, function () {
            modalInstance.dismiss();
        });

		var formData = {};
		formData.data = {};
		formData.method = 'GET';
		formData.apiController = 'search';
		formData.data.search = '';
		formData.params = '?disciplines='+$scope.theJob.discipline+'&exclude='+$scope.theJob.referencenumber;
	    dataPassage.apiQuery(formData)
	        .then(function(data){
                if(data.data.status == 'error'){
                    $scope.searchEmpty = true;
                    //$scope.alerts.push({type: 'danger', msg: 'Sorry, no jobs can be found for that search term. Try searching for all jobs using the button above.'});
                }else{
                    $('.footable').footable();
                    $scope.searchEmpty = false;
                    $scope.searchResults = data.data.results;
                    $scope.searchTotal = data.data.total;
                }
	        }, function(){
	        	$scope.alerts.push({type: 'danger', msg: 'No related jobs found for this discipline'});
	        });

		$scope.activateMap = function(mapEnabled)
		{	
			$timeout(function () {
				if(!mapEnabled){
					L.mapbox.accessToken = 'pk.eyJ1IjoiYWx0YWJsdWUiLCJhIjoiZTExYjgyYTNlOWQ0Y2Q3OWJiZDIwYjM3ZWViMjNmNGEifQ.S2_ECSlRsa7ow0EhINffOg';
					$scope.map = L.mapbox.map('map', 'mapbox.emerald',{worldCopyJump: true, spiderfyOnMaxZoom:false, minZoom: 1, maxZoom: 18})
					    .setView([40, -74.50], 10)
						.invalidateSize(true);
					$scope.mapEnabled = true;
				}
				var jobLoc = L.layerGroup().addTo($scope.map);
				var markerGroup = [];

				markCluster = new L.markerClusterGroup({
					iconCreateFunction: function(cluster) {
				        return L.mapbox.marker.icon({
				          	'marker-symbol': cluster.getChildCount(),
				          	'marker-color': '#0099CC',
				          	'marker-size': 'large',
				        });
			      	}
				});

				for (var i = 0; i < $scope.searchResults.length; i++) {
					if($scope.searchResults[i]._source.location){
						var venue = $scope.searchResults[i];
						var jsonLat = $scope.searchResults[i]._source.location.split(',');
						//var latlng = L.latLng(jsonLat[0],jsonLat[1]);
						var marker = L.marker(new L.LatLng(jsonLat[0],jsonLat[1]), {
						    icon: L.mapbox.marker.icon({
							    'marker-symbol': 'circle',
							    'marker-size': 'large',
						        'marker-color': '#0099CC'
						    })
						});
						marker.bindPopup('<strong><a href="/job/'+$scope.searchResults[i]._source.category+'/'+$scope.searchResults[i]._source.referencenumber+'">' + $scope.searchResults[i]._source.title + '</a></strong><br/><span>Extra info</span>');
						markerGroup[i] = [jsonLat[0],jsonLat[1]];
						markCluster.addLayer(marker);
					}
				}

				//var bounds = markCluster.getBounds();
				//$scope.map.fitBounds(bounds);

				var latlngbounds = new L.latLngBounds(markerGroup);
				$scope.map.fitBounds(latlngbounds);
				$scope.map.addLayer(markCluster);
			});
		}

	}

])
.controller('MarketingModalController', ['$scope', 'items', '$modalInstance', '$resource','$http', 'Auth', 'dataPassage',

	function($scope, items, $modalInstance, $resource,$http, Auth, dataPassage) {

		$scope.job = items;

		$scope.subscribe = false;

        $scope.cancel = function () {
           $modalInstance.dismiss('cancel');
        };

	}

]).controller('ProcessModalController', ['$scope', 'items', '$modalInstance', '$resource','$http', 'Auth', 'dataPassage',

	function($scope, items, $modalInstance, $resource,$http, Auth, dataPassage) {

		$scope.message = items;

	}
]);