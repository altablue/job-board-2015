'use strict';

angular.module('core').controller('CoreController', ['$scope', '$stateParams', '$localStorage', '$sessionStorage', '$cookieStore',

	function ($scope, $stateParams, $localStorage, $sessionStorage, $cookieStore) {

		$scope.alerts = [];

		$scope.header = {};

		$scope.header.adminHeader = $stateParams.adminHeader || false;

		$scope.header.adminAccess = false;

        $scope.page = {};

        $scope.page.title = 'Search and Find all Globally available Jobs through Altablue';

        $scope.page.description = 'Search and Find all Globally available Jobs through Altablue with our infamous 1-click application process. Altablue - Great People, Great Business';

		$scope.closeAlert = function(index) {
			$scope.alerts.splice(index, 1);
		};

		$(".pagination").rPage();

	}

]);