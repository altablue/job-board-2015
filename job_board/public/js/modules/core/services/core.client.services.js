'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('core').factory('Global', ['$http', '$cookies',

    function ($http, $cookies) {
        
       
    }
])
.service('APIInterceptor', ['$rootScope', '$cookies', '$cookieStore', '$q', '$injector','$location',
	function($rootScope, $cookies, $cookieStore, $q, $injector, $location) {
	    var service = this;

	    service.request = function(config) { 
	        if($cookieStore.get('access_token')){
	            config.headers.authorization  = 'Bearer '+$cookieStore.get('access_token');
	        }else if($cookies.publicAccessToken){
	            config.headers.authorization  = 'Bearer '+$cookies.publicAccessToken;
	        }else if($rootScope.oauth){
	        	config.headers.authorization  = 'Bearer '+$rootScope.oauth;
	        }else{
	        	config.headers.authorization  = 'Bearer fffacf26f948a8848cf2f67bc2158780592d02f1';
	        }

	        return config;
	    };

	    service.responseError = function(response) {
	        if (response.status === 401) {
	        	var deferred = $q.defer();		
				$injector.get("$http").post('/api/1.0/token/gen', {grant_type: 'refresh_token', refresh_token: $cookies.publicRefreshToken, client_id: 'k6iIxfjCZ462AWJuxi9eXDx6ocmAfnB2Lh2iUCts9q49m46Q0O'}).then(function(loginResponse) { 
					if (loginResponse.data && loginResponse.status === 200) { 

                        $cookieStore.remove('publicAccessToken');
                        document.cookie = 'publicAccessToken=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

                        var date = new Date();
                        date.setTime(date.getTime()+(1209600));
                        var refresh = "; expires="+date.toGMTString();
                        var date = new Date();
                        date.setTime(date.getTime()+(loginResponse.data.expires_in));
                        var access = "; expires="+date.toGMTString();

                        document.cookie = "publicAccessToken="+loginResponse.data.access_token+'; expires='+access+';';
                        document.cookie = 'publicRefreshToken='+loginResponse.data.refresh_token+'; expires='+refresh+';';

                        $rootScope.oauth = loginResponse.data.access_token;

						$injector.get("$http")(response.config).then(function(response) { 
							deferred.resolve(response); 
						},function(response) {
							deferred.reject(); // something went wrong 
						}); 
					} else { 
						deferred.reject(); // login.json didn't give us data 
					} 
				}, function(response) { 
					deferred.reject(); // token retry failed, redirect so user can login again 
					//$location.path('/user/sign/in'); 
					return; 
				}); 
				return deferred.promise;
	        }
	        return response;
	    };
	}
])
.directive('title', ['$rootScope', '$timeout',
  function($rootScope, $timeout) {
    return {
      link: function() {

        var listener = function(event, toState) {

          $timeout(function() {
            $rootScope.title = (toState.data && toState.data.pageTitle) 
            ? toState.data.pageTitle 
            : 'Default title';
            $rootScope.desc = (toState.data && toState.data.pageDesc) 
            ? toState.data.pageDesc
            : 'Default Desc';
          });
        };

        $rootScope.$on('$stateChangeSuccess', listener);
      }
    };
  }
]);

        /*if($cookieStore.get('access_token')){
            headersGetter()['Authorization'] = 'Bearer '+$cookieStore.get('access_token');
        }else{
            headersGetter()['Authorization'] = 'Bearer '+$cookies.publicAccessToken;
        }*/

/*.factory('api', function ($http, $cookies, $cookieStore) {
	return {
		init: function (token) {
			if($cookieStore.get('access_token')){
				$http.defaults.headers.common['Authorization'] = 'Bearer '+$cookieStore.get('access_token');
			}else{
				$http.defaults.headers.common['Authorization'] = 'Bearer '+$cookies.publicAccessToken;
			}
		}
	};
});*/