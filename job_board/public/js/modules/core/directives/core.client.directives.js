'use strict';

/*angular.module('core').directive('selectPicker', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            if(element){
                $(element).selectpicker();
            }
        }
    };
})*/
angular.module('core').filter('capitalize', function() {
    return function(input, all) {
        return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
})

.directive('footablequals', function(){
  return function(scope, element){
    var footableTable = element.parents('table');


    if( !scope.$last ) {
        return false;
    }

    scope.$evalAsync(function(){

        if (! footableTable.hasClass('footable-loaded')) {
            footableTable.footable();
        }

        footableTable.trigger('footable_initialized');
        footableTable.trigger('footable_resize');
        footableTable.data('footable').redraw();

    });
  };
});