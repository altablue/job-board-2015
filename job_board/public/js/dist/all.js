'use strict';

var siteBaseUrl = "https://jobs.alta-blue.com";

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function() {
	// Init module configuration options
	var applicationModuleName = 'abjb';
	var applicationModuleVendorDependencies = ['ngResource', 'ngRoute', 'ngCookies',  'ngAnimate', 'ui.select', 'ngTouch',  'ngSanitize',  'ui.router', 'ui.bootstrap', 'ui.utils', 'angular-ladda', 'ngHolder', 'angular-loading-bar', 'ngStorage', 'angularMoment', 'ngMap', 'ui.footable', 'ngFileUpload', 'iso-3166-country-codes'];

	// Add a new vertical module
	var registerModule = function(moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();
'use strict';

var $urlRouterProviderRef = null;
var $stateProviderRef = null;
var $locationProviderRef = null;

angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

angular.module(ApplicationConfiguration.applicationModuleName).config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', '$uiViewScrollProvider',
	function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $uiViewScrollProvider) {
    	$urlRouterProviderRef = $urlRouterProvider;
    
    	$locationProvider.html5Mode(true);
    	$uiViewScrollProvider.useAnchorScroll();
    	$stateProviderRef = $stateProvider;
    	$locationProviderRef = $locationProvider;

		$urlRouterProvider.rule(function($injector, $location) {
	        if($location.protocol() === 'file')
	            return;

	        var path = $location.path()
	        // Note: misnomer. This returns a query object, not a search string
	            , search = $location.search()
	            , params
	            ;

	        // check to see if the path already ends in '/'
	        if (path[path.length - 1] === '/') {
	            return;
	        }

	        // If there was no search string / query params, return with a `/`
	        if (Object.keys(search).length === 0) {
	            return path + '/';
	        }

	        // Otherwise build the search string and return a `/?` prefix
	        params = [];
	        angular.forEach(search, function(v, k){
	            params.push(k + '=' + v);
	        });
	        return path + '/?' + params.join('&');
	    });

	}
]);

angular.module(ApplicationConfiguration.applicationModuleName).config(['$interpolateProvider','$routeProvider','$locationProvider','$resourceProvider',
	function($interpolateProvider,$routeProvider,$locationProvider,$resourceProvider) {
  		$interpolateProvider.startSymbol('[{[');
 	 	$interpolateProvider.endSymbol(']}]');
 	 	$resourceProvider.defaults.stripTrailingSlashes = true;
 	 }
]);

angular.module(ApplicationConfiguration.applicationModuleName).config(['$urlMatcherFactoryProvider',
	function ($urlMatcherFactoryProvider) {
	  $urlMatcherFactoryProvider.caseInsensitive(true);
	  $urlMatcherFactoryProvider.strictMode(true);
	}
]);

angular.element(document).ready(function() {
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});

function onLinkedInLoad() {
	IN.Event.on(IN, "auth", onLinkedInAccess);
}

function onLinkedInLogout() {
	//location.reload(true);
}

function onLinkedInAccess(){
	angular.element(document.getElementById("linkedInJS")).scope().$apply(
		function($scope) {
			//$scope.linkedInAccessGrant();
		}
	);
}
(function(exports){

    var config = {

        /* List all the roles you wish to use in the app
        * You have a max of 31 before the bit shift pushes the accompanying integer out of
        * the memory footprint for an integer
        */
        roles :[
            'public',
            'user',
            'admin'],

        /*
        Build out all the access levels you want referencing the roles listed above
        You can use the "*" symbol to represent access to all roles.

        The left-hand side specifies the name of the access level, and the right-hand side
        specifies what user roles have access to that access level. E.g. users with user role
        'user' and 'admin' have access to the access level 'user'.
         */
        accessLevels : {
            'public' : "*",
            'anon': ['public'],
            'user' : ['user', 'admin'],
            'admin': ['admin']
        }

    }

    exports.userRoles = buildRoles(config.roles);
    exports.accessLevels = buildAccessLevels(config.accessLevels, exports.userRoles);

    /*
        Method to build a distinct bit mask for each role
        It starts off with "1" and shifts the bit to the left for each element in the
        roles array parameter
     */

    function buildRoles(roles){

        var bitMask = "01";
        var userRoles = {};

        for(var role in roles){
            var intCode = parseInt(bitMask, 2);
            userRoles[roles[role]] = {
                bitMask: intCode,
                title: roles[role]
            };
            bitMask = (intCode << 1 ).toString(2)
        }

        return userRoles;
    }

    /*
    This method builds access level bit masks based on the accessLevelDeclaration parameter which must
    contain an array for each access level containing the allowed user roles.
     */
    function buildAccessLevels(accessLevelDeclarations, userRoles){

        var accessLevels = {};
        for(var level in accessLevelDeclarations){

            if(typeof accessLevelDeclarations[level] == 'string'){
                if(accessLevelDeclarations[level] == '*'){

                    var resultBitMask = '';

                    for( var role in userRoles){
                        resultBitMask += "1"
                    }
                    //accessLevels[level] = parseInt(resultBitMask, 2);
                    accessLevels[level] = {
                        bitMask: parseInt(resultBitMask, 2)
                    };
                }
                else console.log("Access Control Error: Could not parse '" + accessLevelDeclarations[level] + "' as access definition for level '" + level + "'")

            }
            else {

                var resultBitMask = 0;
                for(var role in accessLevelDeclarations[level]){
                    if(userRoles.hasOwnProperty(accessLevelDeclarations[level][role]))
                        resultBitMask = resultBitMask | userRoles[accessLevelDeclarations[level][role]].bitMask
                    else console.log("Access Control Error: Could not find role '" + accessLevelDeclarations[level][role] + "' in registered roles while building access for '" + level + "'")
                }
                accessLevels[level] = {
                    bitMask: resultBitMask
                };
            }
        }

        return accessLevels;
    }

})(typeof exports === 'undefined' ? this['routingConfig'] = {} : exports);

'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('admin');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('pages');
'use strict';

// Configuring the Articles module
angular.module('admin').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		/*$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		});*/
	}
]);
'use strict';

// Setting up route
angular.module('admin').config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', 
	function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

	 	var access = routingConfig.accessLevels;
	 	var item = ['123','hello world'];
	 	var modal = '';

		// Home state routing
		$stateProvider
			.state('public.login', {
				url: '/login',
				controller: 'AdminController',
				params: {
					 alerts: null
				},
				templateUrl: 'js/modules/admin/views/login.client.view.html'
			})
			.state('public.password-reset', {
				url: '/password-reset',
				templateUrl: 'js/modules/admin/views/reset.client.view.html'
			})
			.state('public.logout', {
				url: '/logout',
				controller: function($state, $localStorage, $sessionStorage, $cookieStore, $scope){
					/*delete $sessionStorage.loginToken
					document.cookie = 'access_token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';*/
					$scope.header.adminAccess = false;
					$scope.header.adminHeader = false;
					$cookieStore.remove('access_token');
					$cookieStore.remove('refresh_token');
					document.cookie = 'access_token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
					document.cookie = 'refresh_token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
					var alert = {type: 'info', msg: 'You have been logged out'};
					$state.go('public.login', {'alerts': alert});
				}
			});

		$stateProvider
			.state('admin', {
				abstract: true,
				template: "<ui-view/>",
		        data: {
		            access: access.admin
		        }
			})
			.state('admin.dashboard', {
				url: '/admin',
				templateUrl: 'js/modules/admin/views/dashboard.client.view.html'
			})
			.state('admin.listCampaigns', {
				url: '/admin/campaigns',
				params: {
					alerts: null
				},
				controller: 'CampsListController',
				data: {
		            access: access.admin
		        },
				templateUrl: 'js/modules/admin/views/campaigns.list.client.view.html'
			})
			.state('admin.createCampaigns', {
				url: '/admin/campaigns/create',
				params: {
					alerts: null
				},
				data: {
		            access: access.admin
		        },
				controller: 'CampsModalController'
			})
			.state('admin.applicationQuestions', {
				url: '/admin/questions',
				params: {
					alerts: null
				},
				resolve: {
					questions: function($stateParams, Auth, dataPassage){
						var createLoading = true;
						var passedData = {};
						passedData.data = {};
						passedData.apiController = 'questions';
						passedData.method = 'GET';

						/*var acToken = Auth.getAccessToken('access_token')

						if(!acToken){
							newAccess = false;
							newAccess = Auth.getNewAccessToken();
							newAccess.then(function(result) {
								passedData.params = passedData.params+'?access_token='+result.data.access_token;
								return dataPassage.apiQuery(passedData);
							})
						}else{*/
							//passedData.params = passedData.params//+'?access_token='+acToken;
							return dataPassage.apiQuery(passedData);
						//}
					}
				},
				data: {
		            access: access.admin
		        },
				controller: 'QuestionsController',
				templateUrl: 'js/modules/admin/views/questions.client.view.html'
			})			
			.state('admin.editCampaigns', {
				url: '/admin/campaigns/:campId',
				params: {
					alerts: null
				},
				controller: 'CampsEditController',
				resolve: {
					campaign: function($stateParams, Auth, dataPassage){
						var createLoading = true;
						var passedData = {};
						passedData.data = {};
						passedData.apiController = 'campaigns';
						passedData.method = 'GET';
						passedData.params = $stateParams.campId+'/';

						//var acToken = Auth.getAccessToken('access_token')

						/*if(!acToken){
							newAccess = false;
							newAccess = Auth.getNewAccessToken();
							newAccess.then(function(result) {
								passedData.params = passedData.params+'?access_token='+result.data.access_token;
								return dataPassage.apiQuery(passedData);
							})
						}else{*/
							//passedData.params = passedData.params+'?access_token='+acToken;
							return dataPassage.apiQuery(passedData);
						//}
					}
				},
				data: {
		            access: access.admin
		        },  
				templateUrl: 'js/modules/admin/views/campaigns.create.client.view.html'
			})
			.state('admin.editCampaignsContent', {
				url: '/admin/campaigns/:campId/content/',
				params: {
					alerts: null
				},
				controller: 'CampsEditController',
				resolve: {
					campaign: function($stateParams, Auth, dataPassage){
						var createLoading = true;
						var passedData = {};
						passedData.data = {};
						passedData.apiController = 'campaigns';
						passedData.method = 'GET';
						passedData.params = $stateParams.campId+'/';

						//var acToken = Auth.getAccessToken('access_token')

						/*if(!acToken){
							newAccess = false;
							newAccess = Auth.getNewAccessToken();
							newAccess.then(function(result) {
								passedData.params = passedData.params+'?access_token='+result.data.access_token;
								return dataPassage.apiQuery(passedData);
							})
						}else{*/
							//passedData.params = passedData.params+'?access_token='+acToken;
							return dataPassage.apiQuery(passedData);
						//}
					}
				},
				data: {
		            access: access.admin
		        },  
				templateUrl: 'js/modules/admin/views/campaigns.module.client.view.html'
			})
	}
]);
'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('admin').factory('dataPassage', ['$http', '$cookies', '$q', '$injector', '$cookieStore',

    function ($http, $cookies, $q, $injector, $cookieStore) {

        return {

            apiQuery: function (passedData){
                var url = siteBaseUrl+'/api/1.0/'+passedData.apiController+'/';
                if(typeof passedData.apiAction != 'undefined'){
                    url=url+passedData.apiAction+'/';
                }
                if(typeof passedData.params != 'undefined'){
                    url=url+passedData.params;
                }
                //console.log(url);
                var req = {
                    method: passedData.method,
                    url: url,
                    headers: {
                        'contentType': 'application/json'
                    },
                    data: passedData.data
                }
                return $http(req)
                    .success(function(data){
                        return data;
                    })
                    .error(function(data, status){
                        return data;
                    }); 
            }
        }

    }
])

.factory('Auth', ['$http', '$cookieStore', '$localStorage', '$sessionStorage',

    function($http, $cookieStore, $localStorage, $sessionStorage){

        var accessLevels = routingConfig.accessLevels
            , userRoles = routingConfig.userRoles;

            if($cookieStore.get('access_token')){
                var currentUser = { username: $cookieStore.get('access_token'), role: userRoles.admin }
            }else{
                var currentUser =  { username: '', role: userRoles.public }
            }

        //$cookieStore.remove('user');

        function changeUser(user) {
            angular.extend(currentUser, user);
        }

        return {
            authorize: function(accessLevel, role) {

                if($cookieStore.get('access_token')){
                    var currentUser = { username: $cookieStore.get('access_token'), role: userRoles.admin }
                }else{
                    var currentUser =  { username: '', role: userRoles.public }
                }

                console.log(currentUser);
                
                if(role === undefined) {
                    role = currentUser.role;
                }

                return accessLevel.bitMask & role.bitMask;
            },
            isLoggedIn: function(user) {
                if(user === undefined) {
                    user = currentUser;
                }
                return user.role.title === userRoles.user.title || user.role.title === userRoles.admin.title;
            },
            getOAuthEncryption: function(){
                var at = this.getAccessToken('publicAccessToken');
                console.log(at)
                var hash = CryptoJS.HmacSHA512('gsQtGEr70bHTSn1HOY3nioWdEsBgb2vQqD3FJspxtv8&domain='+document.domain+'&access_token='+at,document.domain);
                return hash.toString();
            },
            getAccessToken: function(cname){
                var name = cname + "=";
                var ca = document.cookie.split(';');
                for(var i=0; i<ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0)==' ') c = c.substring(1);
                    if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
                }
                return "";
            },
            getNewAccessToken: function(cname){
                var data = {}
                data.grant_type = "refresh_token"
                data.refresh_token = $sessionStorage.loginToken
                data.jbuser = 'gsQtGEr70bHTSn1HOY3nioWdEsBgb2vQqD3FJspxtv'
                var req = {
                    method: 'POST',
                    url: siteBaseUrl+'/api/1.0/users/login/',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: serialize(data)
                }
                return $http(req)
                    .success(function(data){
                        document.cookie = 'access_token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                        $sessionStorage.loginToken = data.refresh_token;
                        createCookie('access_token', data.access_token, 1);
                        return data.access_token;
                    })
                    .error(function(data){
                        return 'error';
                    });  
            },
            accessLevels: accessLevels,
            userRoles: userRoles,
            user: currentUser
        };
    }
])
.factory('Users', function($http) {
    return {
        getAll: function(success, error) {
            $http.get('/users').success(success).error(error);
        }
    };
})
.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  }
})

.directive('ngBindModel',function($compile){
    return{
        compile:function(tEl,tAtr){
          tEl[0].removeAttribute('ng-bind-model')
            return function(scope){
              tEl[0].setAttribute('ng-model',scope.$eval(tAtr.ngBindModel))
              $compile(tEl[0])(scope)
            }
        }
    }
});

var serialize = function(obj) {
  var str = [];
  for(var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
}


/*var getHMAC = function(key, timestamp) {
    var key2 = "A5woV0GpCX5NuNnWMoaOUZSiq4dC2RgK6rLm0OtpzY2Y7RVmplsEwEyuTgk7On2xekoYiBdSHItMei9N8Os6c9jfqGvrpGlULOadHH6iH4StZDEeSw7TBebhAGAi8wKFuBFOBctei78s6m8GWbq6HL84FM5BpdaFhTKUuMvuVEy57mtWYya7nJ08uSNohexsYsFggF0LILZKMwmk2OBJiltnTk39duJA2DQ";
    var hash = CryptoJS.HmacSHA512(key+timestamp,key2);
    return hash.toString();
};

var getMicrotime = function (get_as_float) {

  var now = new Date().getTime() / 1000;
  var s = parseInt(now, 10);

  return (get_as_float) ? now : (Math.round((now - s) * 1000) / 1000) + ' ' + s;
}; */
'use strict';

angular.module('admin').controller('AdminController', ['$scope', '$stateParams', '$location', 'dataPassage', '$localStorage', '$sessionStorage', '$sce', '$cookieStore', 'dataPassage',

	function($scope, $stateParams, $location, $dataPassage, $localStorage, $sessionStorage, $sce, $cookieStore, dataPassage) {

		$scope.formData = {}

		$scope.alerts = [];

		$scope.$storage = $sessionStorage;

		if($stateParams.alerts){
			$scope.alerts.push($stateParams.alerts);
		}

		//$scope.$parent.adminHeader = false;
		
		$scope.login = function()
		{
			$scope.alerts = [];
			var formData = {};
			formData.data = {};
			$scope.searchLoading = true;
			formData.method = 'post';
			formData.apiController = 'users';
			formData.apiAction = 'login';
			var search = $scope.search;
			//console.log($scope.formData);
			formData.data.grant_type = 'password';
			formData.data.username = $scope.formData.data.username;
            formData.data.scope = 'write';
			formData.data.password = $scope.formData.data.password;
			formData.data.client_id = 'gsQtGEr70bHTSn1HOY3nioWdEsBgb2vQqD3FJspxtv8';
            dataPassage.apiQuery(formData)
                .then(function(data){
                    $scope.searchLoading = false;
                    $cookieStore.put('access_token', data.data.access_token);
                    $cookieStore.put('refresh_token', data.data.refresh_token);
                    //createCookie('access_token', data.data.access_token, data.data.expires_in);
                   // createCookie('refresh_token', data.data.refresh_token, data.data.expires_in);
                    $scope.header.adminHeader = true;
                    $scope.adminHeader = true;
                    $scope.header.adminAccess = true;
                    $scope.adminAccess = true;
                    $location.path('/admin');
                }, function(data){
                    $scope.searchLoading = false;
                    $scope.alerts.push({type: 'danger', msg: data.data.error_description});
                   // $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
			/*$scope.alerts = [];
			$scope.loginLoading = true;
			$scope.formData.apiMethod = 'post';
            $dataPassage.apiSubmit($scope.formData)
                .then(function(data){
                    $scope.loginLoading = false;
                    $scope.$storage.loginToken = data.data.refresh_token;
                    createCookie('access_token', data.data.access_token, 1);
                    if($scope.formData.remember){
                    	$cookieStore.put('rememberLoginToken', data.data.refresh_token);
                    }
                    $scope.adminHeader = true;
                    $location.path('/admin');
                }, function(data){
                    $scope.loginLoading = false;
                    $scope.alerts.push({type: 'danger', msg: data.data.error_description});
                   // $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });*/
		}

		$scope.passReset = function()
		{
			$scope.loginLoading = true;
		}

	}

]);

function createCookie(name,value,expire) {
  if (expire) {
    var date = new Date();
    date.setTime(date.getTime()+(expire));
    var expires = "; expires="+date.toGMTString();
  }
  else var expires = "";
  document.cookie = name+"="+value+expires+"; path=/";
}
'use strict';

angular.module('admin').controller('QuestionsController', ['$scope', '$stateParams', '$state', '$location', 'dataPassage', '$localStorage', '$sessionStorage', '$sce', '$cookies', 'dataPassage', 'questions', 'Auth',

	function($scope, $stateParams, $state, $location, $dataPassage, $localStorage, $sessionStorage, $sce, $cookies, dataPassage, questions, Auth) {

		$scope.formData = {}

		$scope.alerts = [];

		$scope.$storage = $sessionStorage;

		$scope.header.adminHeader = true;
		$scope.header.adminAccess = true;

		$scope.siteBaseUrl = siteBaseUrl;

		$scope.newQuestions = [];

		$scope.questions = questions.data.questions;

		console.log($scope.questions);

		$scope.deleteQuestion = function(qid){
			$scope.apiLoading = true;
			var createLoading = true;
			var passedData = {};
			passedData.data = {};
			passedData.apiController = 'questions';

			passedData.method = 'DELETE';

			passedData.params = '?qid='+qid; //+'&access_token='+acToken;
			var create = dataPassage.apiQuery(passedData);
				create.then(function(result){
				if(result.data.status == 'success'){
                    delete $scope.questions[result.data.question];
					$scope.apiLoading = false;
				}
			});

			//var acToken = Auth.getAccessToken('access_token')
			/*if(!acToken){
				newAccess = false;
				newAccess = Auth.getNewAccessToken();
				newAccess.then(function(result) {
					passedData.params = '?qid='+qid+'&access_token='+result.data.access_token;
					var create = dataPassage.apiQuery(passedData);
					create.then(function(result){
						if(result.data.status == 'success'){
		                   	delete $scope.questions[result.data.question];
							$scope.apiLoading = false;
						}
					})
				})
			}else{*/

			//}
		}

		$scope.saveQuestion = function(qdata) {
			if(typeof qdata.id != 'undefined'){
				$scope.questionAPI('put', qdata);
			}else{
				$scope.questionAPI('post', qdata);
			}
		}	

		$scope.questionAPI = function(method, question){
			$scope.alerts = [];
			var formData = {};
			formData.data = {};
			$scope.searchLoading = true;
			formData.method = method;
			formData.apiController = 'questions';
			formData.data.question = question;
			//formData.data.access_token = token;
            dataPassage.apiQuery(formData)
                .then(function(data){
                    $scope.searchLoading = false;
                    //$scope.statement = data.data.statement;
                    $scope.alerts.push({type: 'success', msg: data.data.message});
                }, function(data){
                    $scope.searchLoading = false;
                    $scope.alerts.push({type: 'danger', msg: data.data.error_description});
                   // $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
		}

	}
])
'use strict';

angular.module('admin').controller('CampsController', ['$scope', '$stateParams', '$state', '$location', 'dataPassage', '$localStorage', '$sessionStorage', '$sce', '$cookies',

	function($scope, $stateParams, $state, $location, $dataPassage, $localStorage, $sessionStorage, $sce, $cookies) {

		$scope.formData = {}

		$scope.alerts = [];

		$scope.$storage = $sessionStorage;

		$scope.header.adminHeader = true;

		$scope.header.adminAccess = true;

		$scope.siteBaseUrl = siteBaseUrl;

	}
])

.controller('CampsListController',  ['$scope', '$stateParams', '$state', '$modal', '$resource','$http', 'Auth', 'dataPassage', 
	function($scope, $stateParams, $state, $modal, $resource,$http, Auth, dataPassage) {

		var newAccess = true 

		$scope.alerts = [];

		if($stateParams.alerts){
			$scope.alerts.push($stateParams.alerts);
		}

		$scope.closeAlert = function(index) {
			$scope.alerts.splice(index, 1);
		};

		$scope.getCampaigns = function()
		{
			$scope.apiLoading = true;
			var createLoading = true;
			var passedData = {};
			passedData.data = {};
			passedData.apiController = 'campaigns';
			passedData.method = 'GET';

			var create = dataPassage.apiQuery(passedData);
				create.then(function(result){
				if(result.data.status == 'success'){
					$scope.list = result.data.campaigns;
					$scope.apiLoading = false;
				}
			});
		}

	}
])

.controller('CampsEditController',  ['$scope', '$stateParams', '$state', '$modal', '$resource', '$http', 'Auth', 'dataPassage', 'campaign', '$parse', 
	function($scope, $stateParams, $state, $modal, $resource, $http, Auth, dataPassage, campaign, $parse) {

		var newAccess = true;

		$scope.campaignData = [];
		$scope.campaignData = campaign.data.campaigns;

		$scope.modules = [];

		$scope.moduleCont = {};

		var modelNames = '';

		$scope.types = [{'name':'Discipline', 'value': 'disc'},{'name':'Client', 'value': 'client'},{'name':'Project', 'value': 'proj'},{'name':'Location', 'value': 'loc'}];
		//$scope.temps = [{'name': 'Template #1', 'value': 2},{'name': 'Template #1', 'value': 1}]

		$scope.campTemplateSetup = function()
		{
			$scope.temps = [];
			var passedData = {};
			passedData.data = {};
			passedData.apiController = 'templates';
			passedData.method = 'GET';
			var getFields = dataPassage.apiQuery(passedData);
				getFields.then(function(result){
					if(result.data.status == 'success'){
						$scope.temps = result.data.templates;

						var types = $.grep($scope.types, function(e){ return e.value == $scope.campaignData.type; });
						var temps = $.grep($scope.temps, function(e){ return e.value == $scope.campaignData.temp; });

						$scope.campaignData.type = types[0];
						$scope.campaignData.temp = temps[0];

						var tempPath = '/modules/template-1/admin/'

						var newAccess = true

						$scope.modelName = [];

						console.log($scope.temps);

					}else{
						$scope.alerts.push({type: 'danger', msg: result.data.message});
						$scope.temps = [{}];
					}
				});
		}

		$scope.campaignFilterFields = function()
		{
			$scope.filters = {};
			var passedData = {};
			passedData.data = {};
			passedData.data = $scope.campaignData;
			passedData.apiController = 'searchFields';
			passedData.method = 'GET';
			var getFields = dataPassage.apiQuery(passedData);
				getFields.then(function(result){
					if(result.data.status == 'success'){
						$scope.filters.category = JSON.parse(result.data.categories);
						$scope.filters.country = JSON.parse(result.data.countries);
						$scope.filters.discipline = JSON.parse(result.data.disciplines);
						$scope.filters.company = JSON.parse(result.data.companies);
					}else{
						$scope.alerts.push({type: 'danger', msg: result.data.message});
					}
				});
		}

        $scope.appendField = function(obj)
        {
			if(typeof $scope.moduleCont[obj].data != 'object'){
				$scope.moduleCont[obj].data = [];
			}
			$scope.moduleCont[obj].data.push({});        		
        }

        $scope.deleteField = function(obj, number)
        {
			$scope.moduleCont[obj].data.splice(number, 1);        		
        }

		$scope.getTempModuleData = function(){
			$scope.apiLoading = true;
			$scope.modules = [];
			var passedData = {};
			passedData.data = {};
			passedData.apiController = 'templates';
			passedData.apiAction = $stateParams.campId;

			passedData.method = 'GET';

			//var acToken = Auth.getAccessToken('access_token');
			var moduleArray = [];

			var create = dataPassage.apiQuery(passedData);
				create.then(function(result){
				if(result.data.status == 'success'){
					$scope.modelName = [];
					$scope.moduleCont = {};
					for(var i = 0; i < result.data.template_data.modules.length || function(){ $scope.modules = result.data.template_data.modules; return false; }(); i++){
						$scope.modelName[i] = result.data.template_data.modules[i].name;
						$scope.moduleCont[result.data.template_data.modules[i].name] = {};
						$scope.moduleCont[result.data.template_data.modules[i].name].type = result.data.template_data.modules[i].params.admin[0].type;
						moduleArray[i] = {}
						moduleArray[i].name = result.data.template_data.modules[i].name;
					}
					passedData.apiController = 'campaigns';
					passedData.apiAction = $stateParams.campId;
					passedData.params = 'content'
					var modData = dataPassage.apiQuery(passedData);
					modData.then(function(result){
						if(Object.keys(result.data.modules).length > 0){
							var resultLength = Object.keys(result.data.modules).length;
							for (var i = 0; i < resultLength; i++) {
								$scope.moduleCont[result.data.modules[moduleArray[i].name].moduleid] = result.data.modules[moduleArray[i].name];
							}
						}
					})
				}
			})
		}

		$scope.updateCampaignContent = function(isValid) {
			var updateLoading = true;
			//if(isValid){
			var createLoading = true;
			var passedData = {};
			passedData.data = {};
			passedData.data.modules = $scope.moduleCont;
			passedData.apiController = 'campaigns';
			passedData.params = $stateParams.campId+'/content/';
			passedData.method = 'PUT';

			var create = dataPassage.apiQuery(passedData);
				create.then(function(result){
				if(result.data.status == 'success'){
					updateLoading = false;
					var alert = {type: 'success', msg: 'Campaign has been updated'};
					$state.go('admin.listCampaigns', {'alerts': alert})
				}else{
					$scope.alerts.push({type: 'danger', msg: result.data.message});
				}
			});
		}

		$scope.updateCampaign = function(isValid) {
			var updateLoading = true;
			//if(isValid){
				var createLoading = true;
				var passedData = {};
				passedData.data = $scope.campaignData;
				passedData.apiController = 'campaigns';
				passedData.params = $stateParams.campId+'/';
				passedData.method = 'PUT';

				var create = dataPassage.apiQuery(passedData);
					create.then(function(result){
					if(result.data.status == 'success'){
						updateLoading = false;
						var alert = {type: 'success', msg: 'Campaign has been updated'};
						$state.go('admin.listCampaigns', {'alerts': alert})
					}else{
						$scope.alerts.push({type: 'danger', msg: result.data.message});
					}
				});

	      	//};
	    }
	}
])

.controller('CampsModalController', ['$stateParams', '$state', '$scope', '$modal', '$resource','$http', 'Auth', 'dataPassage', 

	function($stateParams, $state, $scope, $modal, $resource,$http, Auth, dataPassage) {

		var item = ['123','hello world'];

		var modalInstance = $modal.open({
            templateUrl: "js/modules/admin/views/campaignsModal.create.client.view.html",
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
			resolve: {
            	items: function() { return item; }
            },
            controller: 'CampsCreateController',
        });
	    modalInstance.result.then(function () {
            modalInstance.dismiss();
            $scope.go('admin.editCampaigns');
        }, function () {
            modalInstance.dismiss();
            $scope.go('admin.editCampaigns');
        });
    }

])

.controller('CampsCreateController', ['$scope', 'items', '$modalInstance', '$resource','$http', 'Auth', 'dataPassage',

	function($scope, items, $modalInstance, $resource,$http, Auth, dataPassage) {

		$scope.siteBaseUrl = siteBaseUrl;
		$scope.types = [{'name':'Discipline', 'value': 'disc'},{'name':'Client', 'value': 'client'},{'name':'Project', 'value': 'proj'},{'name':'Location', 'value': 'loc'}]
		$scope.temps = [{'name':'Template #1', 'value': 1}]

		$scope.dismiss = function() {
			$scope.$dismiss();
		};

		$scope.ok = function () {
            $modalInstance.close('ok');
        };

        $scope.cancel = function () {
           $modalInstance.dismiss('cancel');
        };

	    $scope.campConfigSubmit = function(isValid) {
	      	var newAccess = true;
			if(isValid){
				var createLoading = true;
				var passedData = {};
				passedData.data = $scope.campConfig;
				passedData.apiController = 'campaigns';
				passedData.method = 'POST';

				var create = dataPassage.apiQuery(passedData);
					create.then(function(result){
					if(result.data.status == 'success'){
						$scope.$close(true);
					}
				});	

				/*var acToken = Auth.getAccessToken('access_token')
				if(!acToken){
					newAccess = false;
					newAccess = Auth.getNewAccessToken();
					newAccess.then(function(result) {
						passedData.data.access_token = result.data.access_token;
						var create = dataPassage.apiQuery(passedData);
						create.then(function(result){
							if(result.data.status == 'success'){
								$scope.$close(true);
							}
						})
					})
				}else{
					passedData.data.access_token = acToken;*/

				//}
	      	};
	    }
	}
]);
'use strict';

angular.module('admin').controller('DashController', ['$scope', '$stateParams', '$state', '$location', 'dataPassage', '$localStorage', '$sessionStorage', '$sce', '$cookies',

	function($scope, $stateParams, $state, $location, $dataPassage, $localStorage, $sessionStorage, $sce, $cookies) {

		$scope.formData = {}

		$scope.alerts = [];

		$scope.$storage = $localStorage;

		$scope.header.adminHeader = true;

		$scope.header.adminAccess = true;

		/*if(!$localStorage.loginToken){
			$state.go('login');
		}*/

		$scope.authCheck = function()
		{
			
		}

	}

]);
'use strict';

angular.module('core').controller('CoreController', ['$scope', '$stateParams', '$localStorage', '$sessionStorage', '$cookieStore',

	function ($scope, $stateParams, $localStorage, $sessionStorage, $cookieStore) {

		$scope.alerts = [];

		$scope.header = {};

		$scope.header.adminHeader = $stateParams.adminHeader || false;

		$scope.header.adminAccess = false;

        $scope.page = {};

        $scope.page.title = 'Search and Find all Globally available Jobs through Altablue';

        $scope.page.description = 'Search and Find all Globally available Jobs through Altablue with our infamous 1-click application process. Altablue - Great People, Great Business';

		$scope.closeAlert = function(index) {
			$scope.alerts.splice(index, 1);
		};

		$(".pagination").rPage();

	}

]);
'use strict';

angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {

	}
]);
'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', 
	function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

		var access = routingConfig.accessLevels;

        $httpProvider.interceptors.push('APIInterceptor');

	}
])
.run(['$rootScope', '$state', 'Auth', '$modalStack', '$http', 'dataPassage', '$location',  function ($rootScope, $state, Auth, $modalStack, $http, dataPassage, $location) {

    $rootScope.$on('$stateNotFound', function (event, toState, toParams, fromState, fromParams) {
        console.log('state not found');
    });

    $rootScope.$on("$routeChangeError", function (event, current, previous, rejection) {
        alert("ROUTE CHANGE ERROR: " + rejection);
    });

    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
        
        if(!('data' in toState) || !('access' in toState.data)){
        	console.log(toState);
            $rootScope.error = "Access undefined for this state";
            console.log($rootScope.error);
            //event.preventDefault();
        }
        else if (!Auth.authorize(toState.data.access)) {
            $rootScope.error = "Seems like you tried accessing a route you don't have access to...";
            console.log($rootScope.error);
            event.preventDefault();
            if(fromState.url === '^') {
                if(Auth.isLoggedIn()) {
                    $state.go('public.home');
                } else {
                    //$rootScope.error = null;
                    console.log($state);
                    var alert = {type: 'info', msg: 'Your session has timed out, please log in again'};
                    $state.go('public.login', {'alerts': alert});
                }
            }else{
                $state.go('public.login');
            }
        }
        else if(Auth.authorize(toState.data.access) && toState.data.access.bitMask == 4){
            if(!Auth.getAccessToken('access_token')){
                Auth.getNewAccessToken();
            }
        }

        var top = $modalStack.getTop();
        if (top) {
            $modalStack.dismiss(top.key);
        }

    });

}]);

/*.run(['$rootScope', '$injector', '$cookies', '$cookieStore', function($rootScope,$injector, $cookies, $cookieStore) {

            /*if($cookieStore.get('access_token')){
                $http.defaults.headers.common['Authorization'] = 'Bearer '+$cookieStore.get('access_token');
            }else{
                $http.defaults.headers.common['Authorization'] = 'Bearer '+$cookies.publicAccessToken;
            }

    $injector.get("$http").defaults.transformRequest = function(data, headersGetter) {
        //headersGetter()['Authorization'] = "Bearer " + sessionService.getAccessToken();
        if($cookieStore.get('access_token')){
            headersGetter()['Authorization'] = 'Bearer '+$cookieStore.get('access_token');
        }else{
            headersGetter()['Authorization'] = 'Bearer '+$cookies.publicAccessToken;
        }
        if (data) {
            return angular.toJson(data);
        }
    };
}]);*/
'use strict';

/*angular.module('core').directive('selectPicker', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            if(element){
                $(element).selectpicker();
            }
        }
    };
})*/
angular.module('core').filter('capitalize', function() {
    return function(input, all) {
        return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
})

.directive('footablequals', function(){
  return function(scope, element){
    var footableTable = element.parents('table');


    if( !scope.$last ) {
        return false;
    }

    scope.$evalAsync(function(){

        if (! footableTable.hasClass('footable-loaded')) {
            footableTable.footable();
        }

        footableTable.trigger('footable_initialized');
        footableTable.trigger('footable_resize');
        footableTable.data('footable').redraw();

    });
  };
});
'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('core').factory('Global', ['$http', '$cookies',

    function ($http, $cookies) {
        
       
    }
])
.service('APIInterceptor', ['$rootScope', '$cookies', '$cookieStore', '$q', '$injector','$location',
	function($rootScope, $cookies, $cookieStore, $q, $injector, $location) {
	    var service = this;

	    service.request = function(config) { 
	        if($cookieStore.get('access_token')){
	            config.headers.authorization  = 'Bearer '+$cookieStore.get('access_token');
	        }else if($cookies.publicAccessToken){
	            config.headers.authorization  = 'Bearer '+$cookies.publicAccessToken;
	        }else if($rootScope.oauth){
	        	config.headers.authorization  = 'Bearer '+$rootScope.oauth;
	        }else{
	        	config.headers.authorization  = 'Bearer fffacf26f948a8848cf2f67bc2158780592d02f1';
	        }

	        return config;
	    };

	    service.responseError = function(response) {
	        if (response.status === 401) {
	        	var deferred = $q.defer();		
				$injector.get("$http").post('/api/1.0/token/gen', {grant_type: 'refresh_token', refresh_token: $cookies.publicRefreshToken, client_id: 'k6iIxfjCZ462AWJuxi9eXDx6ocmAfnB2Lh2iUCts9q49m46Q0O'}).then(function(loginResponse) { 
					if (loginResponse.data && loginResponse.status === 200) { 

                        $cookieStore.remove('publicAccessToken');
                        document.cookie = 'publicAccessToken=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

                        var date = new Date();
                        date.setTime(date.getTime()+(1209600));
                        var refresh = "; expires="+date.toGMTString();
                        var date = new Date();
                        date.setTime(date.getTime()+(loginResponse.data.expires_in));
                        var access = "; expires="+date.toGMTString();

                        document.cookie = "publicAccessToken="+loginResponse.data.access_token+'; expires='+access+';';
                        document.cookie = 'publicRefreshToken='+loginResponse.data.refresh_token+'; expires='+refresh+';';

                        $rootScope.oauth = loginResponse.data.access_token;

						$injector.get("$http")(response.config).then(function(response) { 
							deferred.resolve(response); 
						},function(response) {
							deferred.reject(); // something went wrong 
						}); 
					} else { 
						deferred.reject(); // login.json didn't give us data 
					} 
				}, function(response) { 
					deferred.reject(); // token retry failed, redirect so user can login again 
					//$location.path('/user/sign/in'); 
					return; 
				}); 
				return deferred.promise;
	        }
	        return response;
	    };
	}
])
.directive('title', ['$rootScope', '$timeout',
  function($rootScope, $timeout) {
    return {
      link: function() {

        var listener = function(event, toState) {

          $timeout(function() {
            $rootScope.title = (toState.data && toState.data.pageTitle) 
            ? toState.data.pageTitle 
            : 'Default title';
            $rootScope.desc = (toState.data && toState.data.pageDesc) 
            ? toState.data.pageDesc
            : 'Default Desc';
          });
        };

        $rootScope.$on('$stateChangeSuccess', listener);
      }
    };
  }
]);

        /*if($cookieStore.get('access_token')){
            headersGetter()['Authorization'] = 'Bearer '+$cookieStore.get('access_token');
        }else{
            headersGetter()['Authorization'] = 'Bearer '+$cookies.publicAccessToken;
        }*/

/*.factory('api', function ($http, $cookies, $cookieStore) {
	return {
		init: function (token) {
			if($cookieStore.get('access_token')){
				$http.defaults.headers.common['Authorization'] = 'Bearer '+$cookieStore.get('access_token');
			}else{
				$http.defaults.headers.common['Authorization'] = 'Bearer '+$cookies.publicAccessToken;
			}
		}
	};
});*/
'use strict';

// Configuring the Articles module
angular.module('pages').config(['$stateProvider', '$urlRouterProvider', 'cfpLoadingBarProvider',
	function($stateProvider, $urlRouterProvider, $cfpLoadingBarProvider) {
		$cfpLoadingBarProvider.includeSpinner = false;
		// Redirect to home view when route not found

	}
]);
'use strict';

// Setting up route
angular.module('pages').config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', 
    function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
        // Redirect to home view when route not found
        var access = routingConfig.accessLevels;

        $urlRouterProvider.otherwise("/");

        $stateProvider
            .state('public', {
                abstract: true,
                template: "<div ui-view />",
                data: {
                    access: access.public,
                    pageTitle: 'Search and Find all Globally available Jobs through Altablue',
                    pageDesc: 'Search and Find all Globally available Jobs through Altablue with our infamous 1-click application process. Altablue - Great People, Great Business'
                }
            })
            .state('public.homeie', {
                url: '',
                templateUrl: 'js/modules/pages/views/search.client.view.html',
            })
            .state('public.results', {
                url: '/results',
                controller: function($q, $scope, $location, dataPassage, $state, $resolve, routeCheck, $templateCache, $localStorage){
                    $state.go('public.home');
                }
            })
            .state('public.home', {
                url: '/',
                templateUrl: 'js/modules/pages/views/search.client.view.html',
            })
            .state('public.questions', {
                //url: '/candidateQuestions',
                /*resolve: {
                    questions: function($stateParams, Auth, dataPassage){

                    }
                },*/                                    
                params: {
                    job: null
                },
                controller: 'CandQuestionsController',
            })
            .state('public.relatedJobs', {
                params: {
                    job: null
                },
                controller: 'RelJobsController',
                templateUrl: 'js/modules/pages/views/relJobs.client.view.html',
            })
            .state('public.search', {
                url: '/search',
                templateUrl: 'js/modules/pages/views/search.client.view.html',
            })
            .state('public.jobView', {
                url: '/job/{industry}/{irc}',
                resolve: {
                    job: function($stateParams, Auth, dataPassage){
                        var formData = {};
                        formData.data = {};
                        formData.method = 'get';
                        formData.apiController = 'jobs';
                        formData.apiAction = $stateParams.irc;
                        formData.params = '&html=true'; //'?access_token='+token+'&html=true';
                        return dataPassage.apiQuery(formData);
                    }
                },
                controller: 'JobPageController',
                templateUrl: 'js/modules/pages/views/job.client.view.html',
            })
            .state('public.404', {
                url: '/404/',
                templateUrl: 'js/modules/pages/views/404.client.view.html',
            })
            .state('public.dynamic', {
                url: '*any',
                controller: function($q, $scope, $location, dataPassage, $state, $resolve, routeCheck, $templateCache, $localStorage){
                    $scope.$storage = $localStorage;
                    var states = $state.get();
                    var check = routeCheck.routeJsonCheck(states,$location.$$path,'url')
                    if(check.hasMatch){
                        var tempCache = $templateCache.get(check.state.templateUrl)
                        var dataCache = $scope.$storage[check.state.name];
                    }else{
                        var tempCache = false;
                        var dataCache = false;
                    }
                    if(!tempCache || !dataCache){
                        var formData = {};
                        formData.data = {};
                        formData.method = 'get';
                        //formData.data.access_token = token;
                        formData.data.jbuser = 'jobboardsite';
                        formData.apiController = 'pages';
                        formData.apiAction = encodeURIComponent($location.$$path.replace(/^\/|\/$/g, ''));
                        //formData.params = '?access_token='+token;
                        dataPassage.apiQuery(formData).success(function(data){
                            if(data.status == 'success'){
                                var state = {
                                    "url": '/'+data.data.campaign_data.url,
                                    "parent": "public",
                                    params: {
                                        campData: null
                                    },
                                    'controller': 'CampaignPageController',
                                    "templateUrl": data.data.template_data.path+'/index.html',
                                };
                                if(!check.hasMatch){
                                    $stateProviderRef.state('public.'+data.data.campaign_data.pagetitle.replace(/\s/g, ''), state); 
                                }
                                $scope.$storage['public.'+data.data.campaign_data.pagetitle.replace(/\s/g, '')] = {'campData':data.data};
                                $state.go('public.'+data.data.campaign_data.pagetitle.replace(/\s/g, ''), {'campData': data.data});
                            }else{
                                console.log('404');
                                $state.go('public.404');
                            }
                        });
                    }else{
                        $state.go(check.state.name, dataCache);
                    }
                }
            });
    }
]);
'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('pages').factory('routeCheck', ['$http', '$cookies',

    function ($http, $cookies) {

    	return {

    		routeJsonCheck: function(json, value, key)
    		{
    			var hasMatch =false;
    			var state = false;

				for (var index = 0; index < json.length; ++index) {

					var route = json[index];

					//console.log(route.url);

					if(route[key] == value){
					    hasMatch = true;
					    state = route
						break;
					}
				}
				var returned = {'hasMatch': hasMatch, 'state': state}
				return returned;
    		}

    	}

    }
])

.directive('footableDraw', function() {
  return function(scope, element, attrs) {
    if (scope.$last){
        $('table').footable();
        $('table').trigger('footable_redraw');
        $(".pagination-md").rPage();
    }
  };
})

.directive('dateformat', ['$http', function ($http) {
	return {
	    restrict : 'E',
	    replace: true,
	    scope: {
		    tehdate: '@'
		},
	    link: function($scope, element, $attrs) {
			var tempdate = $attrs.date;
			var pattern = /(\d{4})(\d{2})(\d{2})/;
  			$scope.tehdate = moment(tempdate.replace(pattern, '$1-$2-$3')).format('DD/MM/YYYY');
	    },
	    template: '<time ng-bind="tehdate"></time>' //You could define your template in an external file if it is complex (use templateUrl instead to provide a URL to an HTML file)
	}
}]).filter('escape', function () {
    return function(text){
        var escaped = unescape(text);
        return escaped.replace(/\s+/g, '').replace(/[^\w\s]/gi, '').replace(/&amp;/g, '').replace(/[^\w\s]/gi, '').toLowerCase();
    }
});
'use strict';

angular.module('pages').controller('HomepageController', ['$scope', '$stateParams', '$state', '$location', '$localStorage', '$sessionStorage', '$sce', '$cookies', '$cookieStore',

	function($scope, $stateParams, $state, $location, $localStorage, $sessionStorage, $sce, $cookies, $cookieStore) {

		$scope.header.adminHeader = false;

		$scope.slider = function()
		{
			var slides = $scope.slides = [];
			var i = 0
			while(i < 5) {
				var newWidth = 600 + slides.length + 1;
				var newLen = slides.length + 1;
			    slides.push({
			      image: 'holder.js/100%x300/auto/#0099cc:#007AA3/text: campaign #'+ newLen,
			      text: 'campaign #'+(slides.length + 1)
			    });
			    i++;
			}
		}
	}

]).controller('CampaignPageController', ['$scope', '$stateParams', '$state', '$location', '$localStorage', '$sessionStorage', '$sce', '$cookies', 'dataPassage',

	function($scope, $stateParams, $state, $location, $localStorage, $sessionStorage, $sce, $cookies, dataPassage) {

		$scope.header.adminHeader = false;

		$scope.campData = $stateParams.campData;

		$scope.tempModules = {};


		for(var i = 0; i < $stateParams.campData.template_data.modules.length || function(){ $scope.modules = $stateParams.campData.template_data.modules; return false; }(); i++){
			$scope.tempModules[$stateParams.campData.template_data.modules[i].name] = $stateParams.campData.template_data.modules[i].params.front[0];
			$scope.tempModules[$stateParams.campData.template_data.modules[i].name].path = $scope.campData.template_data.path+'/'+$scope.tempModules[$stateParams.campData.template_data.modules[i].name].path
		}

		$scope.getJobs = function(query){
			query = typeof query !== 'undefined' ? query : false;
			var formData = {};
			formData.params = '?';
			if(typeof query != 'undefined'){
				for (var i = 0; i < query.length; i++) {
					switch(query[i].query){
						case 'is not':
							var querySyn = '!=';
							break;
						default:
							var querySyn = '=';
					}
					formData.params += query[i].attr+querySyn+query[i].value+'&';
				}

				$scope.alerts = [];
				formData.data = {};
				$scope.searchLoading = true;
				formData.method = 'get';
				formData.apiController = 'search';
				formData.params=formData.params.slice(0, - 1);
				
	            dataPassage.apiQuery(formData)
	                .then(function(data){
	                    $scope.searchLoading = false;
	                    $scope.jobs = data.data.results;
	                }, function(data){
	                    $scope.searchLoading = false;
	                    $scope.alerts.push({type: 'danger', msg: data.data.error_description});
	                });
			}
		}

	}

]).controller('SearchPageController', ['$scope', '$stateParams', '$state', '$location', '$localStorage', '$sessionStorage', '$sce', '$cookies', 'dataPassage', 'Auth', '$http', '$timeout', '$anchorScroll',

	function($scope, $stateParams, $state, $location, $localStorage, $sessionStorage, $sce, $cookies, dataPassage, Auth, $http, $timeout, $anchorScroll) {

		$scope.header.adminHeader = false;

		$scope.tempModules = {};

		$scope.searchLoading = false;

		$scope.search = {};

		$scope.mapEnabled = false;

		$scope.map = false;

		var markCluster = '';

        var mapSearchQ = false;

        $scope.teh = {};

        $scope.viewMap = false;

        var searchButton = 0;

        $scope.teh.s = {name:10, val:10};

            var formData = {};
            formData.data = {};

        $scope.sorts = [{name: "Relevancy", val: "rel"},{name: "Newest First", val: "new"},{name: "Oldest First", val: "old"}]
        $scope.showNums = [{name: 10, val: 10},{name: 25, val: 25},{name: 50, val: 50},{name: 75, val: 75},{name: 100, val: 100}]

        $scope.niceAddy  = '';

        if($location.search().keyword){
            $scope.search.keyword = $location.search().keyword;
        }

        if($location.search().disciplines){
            $scope.teh.disciplines = {value: $location.search().disciplines, name: decodeURIComponent($location.search().disciplines)};
            $scope.search.disciplines = $location.search().disciplines;
        }

        if($location.search().geo){
            $scope.search.geo = decodeURIComponent($location.search().geo);
        }

        if($location.search().address){
            $scope.address = decodeURIComponent($location.search().address);
        }

        if($location.search().location){
            $scope.search.location = decodeURIComponent($location.search().location);
        }

        var fireSearch = false;

        $(".pagination-md").rPage();

		//console.log(Auth.getOAuthEncryption());

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

		$scope.placeChanged = function() {
           	var place = this.getPlace();
            var elems = Object.getOwnPropertyNames(place.geometry.viewport);
            var childParms = Object.getOwnPropertyNames(place.geometry.viewport[elems[1]]);
            var bb = place.geometry.viewport[elems[1]][childParms[0]]+','+place.geometry.viewport[elems[0]][childParms[1]]+','+place.geometry.viewport[elems[1]][childParms[1]]+','+place.geometry.viewport[elems[0]][childParms[0]];
            var elems2 = Object.getOwnPropertyNames(place.geometry.viewport);
			var gl = place.geometry.location[elems[1]]+','+place.geometry.location[elems[0]];
            $scope.niceAddy = place.formatted_address;
            if(place.geometry.viewport){
                $scope.search.geo = bb;
				$scope.search.location = 'bounding_box';
           	}else{
           		$scope.search.location = 'geo_distance';
           		$scope.search.geo = gl;
           	}
        }

        $scope.listTab = function() {
        }

        $scope.fourOhSearch = function() {
            var formData = {};
            formData.data = {};
            formData.method = 'GET';
            formData.apiController = 'search';
            formData.data.search = '';
            formData.params = '?s=100&keyword=&sb=new';
            dataPassage.apiQuery(formData)
                .then(function(data){
                    $('.footable').footable();
                    $scope.searchResults = data.data.results;
                }, function(){
                    $scope.alerts.push({type: 'danger', msg: 'No available jobs at this moment'});
                });
        }

		$scope.searchResults = {};

        var formData = {};
        formData.method = 'get';
        formData.apiController = 'searchFields';
        dataPassage.apiQuery(formData)
            .then(function(data){
                $scope.disciplines = $.map(JSON.parse(data.data.disciplines), function(value, index) {
                    return [value];
                });
                //console.log(JSON.parse(data.data.countries));
                //console.log(JSON.parse(data.data.companies));
            });

		$scope.activateMap = function(mapEnabled)
		{	
            if(!mapSearchQ){            
                $scope.mapSearch();
                mapSearchQ = true;
            }
			$timeout(function () {
				if(!mapEnabled){
					L.mapbox.accessToken = 'pk.eyJ1IjoiYWx0YWJsdWUiLCJhIjoiZTExYjgyYTNlOWQ0Y2Q3OWJiZDIwYjM3ZWViMjNmNGEifQ.S2_ECSlRsa7ow0EhINffOg';
					$scope.map = L.mapbox.map('map', 'mapbox.emerald',{worldCopyJump: true, spiderfyOnMaxZoom:false, minZoom: 1, maxZoom: 18})
					    .setView([40, -74.50], 10)
						.invalidateSize(true);
					$scope.mapEnabled = true;
				}
				var jobLoc = L.layerGroup().addTo($scope.map);
				var markerGroup = [];

				markCluster = new L.markerClusterGroup({
					iconCreateFunction: function(cluster) {
				        return L.mapbox.marker.icon({
				          	'marker-symbol': cluster.getChildCount(),
				          	'marker-color': '#0099CC',
				          	'marker-size': 'large',
				        });
			      	}
				});

				for (var i = 0; i < $scope.mapResults.length; i++) {
					if($scope.mapResults[i]._source.location){
						var venue = $scope.mapResults[i];
						var jsonLat = $scope.mapResults[i]._source.location.split(',');
						//var latlng = L.latLng(jsonLat[0],jsonLat[1]);
						var marker = L.marker(new L.LatLng(jsonLat[0],jsonLat[1]), {
						    icon: L.mapbox.marker.icon({
							    'marker-symbol': 'circle',
							    'marker-size': 'large',
						        'marker-color': '#0099CC'
						    })
						});
						marker.bindPopup('<strong><a href="/job/'+$scope.mapResults[i]._source.category+'/'+$scope.mapResults[i]._source.referencenumber+'">' + $scope.mapResults[i]._source.title + '</a></strong><br/><span>Reference Id: '+$scope.mapResults[i]._source.referencenumber+'<br/>Location: '+$scope.mapResults[i]._source.country+'</span>');
						markerGroup[i] = [jsonLat[0],jsonLat[1]];
						markCluster.addLayer(marker);
					}
				}

				//var bounds = markCluster.getBounds();
				//$scope.map.fitBounds(bounds);

				var latlngbounds = new L.latLngBounds(markerGroup);
				$scope.map.fitBounds(latlngbounds);
				$scope.map.addLayer(markCluster);
			});
		}

		$scope.submitSearch = function($page,$autoSearch)
		{
            searchButton  = 1;
            mapSearchQ = false;
			$page = typeof $page !== 'undefined' ? $page : false;
			$scope.searchLoading = true;
			formData.method = 'get';
			formData.apiController = 'search';
			var search = $scope.search;
            if(typeof $scope.teh.disciplines == 'object'){
                if('value' in $scope.teh.disciplines){
                    $scope.search.disciplines = $scope.teh.disciplines.name;
                }
            }
            if(typeof $scope.teh.s == 'object'){
                $scope.search.s = $scope.teh.s.val;
            }
            if(typeof $scope.teh.sb == 'object'){
                $scope.search.sb = $scope.teh.sb.val;
            }
			var str = "";
			for (var key in search) {
			    if (str != "") {
			        str += "&";
			    }
			    str += key + "=" + encodeURIComponent(search[key]);
			}
			if($page){
				str += '&f='+$page;		
			}
			formData.params = '?'+str;
            if(!$autoSearch){
                str += '&address='+$scope.niceAddy+'&show=true';
                window.history.pushState("", "", '?'+str);
            }
            var state = {};
            dataPassage.apiQuery(formData)
                .then(function(data){
                    $scope.searchEmpty = false;
                	if($scope.mapEnabled){
                		$scope.map.removeLayer(markCluster);
                		$scope.activateMap(true);
                	}
                    $scope.searchLoading = false;
                    //$scope.statement = data.data.statement;
                    if(data.data.status == 'error'){
                        $scope.searchEmpty = true;
                        //$scope.alerts.push({type: 'danger', msg: 'Sorry, no jobs can be found for that search term. Try searching for all jobs using the button above.'});
                    }else{
                        if(!mapSearchQ){
                            if($scope.mapEnabled){
                                $scope.map.removeLayer(markCluster);
                                $scope.activateMap(true);
                            }
                            $scope.mapResults = data.data.results;
                        }
                        $scope.searchEmpty = false;
                        $scope.searchResults = data.data.results;
                        $scope.searchTotal = data.data.total;
                        if($page){$scope.currentPage = $page;}
                    }
                    $location.hash('results');
                    $anchorScroll();
                }, function(data){
                    $scope.searchTotal = 0;
                    $scope.searchLoading = false;
                    $scope.alerts.push({type: 'danger', msg: data.data.error_description});
                   // $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
		}

        $scope.mapSearch = function($page)
        {
            searchButton  = 3;
            $scope.viewMap = true;
            $page = typeof $page !== 'undefined' ? $page : false;
            $scope.alerts = [];
            var formData = {};
            formData.data = {};
            $scope.searchLoading = true;
            formData.method = 'get';
            formData.apiController = 'search';
            var search = $scope.search;
            if(typeof $scope.teh.disciplines == 'object'){
                if('value' in $scope.teh.disciplines){
                    $scope.search.disciplines = $scope.teh.disciplines.name;
                }
            }
            if(typeof $scope.teh.s == 'object'){
                $scope.search.s = $scope.teh.s.val;
            }
            if(typeof $scope.teh.sb == 'object'){
                $scope.search.sb = $scope.teh.sb.val;
            }
            var str = "";
            for (var key in search) {
                if (str != "") {
                    str += "&";
                }
                str += key + "=" + encodeURIComponent(search[key]);
            }
            if($page){
                str += '&s='+search.s+'&f='+$page;     
            }
            formData.params = '?'+str;
            dataPassage.apiQuery(formData)
                .then(function(data){
                    $scope.searchEmpty = false;
                    if($scope.mapEnabled){
                        $scope.map.removeLayer(markCluster);
                        $scope.activateMap(true);
                    }
                    $scope.searchLoading = false;
                    //$scope.statement = data.data.statement;
                    $scope.mapResults = data.data.results;
                    $scope.searchTotal = data.data.total;
                    if($page){$scope.currentPage = $page;}
                    $location.hash('results');
                    $anchorScroll();
                }, function(data){
                    $scope.searchTotal = 0;
                    $scope.searchLoading = false;
                    $scope.alerts.push({type: 'danger', msg: data.data.message});
                   // $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
        }

        $scope.submitSearchAll = function($page){
            searchButton  = 2;
            $page = typeof $page !== 'undefined' ? $page : false;
            $scope.alerts = [];
            var formData = {};
            formData.data = {};
            $scope.searchLoading = true;
            formData.method = 'get';
            formData.apiController = 'search';
            var search = $scope.search;
            var str = "keyword=";
            if(typeof $scope.teh.s == 'object'){
                str += '&s='+$scope.teh.s.val;
            }
            if(typeof $scope.teh.sb == 'object'){
                str += '&sb='+$scope.teh.sb.val;
            }
            if($page){
                str += '&f='+$page;   
            }
            formData.params = '?'+str;
            dataPassage.apiQuery(formData)
                .then(function(data){
                    $scope.searchEmpty = false;
                    if($scope.mapEnabled){
                        $scope.map.removeLayer(markCluster);
                        $scope.activateMap(true);
                    }
                    $scope.searchLoading = false;
                    //$scope.statement = data.data.statement;
                    $scope.searchResults = data.data.results;
                    $scope.searchTotal = data.data.total;
                    if($page){$scope.currentPage = $page;}
                    $location.hash('results');
                    $anchorScroll();
                }, function(data){
                    $scope.searchTotal = 0;
                    $scope.searchLoading = false;
                    $scope.alerts.push({type: 'danger', msg: data.data.error_description});
                   // $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });            
        }

		$scope.pageChanged = function(num) {
            if(searchButton > 2){
                $scope.mapSearch($scope.currentPage);  
            }else if(searchButton == 2){
                $scope.submitSearchAll($scope.currentPage);    
            }else if(searchButton < 2 && searchButton > 0 ){
                $scope.submitSearch($scope.currentPage);    
            }
		};

        $scope.updateLoc = function(place){
            delete $scope.search.geo;
            delete $scope.search.location;
        }

        $scope.filterChange = function(value) {
            if(searchButton > 2){
                $scope.mapSearch();  
            }else if(searchButton == 2){
                $scope.submitSearchAll();    
            }else if(searchButton < 2 && searchButton > 0 ){
                $scope.submitSearch();    
            }
        };


        if($location.search().show){
            if($location.search().show == "true"){
                $scope.submitSearch(0,true);
            }
        }

	}

]).controller('JobPageController', ['$scope', '$stateParams', '$routeParams', '$state', '$location', '$localStorage', '$sessionStorage', '$sce', '$cookies', 'dataPassage', 'job', '$modal', '$anchorScroll',

	function($scope, $stateParams, $routeParams, $state, $location, $localStorage, $sessionStorage, $sce, $cookies, dataPassage, job, $modal, $anchorScroll) {

		$anchorScroll();

		$scope.header.adminHeader = false;

		$scope.job = job.data.job;

		var pattern = /(\d{4})(\d{2})(\d{2})/;

        if(typeof $scope.job.date != 'undefined'){
  		    var tehdate = moment($scope.job.date.replace(pattern, '$1-$2-$3')).format('DD/MM/YYYY');
        }else{
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();

            if(dd<10) {
                dd='0'+dd
            } 

            if(mm<10) {
                mm='0'+mm
            } 

            var tehdate = dd+'/'+mm+'/'+yyyy;
        }

		$scope.job.date = tehdate

		$scope.jobdesc = $sce.trustAsHtml(decodeURIComponent(he.decode($scope.job.description)));

		$scope.applyLoading = false;

		$scope.applyDisabled = false;

        var applied = false;
        var linkedIn = false;
        var iOS = /iPad|iPhone|iPod/.test( navigator.userAgent );

        var modalInstance = '';

        $scope.cancel = function () {
           $modalInstance.dismiss('cancel');
        };

        $scope.linkedInAccessGrant = function()
        {
            if(!applied){
                linkedIn = true;
                IN.User.authorize(function() { $scope.linkedInApply(); });
            }
        }

        $scope.checkReturn = function()
        {
            if($location.search().apply && iOS){
                linkedIn = false;
                IN.User.authorize(function() { $scope.linkedInApply(); });
            }
        }

        function liIOS()
        {
            if(linkedIn){
                linkedIn = false;
                window.location.href = location.href + "?apply=true";
            }
        }

        if(iOS){
            window.onfocus = liIOS;
        }
        
        $scope.linkedSuccess = function(data)
        {
            //$state.go('public.questions', {'job': $scope.job});
            var passedData = {}
            passedData.method = 'post';
            passedData.apiController = 'apply';
            passedData.data = {};
            passedData.data.profile = data;
            passedData.data.client = 'jobboardsite';
            passedData.data.irc = $scope.job.referencenumber;
            if($location.search().bid){
                passedData.data.track = $location.search().bid
            }else{
                passedData.data.track = false
            }
            //passedData.data.access_token = token;
            dataPassage.apiQuery(passedData)
                .then(function(result){
                    modalInstance.dismiss();
                    linkedIn = false;
                    $scope.applyLoading = false;
                    applied = false;
                    $state.go('public.relatedJobs', {'job': $scope.job});
                });
        }

        $scope.cvApply = function()
        {
            modalInstance = $modal.open({
                templateUrl: "js/modules/pages/views/cvUpload.modal.client.view.html",
                backdrop: 'static',
                keyboard: false,
                size: 'lg',
                resolve: {
                    items: function() { return job; }
                },
                controller: 'CvUploadModalController',
            });
            modalInstance.result.then(function (data) {
                modalInstance.dismiss();
                $state.go('public.relatedJobs', {'job': $scope.job});
                //$scope.go('admin.editCampaigns');
                //$state.go('public.relatedJobs', {'job': $stateParams.job});
            }, function (data) {
                modalInstance.dismiss();
            });
        }

        $scope.linkedError = function(error)
        {
            $scope.alerts.push({type: 'danger', msg: error});
            applied = false;
        }

        $scope.linkedInApply = function()
        {
            if(!applied){
                modalInstance = $modal.open({
                    templateUrl: "js/modules/pages/views/processing.modal.client.view.html",
                    backdrop: 'static',
                    keyboard: false,
                    size: 'lg',
                    resolve: {
                        items: function() { return {'message': 'Applying to this position using LinkedIn, please wait.' }}
                    },
                    controller: 'ProcessModalController',
                });
                applied = true;
                $scope.applyLoading = true;
                IN.API.Raw("/people/~:(id,first-name,last-name,headline,email-address,phone-numbers,summary,skills,educations,languages,positions,interests)").result($scope.linkedSuccess).error($scope.linkedError);
            }           
        }

	}

]).controller('CvUploadModalController', ['$scope', 'items', '$modalInstance', '$resource','$http', 'Auth', 'dataPassage', 'Upload', '$cookies', '$state', '$location',
	
	function($scope, items, $modalInstance, $resource,$http, Auth, dataPassage, Upload, $cookies, $state, $location) {

		$scope.progress = false;
		$scope.dataUrls = [];

	    var fileReader = new FileReader();

		$scope.uploadCV = function (files) {
	        $scope.formUpload = true;
	        if (files != null) {
	            upload(files[0])
	        }
	    };

        $scope.cancel = function () {
           $modalInstance.dismiss('cancel');
        };

	    $scope.upload = function (files) {
            if($location.search().bid){
                var track = $location.search().bid
            }else{
                var track = false
            }
	        if (files && files.length) {
	            for (var i = 0; i < files.length; i++) {
	                var file = files[i];
	                $scope.progress = true;
	                Upload.upload({
	                    url: 'api/1.0/apply/upload/',
	                    headers: {'Authorization':'Bearer '+$cookies.publicAccessToken},
	                    sendFieldsAs: 'json',
	                    fields: {'irc': items.data.job.referencenumber, 'email': $scope.email, 'track': track},
	                    file: file
	                }).progress(function (evt) {
	                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	                    console.log('progress: ' + progressPercentage + '% ');
	                }).success(function (data, status, headers, config) {
	                	$modalInstance.close('ok');
	                    //console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
	                }).error(function (data, status, headers, config) {
                        $modalInstance.close('cancel');
                    });
	            }
	        }
    	};	

	}

]).controller('CandQuestionsController', ['$scope', '$stateParams', '$state', '$location', '$localStorage', '$sessionStorage', '$modal', 
	
	function($scope, $stateParams, $state, $location, $localStorage, $sessionStorage, $modal) {

		var item = ['123','hello world'];

		var modalInstance = $modal.open({
            templateUrl: "js/modules/pages/views/questions.modal.client.view.html",
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
			resolve: {
            	items: function() { return $stateParams.job; }
            },
            controller: 'CandQuestionsModalController',
        });
	    modalInstance.result.then(function () {
            modalInstance.dismiss();
            //$scope.go('admin.editCampaigns');
            $state.go('public.relatedJobs', {'job': $stateParams.job});
        }, function () {
            modalInstance.dismiss();
            //$scope.go('admin.editCampaigns');
        });

	}
]).controller('CandQuestionsModalController', ['$scope', 'items', '$modalInstance', '$resource','$http', 'Auth', 'dataPassage',

	function($scope, items, $modalInstance, $resource,$http, Auth, dataPassage) {

		$scope.answer = {};

		$scope.alerts = [];
		var formData = {};
		formData.data = {};
		formData.method = 'get';
		formData.apiController = 'questions';
	    dataPassage.apiQuery(formData)
	        .then(function(data){
	            $scope.questions = data.data.questions;
	            for(var key in data.data.questions){
	            	if($scope.questions[key].query.second === 'is'){
		            	if(items[$scope.questions[key].query.first] != $scope.questions[key].query.third){
		            		delete $scope.questions[key];
		            	}
	            	}else{
		            	if(items[$scope.questions[key].query.first] == $scope.questions[key].query.third){
		            		delete $scope.questions[key];
		            	}
	            	}
	            }
	        }, function(data){
	            $scope.alerts.push({type: 'danger', msg: data.data.error_description});
	           // $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
	        });

		$scope.dismiss = function() {
			$scope.$dismiss();
		};

		$scope.ok = function () {
            $modalInstance.close('ok');
        };

        $scope.cancel = function () {
           $modalInstance.dismiss('cancel');
        };

	    $scope.additionalQsSubmit = function(question){
	    	if(question.$valid){
				$scope.alerts = [];
				var formData = {};
				formData.data = {};
				formData.data.answers = $scope.answer;
				formData.method = 'post';
				formData.apiController = 'questions';
				formData.data.response_type = 'token';
				formData.data.client_id = 'jobboardsite';
				//formData.params = '?access_token='+token; 
			    dataPassage.apiQuery(formData)
			        .then(function(data){
			        	$scope.$close(true);
			        }, function(){
			        	console.log('error');
			        });
	    	}
	    }

	}
]).controller('RelJobsController', ['$scope', '$stateParams', '$resource','$http', 'Auth', 'dataPassage', '$modal', '$timeout', '$anchorScroll',
	
	function($scope, $stateParams, $resource, $http, Auth, dataPassage, $modal, $timeout, $anchorScroll) {

		$scope.theJob = $stateParams.job;

		$scope.alerts = [];

        var markCluster = '';

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

		var modalInstance = $modal.open({
            templateUrl: "js/modules/pages/views/marketing.modal.client.view.html",
            backdrop: true,
            keyboard: true,
            size: 'lg',
			resolve: {
            	items: function() { return $stateParams.job; }
            },
            controller: 'MarketingModalController',
        });
	    modalInstance.result.then(function () {
            modalInstance.dismiss();
        }, function () {
            modalInstance.dismiss();
        });

		var formData = {};
		formData.data = {};
		formData.method = 'GET';
		formData.apiController = 'search';
		formData.data.search = '';
		formData.params = '?disciplines='+$scope.theJob.discipline+'&exclude='+$scope.theJob.referencenumber;
	    dataPassage.apiQuery(formData)
	        .then(function(data){
                if(data.data.status == 'error'){
                    $scope.searchEmpty = true;
                    //$scope.alerts.push({type: 'danger', msg: 'Sorry, no jobs can be found for that search term. Try searching for all jobs using the button above.'});
                }else{
                    $('.footable').footable();
                    $scope.searchEmpty = false;
                    $scope.searchResults = data.data.results;
                    $scope.searchTotal = data.data.total;
                }
	        }, function(){
	        	$scope.alerts.push({type: 'danger', msg: 'No related jobs found for this discipline'});
	        });

		$scope.activateMap = function(mapEnabled)
		{	
			$timeout(function () {
				if(!mapEnabled){
					L.mapbox.accessToken = 'pk.eyJ1IjoiYWx0YWJsdWUiLCJhIjoiZTExYjgyYTNlOWQ0Y2Q3OWJiZDIwYjM3ZWViMjNmNGEifQ.S2_ECSlRsa7ow0EhINffOg';
					$scope.map = L.mapbox.map('map', 'mapbox.emerald',{worldCopyJump: true, spiderfyOnMaxZoom:false, minZoom: 1, maxZoom: 18})
					    .setView([40, -74.50], 10)
						.invalidateSize(true);
					$scope.mapEnabled = true;
				}
				var jobLoc = L.layerGroup().addTo($scope.map);
				var markerGroup = [];

				markCluster = new L.markerClusterGroup({
					iconCreateFunction: function(cluster) {
				        return L.mapbox.marker.icon({
				          	'marker-symbol': cluster.getChildCount(),
				          	'marker-color': '#0099CC',
				          	'marker-size': 'large',
				        });
			      	}
				});

				for (var i = 0; i < $scope.searchResults.length; i++) {
					if($scope.searchResults[i]._source.location){
						var venue = $scope.searchResults[i];
						var jsonLat = $scope.searchResults[i]._source.location.split(',');
						//var latlng = L.latLng(jsonLat[0],jsonLat[1]);
						var marker = L.marker(new L.LatLng(jsonLat[0],jsonLat[1]), {
						    icon: L.mapbox.marker.icon({
							    'marker-symbol': 'circle',
							    'marker-size': 'large',
						        'marker-color': '#0099CC'
						    })
						});
						marker.bindPopup('<strong><a href="/job/'+$scope.searchResults[i]._source.category+'/'+$scope.searchResults[i]._source.referencenumber+'">' + $scope.searchResults[i]._source.title + '</a></strong><br/><span>Extra info</span>');
						markerGroup[i] = [jsonLat[0],jsonLat[1]];
						markCluster.addLayer(marker);
					}
				}

				//var bounds = markCluster.getBounds();
				//$scope.map.fitBounds(bounds);

				var latlngbounds = new L.latLngBounds(markerGroup);
				$scope.map.fitBounds(latlngbounds);
				$scope.map.addLayer(markCluster);
			});
		}

	}

])
.controller('MarketingModalController', ['$scope', 'items', '$modalInstance', '$resource','$http', 'Auth', 'dataPassage',

	function($scope, items, $modalInstance, $resource,$http, Auth, dataPassage) {

		$scope.job = items;

		$scope.subscribe = false;

        $scope.cancel = function () {
           $modalInstance.dismiss('cancel');
        };

	}

]).controller('ProcessModalController', ['$scope', 'items', '$modalInstance', '$resource','$http', 'Auth', 'dataPassage',

	function($scope, items, $modalInstance, $resource,$http, Auth, dataPassage) {

		$scope.message = items;

	}
]);