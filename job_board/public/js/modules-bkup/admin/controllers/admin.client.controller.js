'use strict';

angular.module('admin').controller('AdminController', ['$scope', '$stateParams', '$location', 'dataPassage', '$localStorage', '$sessionStorage', '$sce', '$cookieStore', 'dataPassage',

	function($scope, $stateParams, $location, $dataPassage, $localStorage, $sessionStorage, $sce, $cookieStore, dataPassage) {

		$scope.formData = {}

		$scope.alerts = [];

		$scope.$storage = $sessionStorage;

		if($stateParams.alerts){
			$scope.alerts.push($stateParams.alerts);
		}

		//$scope.$parent.adminHeader = false;
		
		$scope.login = function()
		{
			$scope.alerts = [];
			var formData = {};
			formData.data = {};
			$scope.searchLoading = true;
			formData.method = 'post';
			formData.apiController = 'users';
			formData.apiAction = 'login';
			var search = $scope.search;
			//console.log($scope.formData);
			formData.data.grant_type = 'password';
			formData.data.username = $scope.formData.data.username;
            formData.data.scope = 'write';
			formData.data.password = $scope.formData.data.password;
			formData.data.client_id = 'gsQtGEr70bHTSn1HOY3nioWdEsBgb2vQqD3FJspxtv8';
            dataPassage.apiQuery(formData)
                .then(function(data){
                    $scope.searchLoading = false;
                    $cookieStore.put('access_token', data.data.access_token);
                    $cookieStore.put('refresh_token', data.data.refresh_token);
                    //createCookie('access_token', data.data.access_token, data.data.expires_in);
                   // createCookie('refresh_token', data.data.refresh_token, data.data.expires_in);
                    $scope.header.adminHeader = true;
                    $scope.adminHeader = true;
                    $scope.header.adminAccess = true;
                    $scope.adminAccess = true;
                    $location.path('/admin');
                }, function(data){
                    $scope.searchLoading = false;
                    $scope.alerts.push({type: 'danger', msg: data.data.error_description});
                   // $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
			/*$scope.alerts = [];
			$scope.loginLoading = true;
			$scope.formData.apiMethod = 'post';
            $dataPassage.apiSubmit($scope.formData)
                .then(function(data){
                    $scope.loginLoading = false;
                    $scope.$storage.loginToken = data.data.refresh_token;
                    createCookie('access_token', data.data.access_token, 1);
                    if($scope.formData.remember){
                    	$cookieStore.put('rememberLoginToken', data.data.refresh_token);
                    }
                    $scope.adminHeader = true;
                    $location.path('/admin');
                }, function(data){
                    $scope.loginLoading = false;
                    $scope.alerts.push({type: 'danger', msg: data.data.error_description});
                   // $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });*/
		}

		$scope.passReset = function()
		{
			$scope.loginLoading = true;
		}

	}

]);

function createCookie(name,value,expire) {
  if (expire) {
    var date = new Date();
    date.setTime(date.getTime()+(expire));
    var expires = "; expires="+date.toGMTString();
  }
  else var expires = "";
  document.cookie = name+"="+value+expires+"; path=/";
}