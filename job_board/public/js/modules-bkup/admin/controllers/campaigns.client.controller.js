'use strict';

angular.module('admin').controller('CampsController', ['$scope', '$stateParams', '$state', '$location', 'dataPassage', '$localStorage', '$sessionStorage', '$sce', '$cookies',

	function($scope, $stateParams, $state, $location, $dataPassage, $localStorage, $sessionStorage, $sce, $cookies) {

		$scope.formData = {}

		$scope.alerts = [];

		$scope.$storage = $sessionStorage;

		$scope.header.adminHeader = true;

		$scope.header.adminAccess = true;

		$scope.siteBaseUrl = siteBaseUrl;

	}
])

.controller('CampsListController',  ['$scope', '$stateParams', '$state', '$modal', '$resource','$http', 'Auth', 'dataPassage', 
	function($scope, $stateParams, $state, $modal, $resource,$http, Auth, dataPassage) {

		var newAccess = true 

		$scope.alerts = [];

		if($stateParams.alerts){
			$scope.alerts.push($stateParams.alerts);
		}

		$scope.closeAlert = function(index) {
			$scope.alerts.splice(index, 1);
		};

		$scope.getCampaigns = function()
		{
			$scope.apiLoading = true;
			var createLoading = true;
			var passedData = {};
			passedData.data = {};
			passedData.apiController = 'campaigns';
			passedData.method = 'GET';

			var create = dataPassage.apiQuery(passedData);
				create.then(function(result){
				if(result.data.status == 'success'){
					$scope.list = result.data.campaigns;
					$scope.apiLoading = false;
				}
			});
		}

	}
])

.controller('CampsEditController',  ['$scope', '$stateParams', '$state', '$modal', '$resource', '$http', 'Auth', 'dataPassage', 'campaign', '$parse', 
	function($scope, $stateParams, $state, $modal, $resource, $http, Auth, dataPassage, campaign, $parse) {

		var newAccess = true;

		$scope.campaignData = [];
		$scope.campaignData = campaign.data.campaigns;

		$scope.modules = [];

		$scope.moduleCont = {};

		var modelNames = '';

		$scope.types = [{'name':'Discipline', 'value': 'disc'},{'name':'Client', 'value': 'client'},{'name':'Project', 'value': 'proj'},{'name':'Location', 'value': 'loc'}];
		//$scope.temps = [{'name': 'Template #1', 'value': 2},{'name': 'Template #1', 'value': 1}]

		$scope.campTemplateSetup = function()
		{
			$scope.temps = [];
			var passedData = {};
			passedData.data = {};
			passedData.apiController = 'templates';
			passedData.method = 'GET';
			var getFields = dataPassage.apiQuery(passedData);
				getFields.then(function(result){
					if(result.data.status == 'success'){
						$scope.temps = result.data.templates;

						var types = $.grep($scope.types, function(e){ return e.value == $scope.campaignData.type; });
						var temps = $.grep($scope.temps, function(e){ return e.value == $scope.campaignData.temp; });

						$scope.campaignData.type = types[0];
						$scope.campaignData.temp = temps[0];

						var tempPath = '/modules/template-1/admin/'

						var newAccess = true

						$scope.modelName = [];

						console.log($scope.temps);

					}else{
						$scope.alerts.push({type: 'danger', msg: result.data.message});
						$scope.temps = [{}];
					}
				});
		}

		$scope.campaignFilterFields = function()
		{
			$scope.filters = {};
			var passedData = {};
			passedData.data = {};
			passedData.data = $scope.campaignData;
			passedData.apiController = 'searchFields';
			passedData.method = 'GET';
			var getFields = dataPassage.apiQuery(passedData);
				getFields.then(function(result){
					if(result.data.status == 'success'){
						$scope.filters.category = JSON.parse(result.data.categories);
						$scope.filters.country = JSON.parse(result.data.countries);
						$scope.filters.discipline = JSON.parse(result.data.disciplines);
						$scope.filters.company = JSON.parse(result.data.companies);
					}else{
						$scope.alerts.push({type: 'danger', msg: result.data.message});
					}
				});
		}

        $scope.appendField = function(obj)
        {
			if(typeof $scope.moduleCont[obj].data != 'object'){
				$scope.moduleCont[obj].data = [];
			}
			$scope.moduleCont[obj].data.push({});        		
        }

        $scope.deleteField = function(obj, number)
        {
			$scope.moduleCont[obj].data.splice(number, 1);        		
        }

		$scope.getTempModuleData = function(){
			$scope.apiLoading = true;
			$scope.modules = [];
			var passedData = {};
			passedData.data = {};
			passedData.apiController = 'templates';
			passedData.apiAction = $stateParams.campId;

			passedData.method = 'GET';

			//var acToken = Auth.getAccessToken('access_token');
			var moduleArray = [];

			var create = dataPassage.apiQuery(passedData);
				create.then(function(result){
				if(result.data.status == 'success'){
					$scope.modelName = [];
					$scope.moduleCont = {};
					for(var i = 0; i < result.data.template_data.modules.length || function(){ $scope.modules = result.data.template_data.modules; return false; }(); i++){
						$scope.modelName[i] = result.data.template_data.modules[i].name;
						$scope.moduleCont[result.data.template_data.modules[i].name] = {};
						$scope.moduleCont[result.data.template_data.modules[i].name].type = result.data.template_data.modules[i].params.admin[0].type;
						moduleArray[i] = {}
						moduleArray[i].name = result.data.template_data.modules[i].name;
					}
					passedData.apiController = 'campaigns';
					passedData.apiAction = $stateParams.campId;
					passedData.params = 'content'
					var modData = dataPassage.apiQuery(passedData);
					modData.then(function(result){
						if(Object.keys(result.data.modules).length > 0){
							var resultLength = Object.keys(result.data.modules).length;
							for (var i = 0; i < resultLength; i++) {
								$scope.moduleCont[result.data.modules[moduleArray[i].name].moduleid] = result.data.modules[moduleArray[i].name];
							}
						}
					})
				}
			})
		}

		$scope.updateCampaignContent = function(isValid) {
			var updateLoading = true;
			//if(isValid){
			var createLoading = true;
			var passedData = {};
			passedData.data = {};
			passedData.data.modules = $scope.moduleCont;
			passedData.apiController = 'campaigns';
			passedData.params = $stateParams.campId+'/content/';
			passedData.method = 'PUT';

			var create = dataPassage.apiQuery(passedData);
				create.then(function(result){
				if(result.data.status == 'success'){
					updateLoading = false;
					var alert = {type: 'success', msg: 'Campaign has been updated'};
					$state.go('admin.listCampaigns', {'alerts': alert})
				}else{
					$scope.alerts.push({type: 'danger', msg: result.data.message});
				}
			});
		}

		$scope.updateCampaign = function(isValid) {
			var updateLoading = true;
			//if(isValid){
				var createLoading = true;
				var passedData = {};
				passedData.data = $scope.campaignData;
				passedData.apiController = 'campaigns';
				passedData.params = $stateParams.campId+'/';
				passedData.method = 'PUT';

				var create = dataPassage.apiQuery(passedData);
					create.then(function(result){
					if(result.data.status == 'success'){
						updateLoading = false;
						var alert = {type: 'success', msg: 'Campaign has been updated'};
						$state.go('admin.listCampaigns', {'alerts': alert})
					}else{
						$scope.alerts.push({type: 'danger', msg: result.data.message});
					}
				});

	      	//};
	    }
	}
])

.controller('CampsModalController', ['$stateParams', '$state', '$scope', '$modal', '$resource','$http', 'Auth', 'dataPassage', 

	function($stateParams, $state, $scope, $modal, $resource,$http, Auth, dataPassage) {

		var item = ['123','hello world'];

		var modalInstance = $modal.open({
            templateUrl: "js/modules/admin/views/campaignsModal.create.client.view.html",
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
			resolve: {
            	items: function() { return item; }
            },
            controller: 'CampsCreateController',
        });
	    modalInstance.result.then(function () {
            modalInstance.dismiss();
            $scope.go('admin.editCampaigns');
        }, function () {
            modalInstance.dismiss();
            $scope.go('admin.editCampaigns');
        });
    }

])

.controller('CampsCreateController', ['$scope', 'items', '$modalInstance', '$resource','$http', 'Auth', 'dataPassage',

	function($scope, items, $modalInstance, $resource,$http, Auth, dataPassage) {

		$scope.siteBaseUrl = siteBaseUrl;
		$scope.types = [{'name':'Discipline', 'value': 'disc'},{'name':'Client', 'value': 'client'},{'name':'Project', 'value': 'proj'},{'name':'Location', 'value': 'loc'}]
		$scope.temps = [{'name':'Template #1', 'value': 1}]

		$scope.dismiss = function() {
			$scope.$dismiss();
		};

		$scope.ok = function () {
            $modalInstance.close('ok');
        };

        $scope.cancel = function () {
           $modalInstance.dismiss('cancel');
        };

	    $scope.campConfigSubmit = function(isValid) {
	      	var newAccess = true;
			if(isValid){
				var createLoading = true;
				var passedData = {};
				passedData.data = $scope.campConfig;
				passedData.apiController = 'campaigns';
				passedData.method = 'POST';

				var create = dataPassage.apiQuery(passedData);
					create.then(function(result){
					if(result.data.status == 'success'){
						$scope.$close(true);
					}
				});	

				/*var acToken = Auth.getAccessToken('access_token')
				if(!acToken){
					newAccess = false;
					newAccess = Auth.getNewAccessToken();
					newAccess.then(function(result) {
						passedData.data.access_token = result.data.access_token;
						var create = dataPassage.apiQuery(passedData);
						create.then(function(result){
							if(result.data.status == 'success'){
								$scope.$close(true);
							}
						})
					})
				}else{
					passedData.data.access_token = acToken;*/

				//}
	      	};
	    }
	}
]);