'use strict';

// Setting up route
angular.module('admin').config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', 
	function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

	 	var access = routingConfig.accessLevels;
	 	var item = ['123','hello world'];
	 	var modal = '';

		// Home state routing
		$stateProvider
			.state('public.login', {
				url: '/login',
				controller: 'AdminController',
				params: {
					 alerts: null
				},
				templateUrl: 'js/modules/admin/views/login.client.view.html'
			})
			.state('public.password-reset', {
				url: '/password-reset',
				templateUrl: 'js/modules/admin/views/reset.client.view.html'
			})
			.state('public.logout', {
				url: '/logout',
				controller: function($state, $localStorage, $sessionStorage, $cookieStore, $scope){
					/*delete $sessionStorage.loginToken
					document.cookie = 'access_token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';*/
					$scope.header.adminAccess = false;
					$scope.header.adminHeader = false;
					$cookieStore.remove('access_token');
					$cookieStore.remove('refresh_token');
					document.cookie = 'access_token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
					document.cookie = 'refresh_token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
					var alert = {type: 'info', msg: 'You have been logged out'};
					$state.go('public.login', {'alerts': alert});
				}
			});

		$stateProvider
			.state('admin', {
				abstract: true,
				template: "<ui-view/>",
		        data: {
		            access: access.admin
		        }
			})
			.state('admin.dashboard', {
				url: '/admin',
				templateUrl: 'js/modules/admin/views/dashboard.client.view.html'
			})
			.state('admin.listCampaigns', {
				url: '/admin/campaigns',
				params: {
					alerts: null
				},
				controller: 'CampsListController',
				data: {
		            access: access.admin
		        },
				templateUrl: 'js/modules/admin/views/campaigns.list.client.view.html'
			})
			.state('admin.createCampaigns', {
				url: '/admin/campaigns/create',
				params: {
					alerts: null
				},
				data: {
		            access: access.admin
		        },
				controller: 'CampsModalController'
			})
			.state('admin.applicationQuestions', {
				url: '/admin/questions',
				params: {
					alerts: null
				},
				resolve: {
					questions: function($stateParams, Auth, dataPassage){
						var createLoading = true;
						var passedData = {};
						passedData.data = {};
						passedData.apiController = 'questions';
						passedData.method = 'GET';

						/*var acToken = Auth.getAccessToken('access_token')

						if(!acToken){
							newAccess = false;
							newAccess = Auth.getNewAccessToken();
							newAccess.then(function(result) {
								passedData.params = passedData.params+'?access_token='+result.data.access_token;
								return dataPassage.apiQuery(passedData);
							})
						}else{*/
							//passedData.params = passedData.params//+'?access_token='+acToken;
							return dataPassage.apiQuery(passedData);
						//}
					}
				},
				data: {
		            access: access.admin
		        },
				controller: 'QuestionsController',
				templateUrl: 'js/modules/admin/views/questions.client.view.html'
			})			
			.state('admin.editCampaigns', {
				url: '/admin/campaigns/:campId',
				params: {
					alerts: null
				},
				controller: 'CampsEditController',
				resolve: {
					campaign: function($stateParams, Auth, dataPassage){
						var createLoading = true;
						var passedData = {};
						passedData.data = {};
						passedData.apiController = 'campaigns';
						passedData.method = 'GET';
						passedData.params = $stateParams.campId+'/';

						//var acToken = Auth.getAccessToken('access_token')

						/*if(!acToken){
							newAccess = false;
							newAccess = Auth.getNewAccessToken();
							newAccess.then(function(result) {
								passedData.params = passedData.params+'?access_token='+result.data.access_token;
								return dataPassage.apiQuery(passedData);
							})
						}else{*/
							//passedData.params = passedData.params+'?access_token='+acToken;
							return dataPassage.apiQuery(passedData);
						//}
					}
				},
				data: {
		            access: access.admin
		        },  
				templateUrl: 'js/modules/admin/views/campaigns.create.client.view.html'
			})
			.state('admin.editCampaignsContent', {
				url: '/admin/campaigns/:campId/content/',
				params: {
					alerts: null
				},
				controller: 'CampsEditController',
				resolve: {
					campaign: function($stateParams, Auth, dataPassage){
						var createLoading = true;
						var passedData = {};
						passedData.data = {};
						passedData.apiController = 'campaigns';
						passedData.method = 'GET';
						passedData.params = $stateParams.campId+'/';

						//var acToken = Auth.getAccessToken('access_token')

						/*if(!acToken){
							newAccess = false;
							newAccess = Auth.getNewAccessToken();
							newAccess.then(function(result) {
								passedData.params = passedData.params+'?access_token='+result.data.access_token;
								return dataPassage.apiQuery(passedData);
							})
						}else{*/
							//passedData.params = passedData.params+'?access_token='+acToken;
							return dataPassage.apiQuery(passedData);
						//}
					}
				},
				data: {
		            access: access.admin
		        },  
				templateUrl: 'js/modules/admin/views/campaigns.module.client.view.html'
			})
	}
]);