'use strict';

angular.module('pages').controller('HomepageController', ['$scope', '$stateParams', '$state', '$location', '$localStorage', '$sessionStorage', '$sce', '$cookies', '$cookieStore',

	function($scope, $stateParams, $state, $location, $localStorage, $sessionStorage, $sce, $cookies, $cookieStore) {

		$scope.header.adminHeader = false;

		$scope.slider = function()
		{
			var slides = $scope.slides = [];
			var i = 0
			while(i < 5) {
				var newWidth = 600 + slides.length + 1;
				var newLen = slides.length + 1;
			    slides.push({
			      image: 'holder.js/100%x300/auto/#0099cc:#007AA3/text: campaign #'+ newLen,
			      text: 'campaign #'+(slides.length + 1)
			    });
			    i++;
			}
		}
	}

]).controller('CampaignPageController', ['$scope', '$stateParams', '$state', '$location', '$localStorage', '$sessionStorage', '$sce', '$cookies', 'dataPassage',

	function($scope, $stateParams, $state, $location, $localStorage, $sessionStorage, $sce, $cookies, dataPassage) {

		$scope.header.adminHeader = false;

		$scope.campData = $stateParams.campData;

		$scope.tempModules = {};


		for(var i = 0; i < $stateParams.campData.template_data.modules.length || function(){ $scope.modules = $stateParams.campData.template_data.modules; return false; }(); i++){
			$scope.tempModules[$stateParams.campData.template_data.modules[i].name] = $stateParams.campData.template_data.modules[i].params.front[0];
			$scope.tempModules[$stateParams.campData.template_data.modules[i].name].path = $scope.campData.template_data.path+'/'+$scope.tempModules[$stateParams.campData.template_data.modules[i].name].path
		}

		$scope.getJobs = function(query = false){
			var formData = {};
			formData.params = '?';
			if(typeof query != 'undefined'){
				for (var i = 0; i < query.length; i++) {
					switch(query[i].query){
						case 'is not':
							var querySyn = '!=';
							break;
						default:
							var querySyn = '=';
					}
					formData.params += query[i].attr+querySyn+query[i].value+'&';
				}

				$scope.alerts = [];
				formData.data = {};
				$scope.searchLoading = true;
				formData.method = 'get';
				formData.apiController = 'search';
				formData.params=formData.params.slice(0, - 1);
				
	            dataPassage.apiQuery(formData)
	                .then(function(data){
	                    $scope.searchLoading = false;
	                    $scope.jobs = data.data.results;
	                }, function(data){
	                    $scope.searchLoading = false;
	                    $scope.alerts.push({type: 'danger', msg: data.data.error_description});
	                });
			}
		}

	}

]).controller('SearchPageController', ['$scope', '$stateParams', '$state', '$location', '$localStorage', '$sessionStorage', '$sce', '$cookies', 'dataPassage', 'Auth', '$http', '$timeout',

	function($scope, $stateParams, $state, $location, $localStorage, $sessionStorage, $sce, $cookies, dataPassage, Auth, $http, $timeout) {

		$scope.header.adminHeader = false;

		$scope.tempModules = {};

		$scope.searchLoading = false;

		$scope.search = {};

		$scope.mapEnabled = false;

		//console.log(Auth.getOAuthEncryption());

		$scope.placeChanged = function() {
           var place = this.getPlace();
           $scope.search.location = place.geometry.location.A+','+place.geometry.location.F;
        }

		var formData = {};
		formData.method = 'get';
		formData.apiController = 'searchFields';
        dataPassage.apiQuery(formData)
            .then(function(data){
                $scope.countries = $.map(JSON.parse(data.data.countries), function(value, index) {
				    return [value];
				});
 				$scope.companies = $.map(JSON.parse(data.data.companies), function(value, index) {
				    return [value];
				});
            	//console.log(JSON.parse(data.data.countries));
            	//console.log(JSON.parse(data.data.companies));
            });

		$scope.searchResults = {};

		$scope.activateMap = function(mapEnabled)
		{	
			$timeout(function () {
				if(!mapEnabled){
					L.mapbox.accessToken = 'pk.eyJ1IjoiYWx0YWJsdWUiLCJhIjoiZTExYjgyYTNlOWQ0Y2Q3OWJiZDIwYjM3ZWViMjNmNGEifQ.S2_ECSlRsa7ow0EhINffOg';
					var map = L.mapbox.map('map', 'mapbox.emerald',{worldCopyJump: true})
					    .setView([40, -74.50], 10)
						.invalidateSize(true);
					$scope.mapEnabled = true;
				}

				var jobLoc = L.layerGroup().addTo(map);
				var markerGroup = [];

				for (var i = 0; i < $scope.searchResults.length; i++) {
					if($scope.searchResults[i]._source.location){
						var venue = $scope.searchResults[i];
						var jsonLat = $scope.searchResults[i]._source.location.split(',');
						var latlng = L.latLng(jsonLat[0],jsonLat[1]);
						var marker = L.marker(latlng, {
							icon: L.mapbox.marker.icon({
							    'marker-color': '#0099CC',
							    'marker-symbol': 'circle',
							    'marker-size': 'large'
							  })
							})
							.bindPopup('<strong><a href="/job/'+$scope.searchResults[i]._source.category+'/'+$scope.searchResults[i]._source.referencenumber+'">' + $scope.searchResults[i]._source.title + '</a></strong><br/><span>Extra info</span>')
							.addTo(jobLoc);
						markerGroup[i] = [jsonLat[0],jsonLat[1]];
					}
				}

				var latlngbounds = new L.latLngBounds(markerGroup);
				map.fitBounds(latlngbounds);
			});
		}

		$scope.submitSearch = function()
		{
			$scope.alerts = [];
			var formData = {};
			formData.data = {};
			$scope.searchLoading = true;
			formData.method = 'get';
			formData.apiController = 'search';
			var search = $scope.search;
			var str = "";
			for (var key in search) {
			    if (str != "") {
			        str += "&";
			    }
			    str += key + "=" + encodeURIComponent(search[key]);
			}
			formData.params = '?'+str;
            dataPassage.apiQuery(formData)
                .then(function(data){
                	if(typeof map != 'undefined' && typeof marker != 'undefined'){
                		map.removeLayer(marker);
                	}
                    $scope.searchLoading = false;
                    //$scope.statement = data.data.statement;
                    $scope.searchResults = data.data.results;
                }, function(data){
                    $scope.searchLoading = false;
                    $scope.alerts.push({type: 'danger', msg: data.data.error_description});
                   // $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
		}

	}

]).controller('JobPageController', ['$scope', '$stateParams', '$state', '$location', '$localStorage', '$sessionStorage', '$sce', '$cookies', 'dataPassage', 'job',

	function($scope, $stateParams, $state, $location, $localStorage, $sessionStorage, $sce, $cookies, dataPassage, job) {

		$scope.header.adminHeader = false;

		$scope.job = job.data.job;

		var pattern = /(\d{4})(\d{2})(\d{2})/;
  		var tehdate = moment($scope.job.date.replace(pattern, '$1-$2-$3')).format('DD/MM/YYYY');

		$scope.job.date = moment(tehdate).format('DD/MM/YYYY')

		$scope.jobdesc = decodeURIComponent(he.decode($scope.job.description));

		$scope.applyLoading = false;

		$scope.linkedInAccessGrant = function()
		{
			IN.User.authorize(function() {return false;});
		}

		$scope.linkedSuccess = function(data)
		{
			$state.go('public.questions', {'job': $scope.job});
			/*var passedData = {}
			passedData.method = 'post';
			passedData.apiController = 'apply';
			passedData.data = {};
			passedData.data.profile = data;
			passedData.data.client = 'jobboardsite';
			passedData.data.irc = $scope.job.referencenumber;
			passedData.data.access_token = token;
			dataPassage.apiQuery(passedData)
				.then(function(result){
					$scope.applyLoading = false;
					$state.go('public.questions');
				});*/
		}

		$scope.linkedError = function(error)
		{
			console.log(error);
		}

		$scope.linkedInApply = function()
		{
			$scope.applyLoading = true;
			IN.API.Raw("/people/~:(id,first-name,last-name,headline,email-address,summary,skills,educations,languages,positions,interests)").result($scope.linkedSuccess).error($scope.linkedError);			
		}

	}

]).controller('CandQuestionsController', ['$scope', '$stateParams', '$state', '$location', '$localStorage', '$sessionStorage', '$modal', 
	
	function($scope, $stateParams, $state, $location, $localStorage, $sessionStorage, $modal) {

		var item = ['123','hello world'];

		var modalInstance = $modal.open({
            templateUrl: "js/modules/pages/views/questions.modal.client.view.html",
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
			resolve: {
            	items: function() { return $stateParams.job; }
            },
            controller: 'CandQuestionsModalController',
        });
	    modalInstance.result.then(function () {
            modalInstance.dismiss();
            //$scope.go('admin.editCampaigns');
            $state.go('public.relatedJobs', {'job': $stateParams.job});
        }, function () {
            modalInstance.dismiss();
            //$scope.go('admin.editCampaigns');
        });

	}
]).controller('CandQuestionsModalController', ['$scope', 'items', '$modalInstance', '$resource','$http', 'Auth', 'dataPassage',

	function($scope, items, $modalInstance, $resource,$http, Auth, dataPassage) {

		$scope.answer = {};

		$scope.alerts = [];
		var formData = {};
		formData.data = {};
		formData.method = 'get';
		formData.apiController = 'questions';
	    dataPassage.apiQuery(formData)
	        .then(function(data){
	            $scope.questions = data.data.questions;
	            for(var key in data.data.questions){
	            	if($scope.questions[key].query.second === 'is'){
		            	if(items[$scope.questions[key].query.first] != $scope.questions[key].query.third){
		            		delete $scope.questions[key];
		            	}
	            	}else{
		            	if(items[$scope.questions[key].query.first] == $scope.questions[key].query.third){
		            		delete $scope.questions[key];
		            	}
	            	}
	            }
	        }, function(data){
	            $scope.alerts.push({type: 'danger', msg: data.data.error_description});
	           // $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
	        });

		$scope.dismiss = function() {
			$scope.$dismiss();
		};

		$scope.ok = function () {
            $modalInstance.close('ok');
        };

        $scope.cancel = function () {
           $modalInstance.dismiss('cancel');
        };

	    $scope.additionalQsSubmit = function(question){
	    	if(question.$valid){
				$scope.alerts = [];
				var formData = {};
				formData.data = {};
				formData.data.answers = $scope.answer;
				formData.method = 'post';
				formData.apiController = 'questions';
				formData.data.response_type = 'token';
				formData.data.client_id = 'jobboardsite';
				//formData.params = '?access_token='+token; 
			    dataPassage.apiQuery(formData)
			        .then(function(data){
			        	$scope.$close(true);
			        }, function(){
			        	console.log('error');
			        });
	    	}
	    }

	}
]).controller('RelJobsController', ['$scope', '$stateParams', '$resource','$http', 'Auth', 'dataPassage',
	
	function($scope, $stateParams, $resource, $http, Auth, dataPassage) {

		var job = $stateParams.job;

		$scope.alerts = [];
		var formData = {};
		formData.data = {};
		formData.method = 'post';
		formData.apiController = 'search';
		formData.data.search = '';
	    dataPassage.apiQuery(formData)
	        .then(function(data){
	        	console.log(data)
	        }, function(){
	        	console.log('error');
	        });


	}

]);