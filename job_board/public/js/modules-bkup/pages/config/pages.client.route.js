'use strict';

// Setting up route
angular.module('pages').config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', 
	function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
		// Redirect to home view when route not found
	 	var access = routingConfig.accessLevels;

	    $stateProvider
	        .state('public', {
	            abstract: true,
	            template: "<ui-view/>",
	            data: {
	                access: access.public
	            }
	        })
	        .state('public.home', {
	        	url: '/',
	        	templateUrl: 'js/modules/pages/views/home.client.view.html',
	        })
            .state('public.questions', {
                //url: '/candidateQuestions',
                /*resolve: {
                    questions: function($stateParams, Auth, dataPassage){

                    }
                },*/                                    
                params: {
                    job: null
                },
                controller: 'CandQuestionsController',
            })
            .state('public.relatedJobs', {
                params: {
                    job: null
                },
                controller: 'RelJobsController',
                templateUrl: 'js/modules/pages/views/relJobs.client.view.html',
            })
            .state('public.search', {
                url: '/search',
                templateUrl: 'js/modules/pages/views/search.client.view.html',
            })
            .state('public.jobView', {
                url: '/job/{industry}/{irc}',
                resolve: {
                    job: function($stateParams, Auth, dataPassage){
                        var formData = {};
                        formData.data = {};
                        formData.method = 'get';
                        formData.apiController = 'jobs';
                        formData.apiAction = $stateParams.irc;
                        formData.params = '&html=true'; //'?access_token='+token+'&html=true';
                        return dataPassage.apiQuery(formData);
                    }
                },
                controller: 'JobPageController',
                templateUrl: 'js/modules/pages/views/job.client.view.html',
            })
	        .state('public.404', {
	            url: '/404/',
	            templateUrl: 'js/modules/pages/views/404.client.view.html',
	        })
            .state('public.dynamic', {
                url: '*any',
                controller: function($q, $scope, $location, dataPassage, $state, $resolve, routeCheck, $templateCache, $localStorage){
                    $scope.$storage = $localStorage;
                    var states = $state.get();
                    var check = routeCheck.routeJsonCheck(states,$location.$$path,'url')
                    if(check.hasMatch){
                        var tempCache = $templateCache.get(check.state.templateUrl)
                        var dataCache = $scope.$storage[check.state.name];
                    }else{
                        var tempCache = false;
                        var dataCache = false;
                    }
                    if(!tempCache || !dataCache){
                        var formData = {};
                        formData.data = {};
                        formData.method = 'get';
                        //formData.data.access_token = token;
                        formData.data.jbuser = 'jobboardsite';
                        formData.apiController = 'pages';
                        formData.apiAction = encodeURIComponent($location.$$path.replace(/^\/|\/$/g, ''));
                        //formData.params = '?access_token='+token;
                        dataPassage.apiQuery(formData).success(function(data){
                            if(data.status == 'success'){
                                var state = {
                                    "url": '/'+data.data.campaign_data.url,
                                    "parent": "public",
                                    params: {
                                        campData: null
                                    },
                                    'controller': 'CampaignPageController',
                                    "templateUrl": data.data.template_data.path+'/index.html',
                                };
                                if(!check.hasMatch){
                                    $stateProviderRef.state('public.'+data.data.campaign_data.pagetitle.replace(/\s/g, ''), state); 
                                }
                                $scope.$storage['public.'+data.data.campaign_data.pagetitle.replace(/\s/g, '')] = {'campData':data.data};
                                $state.go('public.'+data.data.campaign_data.pagetitle.replace(/\s/g, ''), {'campData': data.data});
                            }else{
                                console.log('404');
                                //$state.go('public.404');
                            }
                        });
                    }else{
                        $state.go(check.state.name, dataCache);
                    }
                }
            });
	}
]);