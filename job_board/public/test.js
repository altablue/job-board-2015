var routerApp = angular.module('routerApp', ['ui.router']);


routerApp.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
        
    $stateProvider
        
        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/',
            template: '<h1>hello world</h1>'
        })
        
        // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
        .state('about', {
            // we'll get to this in a bit       
        });
        
});

routerApp.run(['$rootScope', '$state',  '$http', '$location',  
    function ($rootScope, $state,  $http,  $location) {

        $rootScope.$on('$stateNotFound', function (event, toState, toParams, fromState, fromParams) {
            console.log('123');
        });

        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams) {
            console.log('789');
        });

    }
]);