<?php
$SITE_ROOT = "https://jobs.alta-blue.com/";

$jsonData = getData($SITE_ROOT);
makePage($jsonData, $SITE_ROOT);
function getData($siteRoot) {
    $id = $_GET['id'];

    $returnCodeReq = exec("curl -XGET 'https://localhost/api/1.0/token/auth/?response_type=code&client_id=k6iIxfjCZ462AWJuxi9eXDx6ocmAfnB2Lh2iUCts9q49m46Q0O&redirect_uri=jobs.alta-blue.com&state=123&scope=read' -k");
    $returnCodeReq = json_decode($returnCodeReq);
    preg_match_all('/code=([^&]+)/', $returnCodeReq->token, $code);
    $returnTokenReq = exec("curl -XPOST 'https://localhost/api/1.0/token/gen/' -d 'grant_type=authorization_code&client_id=k6iIxfjCZ462AWJuxi9eXDx6ocmAfnB2Lh2iUCts9q49m46Q0O&redirect_uri=jobs.alta-blue.com&".$code[0][0]."' -k");
    $returnTokenReq = json_decode($returnTokenReq);

    //return json_encode(array('token' => $returnTokenReq->access_token, 'expires_in' => $returnTokenReq->expires_in, 'refresh_token' => $returnTokenReq->refresh_token));

    //$rawData = exec("curl -XGET 'https://localhost/api/1.0/jobs/".$id."/?html=true&access_token=".$returnTokenReq->access_token."' -d  -k");

    //echo "https://jobs.alta-blue.com/api/1.0/jobs/".$id."/?html=true&access_token=".$returnTokenReq->access_token;

    $rawData = file_get_contents("https://jobs.alta-blue.com/api/1.0/jobs/".$id."/?html=true&access_token=".$returnTokenReq->access_token);
    $json = json_decode($rawData);
    if($json->status == true){
        return $json;
    }else{
        header("HTTP/1.0 404 Not Found");
        die();
    }
}

function makePage($data, $siteRoot) {
    $desc = str_replace('Brief Posting Description  ', '',str_replace('&amp;amp;', '&',str_replace('&nbsp;', ' ',str_replace('&rsquo;', "'", str_replace('&nbsp;&nbsp;', '', htmlspecialchars_decode(str_replace('Â&nbsp;', ' ',str_replace('â€™', "'",htmlentities(htmlspecialchars(strip_tags(rawurldecode($data->job->description)))))))))))); 
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <title>Altablue Job Board | <?php echo $data->job->title; ?></title>

            <meta name="description" content="Altablue Job Board | <?php echo $desc; ?>">

            <link rel="canonical" href="https://jobs.alta-blue.com<?php echo $_GET['url']; ?>" />

            <meta itemprop="name" content="Altablue Job Board | <?php echo $data->job->title; ?>">
            <meta itemprop="description" content="Altablue Job Board | <?php echo $desc; ?>">
            <meta itemprop="image" content="https://jobs.alta-blue.com/_assets/imgs/social/site-prev.jpg">

            <meta name="twitter:card" content="summary_large_image">
            <meta name="twitter:title" content="Altablue Job Board | <?php echo $data->job->title; ?>">
            <meta name="twitter:description" content="Altablue Job Board | <?php echo $desc; ?>">
            <meta name="twitter:image:src" content="https://jobs.alta-blue.com/_assets/imgs/social/site-prev.jpg">

            <meta property="og:title" content="Altablue Job Board | <?php echo $data->job->title; ?>" />
            <meta property="og:type" content="company" />
            <meta property="og:url" content="https://jobs.alta-blue.com<?php echo $_GET['url']; ?>" />
            <meta property="og:description" content="Altablue Job Board | <?php echo $desc; ?>" />
            <meta property="og:site_name" content="Altablue Job Board" />
            <meta property="og:image" content="https://jobs.alta-blue.com/imgs/site-prev.jpg" />
            <!-- etc. -->
        </head>
        <body>
            <h1><?php echo $data->job->title; ?></h1>
            <p><?php echo str_replace('?', '',utf8_decode(str_replace(chr(194)," ",rawurldecode($data->job->description)))); ?></p>
            <img src="https://jobs.alta-blue.com/imgs/site-prev.jpg">
        </body>
    </html>
    <?php
}
?>