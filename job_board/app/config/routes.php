<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

$router = new Phalcon\Mvc\Router(false);

$router->removeExtraSlashes(true);
$router->setDefaultController('index');
$router->setDefaultAction('index');


/**
 * Home page routes
 */

$router->add("/", array(
    'controller' => 'index',
    'action' => 'index',
))->via(array('GET'));

/**
 * Global Route Rules
 */

/*$router->add("/:controller/:params", array(
    'controller' => 1,
    'params' => 2,
))->via(array('GET'));*/

/**
 * Admin login
 */

/*$router->add("/admin/:params", array(
    'controller' => 'admin',
    'action' => 'index',
    'params' => 3,
))->via(array('GET'));*/

/**
 * Password Reset
 */

/*$router->add("/admin/password-reset/:params", array(
    'controller' => 'admin',
    'action' => 'resetPage',
    'params' => 3,
))->via(array('GET'));

$router->add("/api/admin/password/reset/:params", array(
    'controller' => 'admin',
    'action' => 'resetRequest',
    'params' => 3,
))->via(array('POST'));*/

$router->add("/api/1.0/pages/{url}/:params", array(
    'controller' => 'api',
    'action' => 'getPage',
    'params' => 3,
))->via(array('GET'));


$router->add("/api/1.0/token/gen/:params", array(
    'controller' => 'token',
    'action' => 'createToken',
    'params' => 3,
))->via(array('POST'));

$router->add("/api/1.0/token/auth/:params", array(
    'controller' => 'token',
    'action' => 'authorizeRequest',
    'params' => 3,
))->via(array('GET'));

$router->add("/api/1.0/token/auth/:params", array(
    'controller' => 'token',
    'action' => 'authenticateToken',
    'params' => 3,
))->via(array('POST'));



$router->add("/api/1.0/users/create/:params", array(
    'controller' => 'token',
    'action' => 'createUsers',
    'params' => 3,
))->via(array('POST'));

$router->add("/api/1.0/users/login/:params", array(
    'controller' => 'token',
    'action' => 'userLogin',
    'params' => 3,
))->via(array('POST'));



$router->add("/api/1.0/campaigns/:params", array(
    'controller' => 'api',
    'action' => 'createCamp',
    'params' => 3,
))->via(array('POST'));

$router->add("/api/1.0/campaigns/:params", array(
    'controller' => 'api',
    'action' => 'listCamp',
    'params' => 3,
))->via(array('GET'));

$router->add("/api/1.0/campaigns/{id:[0-99999999999]+}/:params", array(
    'controller' => 'api',
    'action' => 'getCamp',
    'params' => 3,
))->via(array('GET'));

$router->add("/api/1.0/campaigns/{id:[0-99999999999]+}/:params", array(
    'controller' => 'api',
    'action' => 'editCamp',
    'params' => 3,
))->via(array('PUT'));

$router->add("/api/1.0/campaigns/{id:[0-99999999999]+}/content/:params", array(
    'controller' => 'api',
    'action' => 'getCampMods',
    'params' => 3,
))->via(array('GET'));

$router->add("/api/1.0/campaigns/{id:[0-99999999999]+}/content/:params", array(
    'controller' => 'api',
    'action' => 'saveCampsMods',
    'params' => 3,
))->via(array('PUT'));



$router->add("/api/1.0/questions/:params", array(
    'controller' => 'api',
    'action' => 'getQuestions',
    'params' => 3,
))->via(array('GET'));

$router->add("/api/1.0/questions/:params", array(
    'controller' => 'api',
    'action' => 'createQuestion',
    'params' => 3,
))->via(array('POST'));

$router->add("/api/1.0/questions/:params", array(
    'controller' => 'api',
    'action' => 'editQuestion',
    'params' => 3,
))->via(array('PUT'));

$router->add("/api/1.0/questions/:params", array(
    'controller' => 'api',
    'action' => 'removeQuestion',
    'params' => 3,
))->via(array('DELETE'));



$router->add("/api/1.0/templates/:params", array(
    'controller' => 'api',
    'action' => 'getTemplates',
    'params' => 3,
))->via(array('GET'));

$router->add("/api/1.0/templates/{id:[0-99999999999]+}/:params", array(
    'controller' => 'api',
    'action' => 'getTempData',
    'params' => 3,
))->via(array('GET'));

$router->add("/api/1.0/searchFields/:params", array(
    'controller' => 'api',
    'action' => 'jobSearchFields',
    'params' => 3,
))->via(array('GET'));



$router->add("/api/1.0/search/:params", array(
    'controller' => 'api',
    'action' => 'jobSearch',
    'params' => 3,
))->via(array('GET', 'POST'));

$router->add("/api/1.0/jobs/{id}/:params", array(
    'controller' => 'api',
    'action' => 'getJobData',
    'params' => 3,
))->via(array('GET'));



$router->add("/api/1.0/apply/:params", array(
    'controller' => 'api',
    'action' => 'generateCVPDF',
    'params' => 3,
))->via(array('POST'));

$router->add("/api/1.0/questions/:params", array(
    'controller' => 'api',
    'action' => 'submitAdditionalQs',
    'params' => 3,
))->via(array('POST'));

$router->add("/api/1.0/apply/upload/:params", array(
    'controller' => 'api',
    'action' => 'uploadCV',
    'params' => 3,
))->via(array('POST'));



