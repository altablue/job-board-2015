<?php 
 $array =array (
  0 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Planner',
      'date' => '20150722',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is supporting one of the UK’s leading Housebuilding and Construction groups with a number of commercial and site based vacancies. Our client boasts a strong reputation for providing whole-life solutions, high standards of project delivery, and an ability to innovate. Our clients operations are organised into five key segments: Building, Infrastructure, Facilities Management, Investments and Integrated Solutions</p><p> </p><p>Altablues client is currently requiring a Planner to work on a large Civil Infrastructure project in the Aberdeen area. The Planner will be responsible for implementing the requirements of the tender and project planning procedures, ensuring compliance with the business management system. To establish and maintain positive and effective relationships with customers and project team members.</p><p> </p><p><b>Detailed Description</b> </p> <p>The Planner is accountable to the Project Lead for:</p><p>         Ensuring that the Contract Programme is produced, managed and maintained in accordance with the planning procedures.</p><p>         The overall planning strategy and the timing &amp; sequence of the Contract Programme activities</p><p>         The issue and communication of the Contract Programme to the Project Lead for onward issue to Client and Project Team</p><p>         Ensuring that all other project planning and programming necessary for the execution of the contract is properly undertaken.</p><p>         Provide support, as required to the BIM Co-ordinators to meet the project requirements or tender submission deliverables</p><p>         Set up appropriate lean visual programme to assist the Project Team to timely deliver projects and improve productivity, performance and safety of projects </p><p>         Ensure a relentless focus on Zero Harm</p><p>         Support the delivery of CSUK’s Sustainability activities</p><p><b>Job Requirements</b> </p> <p>Individuals will require:</p><p>         Operational experience (ie site engineer/site agent) together with previous significant experience of planning services in small to medium sized projects within the construction and engineering industry</p><p>         Proficiency in the use of planning techniques and the required planning toolsets, in particular Primevera 6</p><p>         An understanding of various forms of tender documents, contract documents and specifications.</p><p>         An understanding of various methods of planning, programming and progress monitoring.</p><p>         An understanding of risk and opportunity management with particular reference to planning programming.</p><p>         An understanding of the events that initiate, and of the methods used to demonstrate, delay or change entitlement.</p><p>         An understanding of various forms of tender documents, contract documents and specifications.</p><p>         An understanding of various methods of planning, programming and progress monitoring.</p><p>         Good understanding of risk and opportunity management with particular reference to planning/programming.</p><p>         An understanding of the events that initiate, and of the methods used to demonstrate, compensation event entitlement.</p><p>         An understanding of the construction market, methods of construction, plant, equipment and labour requirements.</p><p>         Relevant CSCS Card</p><p>         An OND/HND in a relevant subject is desirable, as is Professional membership of one or more of the following: RICS/CIOB/RIBA/ICE/CIBSE.</p><p>         Plus additional skills from the CSUK Planning Development and Gap Analysis Tool.</p><p><b>Additional Details</b> </p> <p> </p><p><b>How To Apply</b> </p> <p>Please apply below.</p>',
      'referencenumber' => 'IRC125587',
      'postalcode' => 'AB12 3LE',
      'company' => 'Altablue',
      'discipline' => 'Infrastructure',
      'country' => 'GB',
      'location' => '57.115460279639,-2.078630348308',
    ),
    'attributes' => 
    array (
    ),
  ),
  1 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Agent',
      'date' => '20150730',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is supporting one of the UK’s leading Construction groups with a number of commercial and site based vacancies. Our client boasts a strong reputation for providing whole-life solutions, high standards of project delivery, and an ability to innovate. Our clients operations are organised into five key segments: Building, Infrastructure, Facilities Management, Investments and Integrated Solutions</p><p> </p><p>The Agent will take full responsibility for production functions on contracts allocated by the Contract / Project Managers from award through to the issue of the Maintenance Certificate and to maintain and maximise quality of service and profitability.</p><p><b>Detailed Description</b> </p> <p>Responsibilities include:</p><p> </p><ul><li>Budget Responsibility - agree budget for section; monitor and control costs within agreed budget</li><li>Ensures that sufficient resources are allocated to the Works Section and that each has an appropriate level of ability</li><li>Identifies and arranges training as required</li><li>Sets targets and KPI’s in line with Project objectives and budgets and monitors performance</li><li>Ensures site audits are undertaken and takes appropriate action on the findings</li><li>Appraises improvement options and leads / instigates actions as appropriate</li><li>Implements systems and processes</li><li>Involved in working groups focused on Improvement Areas</li><li>Carries out periodic Safety &amp; Environment Tours as required </li><li>Leads / Reviews Accident / Incident Investigations. Reviews and appraises findings and leads / directs the appropriate action arising</li><li>Appraises improvement options and leads action / instigates as appropriate</li><li>Leads periodic risk reviews and takes output forward to the Project Risk Register</li><li>Monitors effectiveness of the H &amp; S management System</li><li>Keeps up to date with changes in legislation and cascades information to the team</li><li>Sets targets and KPI’s in line with key outputs and monitors performance</li><li>Carry out constructive analysis of fully resourced programme</li><li>Manages sub-contractor performance based on KPI’s </li><li>Attends weekly cost meetings and identifies improvement areas and allocates appropriate actions to team members</li><li>Prepares a cost plan and monitors against plan</li><li>Involved in the procurement of subcontractors and advises on commercial aspects / preferred attendances</li><li>Inputs to risk schedule for section, reviews and updates</li></ul><p><b>Job Requirements</b> </p> <p>Knowledge of / experience in    </p><p> </p><ul><li>Experienced in resource allocation and up skilling</li><li>Good knowledge of specification for highway works with emphasis on highway elements</li><li>Earthworks</li><li>Drainage</li><li>Pavement</li><li>Understands method statements</li><li>Understands the principles of Accident / Incident Investigation</li><li>Good working knowledge of current H &amp; S legislation</li><li>Able to analyse and offer improvement on a fully resourced stage programme in the accepted format / software</li><li>Understands sub-contractor KPI’s and has experience of using them to manage performance</li><li>Used to setting targets in line with budget</li><li>Able to produce a realistic financial forecast</li><li>Working knowledge of the NEC and other relevant forms of contract</li></ul><p> </p><p><b>Additional Details</b> </p> <p>Competencies/personal attributes       </p><ul><li>Team player</li><li>Decision maker within defined boundaries</li><li>Able to lead and motivate their team</li><li>Excellent communication skills with internal and external project stakeholders</li><li>Able to manage and deal with HR issues with team members</li></ul><p> </p><p>Qualifications</p><p> </p>HNC/HND/1st Degree in relevant discipline<p>CSCS card holder at relevant level</p><p>5 day CITB Site Management Safety Scheme</p><p><b>How To Apply</b> </p> <p> </p>',
      'referencenumber' => 'IRC125135',
      'postalcode' => 'AB12 3LE',
      'company' => 'Altablue',
      'discipline' => 'Infrastructure',
      'country' => 'GB',
      'location' => '57.115460279639,-2.078630348308',
    ),
    'attributes' => 
    array (
    ),
  ),
  2 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Quality Assurance Engineer',
      'date' => '20150629',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering recruitment services for our clients with a focus on relationships and collaboration. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p>Altablue is supporting one of the UK’s leading Housebuilding and Construction groups with a number of commercial and site based vacancies. Our client boasts a strong reputation for providing whole-life solutions, high standards of project delivery, and an ability to innovate. Our clients operations are organised into five key segments: Building, Infrastructure, Facilities Management, Investments and Integrated Solutions. </p><p>Altablue are currently looking for Quality Assurance Engineer to support our client with an upcoming project. The purpose of this role is to provide either Project or Business Stream leadership and expertise in the area of Quality specifically to ensure the systems in place are properly executed throughout the project or business. </p><p><b>Detailed Description</b> </p> <ul><li>Ensure that the process is in place to deliver right first time to client specification, in conjunction with the Project Manager as appropriate. Where required provide training and put measures in place to support this Assist the Project Manager to ensure inspection and test plans are in place </li><li>Ensure systems are in place, in conjunction with the Project Manager as appropriate, which allow the required specification to be clearly agreed with customers, customers’ technical representatives, project, sub-contractor supply chain</li><li>Ensure systems are in place to allow sample and benchmark plans to be established and agreed with customer, and sub-contractor supply chain </li><li>Assist in developing specific project / task quality plans </li><li>Undertake assurance that the Quality Plan is being followed and that requirements are being met </li><li>Support the development of either Project or Business Stream Quality strategies and plans and the implementation of measurement frameworks across the scope of services and full project lifecycle </li><li>Support the Quality team in dealing with external auditing governing bodies to maintain existing certification and continuous improvement </li><li>Provide quality improvement performance measures and recommendations, and share best practice </li><li>By using the principles of quality assurance, and where appropriate technical Integrity, ensure the customer requirements are clearly understood and that Quality Control mechanisms are in place that provide assurance that the asset and/or service delivers as designed/required </li><li>Produce project and/or business quality reports as required </li><li>Support the delivery of business stream quality programs and actions </li><li>Assist in training, coaching &amp; developing quality skills in business streams </li><li>Support the establishment of a continuous improvement culture </li><li>Undertake specific investigations and/or report on quality related issues as required. </li><li>Drive efficiencies through early project involvement and explore options to provide lean construction by understanding customers’ needs and expectations </li><li>Understand Lean principles and how to apply them in construction </li><li>Seek to understand and implement the appropriate digital toolset to support the project governance and quality assurance/ measures on the projects </li><li>Ensure a relentless focus on Zero Harm </li></ul><p><b>Job Requirements</b> </p> <ul><li>The following qualities/experience are essential: </li><li>Ability to build strong relationships with delivery teams and project managers </li><li>Substantial practical experience in quality management in a relevant business environment </li><li>Experience of building a quality management framework &amp; KPI measurement </li><li>Ability to gather, analyse and evaluate facts </li><li>Pays attention to detail and sees things through to completion </li><li>Can deliver reports orally and written in an objective and persuasive manner </li><li>Has good interpersonal and communication skills and acts in an independent and professional manner with the ability to influence others including senior management </li><li>Is a self-starter who manages their own time and seeks to add value to their function </li><li>Has an enquiring and questioning approach to audit work </li><li>Able to command respect across the business and work closely with the Business Efficiency team </li><li>Good at building relationships and able to influence effectively </li><li>Can demonstrate initiative and resilience </li><li>Knowledge of the sector or experience within a project based organisation </li><li>An understanding of BIM and how to read and assemble BIM models and produce appropriate reports from them is desirable. </li><li>Results Driven: Demonstrates a passion and excitement for his/her work. Tackles problems head-on and works to resolve them without delay. </li><li>Focus on Excellence: Willing to go the extra mile to exceed expectations. Continually searches for ways to add value and take performance to the next level. </li><li>Teamwork &amp; Collaboration: Puts aside personal agendas to work for the benefit of customers and suppliers and other stakeholders. Proactively builds knowledge through sharing knowledge, ideas and expertise with others. </li><li>Customer Focus: Is dedicated to meeting the expectations and requirements of internal and external customers. Actively collects customer information and uses it to improve services and solutions. Manages the experience of customers to ensure positive relationships are established and maintained. </li><li>Operational Excellence: Plans and organises work to safely achieve maximum efficiency and output. Delivers results consistently. </li><li>Adaptability: Learns quickly. Adapts positively to changing business and customer demands. Is energised by change. </li></ul><p><b>Additional Details</b> </p> <p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC125972 in all correspondence relating to this position.</p><p>Internal applicants must discuss their application with their line manager prior to applying.</p><p>Online applications only - no email applications</p>',
      'referencenumber' => 'IRC125972',
      'postalcode' => 'AB12 3LE',
      'company' => 'Altablue',
      'discipline' => 'Infrastructure',
      'country' => 'GB',
      'location' => '57.115460279639,-2.078630348308',
    ),
    'attributes' => 
    array (
    ),
  ),
  3 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Workforce Solutions Manager',
      'date' => '20150706',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>An exciting opportunity has arisen for an experienced Workforce Solutions Manager to join Altablue for the opening of our new Houston office.</p><p>The role will be accountable for the management of existing clients and contractors within, and for the continuous improvement and growth of, Altablue’s workforce solutions business.  </p><p>Overall accountability for the Workforce Solutions service line within assigned region, including:</p><ul><li>Setting of budget and growth targets for the service line</li><li>Responsible for delivering budget for Workforce Solutions Service line</li><li>Development of Workforce Solutions service offering in line with industry and technological developments</li><li>Marketing Workforce Solutions service line to new and potential customers</li><li>Management and supervision of Contractor Services Team </li><li>Contractor administration, timewriting, payroll, benefits and expenses</li><li>Contractor queries, HR and HSE support as required by local legislation and best practice in assigned region</li></ul><p><b>Detailed Description</b> </p> <p>The Workforce Solutions Manager will implement strategic initiatives to develop existing business services and capability, ensuring a best practice approach in areas such as health &amp; safety, contractor engagement and onboarding, contractor care, quality and reporting.  They will actively support Business Development to deliver winning bids and implement new client accounts; designing and costing appropriate solutions, selecting and managing required vendors and working with clients to ensure successful implementation.  </p><p><strong>Responsibilities</strong></p><ul><li>Ensure compliance with relevant HSE and HR legislation, actively looking for opportunities to continuously improve performance.</li><li>Ongoing development of robust policies and procedures to protect our workforce</li><li>Support clients and ensure that they are meeting their obligations with respect to the on-hire of contract workers.</li><li>Promote and seek opportunities for organic growth within existing customers, and opportunities to provide services to new customers</li><li>Develop robust understanding of markets, companies, clients and industries</li><li>Support bids with relevant solution design and commercial content</li><li>Continuously look for opportunities to extend our service offering in line with market drivers.</li></ul><p><strong>Quality &amp; Performance Management</strong></p><p><strong></strong></p><strong></strong><ul><li>Setting and management of contractor services team KPI’s / objectives</li><li>Engage with existing clients and contractors to monitor performance</li><li>Design, governance and assurance of business processes, policies and procedures utilised in Altablue</li><li>Ensuring timely internal and external reporting  </li></ul><p><strong>Service Delivery</strong></p><p><strong></strong></p><strong></strong><ul><li>Ensure the ongoing wellbeing of the contractor workforce through regular monitoring and reporting</li><li>Ensure timely resolution of issues and queries</li><li>Finalise and sign off of agreed rates / reviews / expenses</li><li>Look for opportunities to refine and develop systems and processes to ensure efficiency and profitability</li><li>Lead and manage the contractor care team to develop strong and mutually beneficial relationships with clients and contractors</li><li>Manage relationships with key vendors to ensure quality, value and consistency of service.</li><li>Ensure the thorough planning and implementation of all new client solutions</li></ul><p><b>Job Requirements</b> </p> <ul><li>Extensive experience in account management within a contractor payrolling organisation eg PEO / Umbrella Company / High Volume contractor Agency / RPO provider / MSP provider</li><li>Broad understanding of legislation applicable to payrolling contractor personnel in the assigned region, including HR, HSE and tax requirements</li><li>Participative and inclusive team leader with proven experience in developing customer-facing teams</li><li>Commitment to delivering outstanding service and driving continuous improvement</li><li>Innovative problem-solver</li><li>Goal oriented, passionate and credible</li></ul><p><b>Additional Details</b> </p> <p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC in all correspondence relating to this position.</p><p>Internal applicants must discuss their application with their line manager prior to applying.</p><p>Online applications only - no email applications</p>',
      'referencenumber' => 'IRC126016',
      'postalcode' => '77094',
      'company' => 'Altablue',
      'discipline' => 'Altablue',
      'country' => 'US',
      'location' => '29.7766082,-95.6883868',
    ),
    'attributes' => 
    array (
    ),
  ),
  4 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Regional Finance Manager - US',
      'date' => '20150720',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue have an exciting opportunity for an experienced Finance Manager to join their new Houston based office. </p><p>The Finance Manager will be responsible for overall financial management of assigned regional operations of Altablue, including;</p><p>         Compliance with all statutory reporting and filing requirements</p><p>         Preparation of regional budget, forecasts and monthly reporting according to the Group Financial calendar </p><p>         Billing and accounts receivable</p><p>         Purchasing and accounts payable</p><p>         Staff timewriting, payroll and expenses</p><p>The role carries profit and loss and balance sheet responsibilities for the applicable regional operation and responsibility for resourcing and development of the team. The other major aspects of the role include all management and statutory reporting, financial forecasting and cash collection. </p><p><b>Detailed Description</b> </p> <p>         Management of Regional finance team including recruitment, staff development and the setting of performance objectives</p><p>         Preparation of monthly management accounts</p><p>         Ensuring monthly client invoices are prepared in accordance with the commercial terms of the contract</p><p>         Report financial performance to Senior management including variance analysis, accounts receivables, DSO, accruals and unbilled analysis</p><p>         Preparation of annual budget, forecasts and rolling forecasts in conjunction with Senior Management</p><p>         Optimisation of cash collection and minimising DSO</p><p>         Liaison with external and internal auditors</p><p>         Completion of all monthly balance sheet reconciliations</p><p>         Provision of commercial assistance to senior management – look for opportunities to drive margin improvements and commercial efficiencies</p><p>         Preparation of statutory accounts</p><p>         Preparation &amp; filing of all statutory returns</p><p>         Assist in providing information to tax department, including preparation of the annual pack &amp; other tax returns</p><p>         Be responsible for all Hyperion financial reporting </p><p>         Be responsible for all transactional processing including, accounts payable, timewriting, payroll &amp; expenses. Or the management of a contract for the provision of these services</p><p>         To supply financial input to all projects as required</p><p>         Execution of various ad hoc tasks as and when required</p><p><b>Job Requirements</b> </p> <p>         Previous experience in a similar role</p><p>         Recognised Accountancy qualification </p><p>         Strong technical and analytical skills</p><p>         Must be highly organised, able to prioritise tasks, multi-task and work under minimal supervision</p><p>         Must have excellent verbal and communication skills</p><p>         Must have good interpersonal skills to build good working relationships within teams, across the wider group and with external contacts</p><p>         Must be able to meet deadlines without compromising on accuracy</p><p>         Staff development and coaching skills</p><p>         Attention to detail</p><p> </p><p><b>Additional Details</b> </p>  <p><b>How To Apply</b> </p> <p>Please quote job reference IRC126026 in all correspondence relating to this position.</p><p>For more information, please contact Morag Reglinski-Gill on morag.reglinski-gill@alta-blue.com</p> ',
      'referencenumber' => 'IRC126026',
      'postalcode' => '77094',
      'company' => 'Altablue',
      'discipline' => 'Altablue',
      'country' => 'US',
      'location' => '29.7766082,-95.6883868',
    ),
    'attributes' => 
    array (
    ),
  ),
  5 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Asset Planning Manager',
      'date' => '20150722',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is supporting amajor client with a number of new vacancies in the North East of Scotland.Our client is a significant player in water and waste water operations acrossthe UK.</p><p>Altablue is currentlyrecruiting for an experienced Asset Planning Manager to support our clientwith a growing project portfolio.</p><p> </p><p><b>Detailed Description</b> </p> <ul><li>Support and motivate the Asset Planning Team</li><li>Manage and direct a team of Process Engineers / Maintenance Engineers and the on-site contractors</li><li>Work alongsidethe Operations Manager on the delivery and planning of the Operations plan</li><li>Plan the Asset management and investment strategies</li><li>Look to cut costs from the investment plans</li><li>Provide leadership with regards to health, safety, compliance, permits to work and handovers.</li></ul><p><b>Job Requirements</b> </p> <ul><li>A proven background in asset management, operations and project management</li><li>A strong leader with experience within the water industry or other complex assetbased industry similar</li><li>IOSH qualification</li><li>A backgroundin a target driven environment with regard to business and team work</li><li>Have excellent communication and networking skills</li><li>Knowledge of waste water processes is essential</li><li>Proven background in the water industry</li><li>Full EU driving license as travel is required between sites across the country</li></ul><p><b>Additional Details</b> </p> <p>Career development, employeewell-being and a positive work experience for all are key priorities for ourclient. They pride themselves on being a business that is continuously evolvingand improving their way of working and they are excited about bringing inindividuals who can support them in achieving their vision for thefuture.  </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC126400 in all correspondence relating to this position.</p><p>Internal applicants must discuss their application with their line manager prior to applying.</p><p>Online applications and email applications - <a>rosy.thomson@alta-blue.com</a></p><p> </p>',
      'referencenumber' => 'IRC126400',
      'postalcode' => 'AB12 3LE',
      'company' => 'Altablue',
      'discipline' => 'Utilities',
      'country' => 'GB',
      'location' => '57.115460279639,-2.078630348308',
    ),
    'attributes' => 
    array (
    ),
  ),
  6 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'General Manager',
      'date' => '20150713',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue issupporting a major client with a number of new vacancies in the North Eastof Scotland. Our client is a significant player in water and waste water operationsacross the UK.</p><p>Altablue iscurrently recruiting for an experienced General Manager to support ourclient with a growing project portfolio</p><p> </p><p><b>Detailed Description</b> </p> <p>         Drive compliance and excellence within the operational performance of our client’s waste water assets</p><p>         Ensure that contractual and regulatory requirements of the business, the client and the regulator are met and maintained. </p><p>         Manage and develop key stakeholder relationships with the client and regulator organisations.</p><p>         Build and maintain positive relationships with the local community, as well as colleagues across the wider company</p><p>         Represent the business and work to further its interests in the wider business and public communities. </p><p>         Manage key interfaces with project funders and ensure that all aspects of financial control and reporting are adhered to. </p><p>         Ensure compliance with regulatory, financial and legal requirements </p><p><b>Job Requirements</b> </p> <p>         A credible leader with strong influencing skills</p><p>         An Operational/Process Engineering background with experience of developing and implementing business strategy, transformation and change</p><p>         A strong track record of stakeholder management with excellent communication and networking skills.</p><p>         Sound understanding of health and safety legislation (IOSH qualification preferred)</p><p>         Proven delivery against challenging business targets</p><p>         Ability to demonstrate strong financial and budget management control </p><p>         Previous experience in the water industry and in particular knowledge of waste water processes is essential.  </p><p>         As this role will require travel between sites, it is essential to have a full EU driving license.</p><p><b>Additional Details</b> </p> <p>Career development, employee well-being and a positive work experience for all are key priorities for our client. They pride themselves on being a business that is continuously evolving and improving their way of working and they are excited about bringing in individuals who can support them in achieving their vision for the future.  </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC126401 in all correspondence relating to this position.</p><p>Internal applicants must discuss their application with their line manager prior to applying.</p><p>Online applications and email applications - <a>rosy.thomson@alta-blue.com</a></p><p> </p>',
      'referencenumber' => 'IRC126401',
      'postalcode' => 'AB12 3LE',
      'company' => 'Altablue',
      'discipline' => 'Utilities',
      'country' => 'GB',
      'location' => '57.115460279639,-2.078630348308',
    ),
    'attributes' => 
    array (
    ),
  ),
  7 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Senior Air Quality Consultant',
      'date' => '20150707',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering recruitment services for our clients with a focus on relationships and collaboration. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity.</p><p>We are delighted to be supporting a rapidly growing environmental consultancy with vacancies across several offices throughout the UK. Due to the continued success and growth of the air quality consultancy business, there are a number of opportunities for air quality specialists of all levels to join the team.</p><p>Altablue are currently looking for a Senior Air Quality Consultant based in Manchester. The successful candidates will be qualified in a relevant environmental discipline to Degree or Masters Level and have experience and working knowledge of dispersion modelling of roads and point sources. </p><p><b>Detailed Description</b> </p> <p>         Undertaking air quality assessments</p><p>         Dispersion modelling</p><p>         Data analysis and interpretation</p><p>         Preparation of factual and interpretative reports</p><p>         Management of multiple projects</p><p>         Production of quotes and tenders</p><p>         Development of client relationships</p> <p><b>Job Requirements</b> </p> <p>         Atmospheric science or environmental based degree</p><p>         Excellent written and communication skills</p><p>         Proficiency in data analysis and interpretation</p><p>         Consultant level - at least 2-years air quality consultancy experience</p><p>         Senior Consultant level - at least 5-years air quality consultancy experience</p><p>         Able to manage own projects from initial proposal to final invoice</p><p>         Knowledge of standard methodologies for assessment of roads, industrial and dust emissions</p><p>         Membership of relevant professional body (IAQM/IEMA)</p><p>         Experience of business development/marketing would be advantageous</p><p>         Current Full Clean Driving License</p><p> </p><p><b>Additional Details</b> </p> <p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC126 498 in all correspondence relating to this position</p><p>Please contact Lauren Allen on <a>Lauren.allen@alta-blue.com</a> or on 01224 778144 for more details. </p><p> </p><p> </p>',
      'referencenumber' => 'IRC126498',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Life Sciences',
      'country' => 'GB',
      'location' => '53.4791466,-2.2447445',
    ),
    'attributes' => 
    array (
    ),
  ),
  8 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Geo-Environmental Consultant',
      'date' => '20150730',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering recruitment services for our clients with a focus on relationships and collaboration. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity.</p><p>Altablue is supporting a rapidly growing environmental consultancy with a number of new vacancies across several offices throughout the UK. The role will require the individual to take part in the full project life cycle ranging from site works through to input on interpretive reports. Your role within the team will allow for the development of technical understanding of both geotechnical and environmental aspects enabling the successful candidate to achieve a fuller knowledge of the geo-environmental industry.</p><p> </p> <p><b>Detailed Description</b> </p> <p>The desired candidate will have a minimum of 2 years relevant experience and be in possession of Chartership status with a recognised professional body. Experience in the design, planning and delivery of contaminated land desk studies, site investigations and risk assessment is essential. Experience of geotechnical aspects and remediation projects would be advantageous but not essential. </p><p><b>Job Requirements</b> </p> <p>You should be ambitious, happy to work on your own demonstrating initiative, with a desire to contribute to the ongoing growth of the business. Strong communication skills and the ability to build effective relationships with suppliers, clients and peers at all levels are vital.</p><p><b>Additional Details</b> </p> <p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC126505 in all correspondence relating to this position.</p><p> </p><p>Please contact Lauren Allen on <a>Lauren.allen@alta-blue.com</a> or on 01224 778144 for more details</p><p> </p>',
      'referencenumber' => 'IRC126505',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Life Sciences',
      'country' => 'GB',
      'location' => '53.4791466,-2.2447445',
    ),
    'attributes' => 
    array (
    ),
  ),
  9 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Business Development Manager',
      'date' => '20150707',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is supporting a rapidly growing environmental consultancy with vacancies across several offices throughout the UK. We currently have an exciting opportunity based in the Midlands for a Business Development Manager for one of the leading Asbestos management and testing consultancies. Due to continued growth within this sector, our client is keen to engage with Business development professionals with a background in Asbestos. </p><p> </p> <p><b>Detailed Description</b> </p> <p> </p>The ideal candidate will be highly motivated, have strong commercial awareness, marketing acumen and a natural ability to build networks with the ability to plan and implement agreed strategies. <p> </p><p>Reporting to the Business Development Director, you will be joining an experienced team of environmental professionals and will work with the team to build on the existing reputation of the business, to promote the brand, to develop new client contacts and to secure new business.</p><p> </p><p>The ideal candidate must be in possession of a Full UK Driving license and be willing to travel to different office locations.</p> <p><b>Job Requirements</b> </p> <p>•              Generating and identifying new business opportunities</p><p>•              Winning new business working closely with the operational and technical teams</p><p>•              Maintaining extensive knowledge of current market conditions</p><p>•              Attending relevant industry/customer events </p><p>•              Cross-selling the company\'s consultancy services to clients</p><p>•              Building and maintaining excellent business relationships with clients</p><p>•              Building and developing the brand and reputation of the organisation</p><p>•              Preparing weekly/monthly reports in order to communicate progress, activity, operations, outcomes in line with Business Strategy and business KPI’s</p><p> </p><p>The successful candidate will be able to demonstrate the following:</p><p> </p><p>•              Strong track-record of sales growth within the asbestos market</p><p>•              Proven marketing skills </p><p>•              Excellent communication and client facing skills </p><p>•              A strong desire and aptitude to seek out new business opportunities and effectively promote the company’s services</p><p>•              A keen eye for detail with excellent interpersonal and communication skills</p><p>•              A team player, who can adapt and respond to the needs of the business</p><p>•              A strong and committed work ethic, an ability to work autonomously and a drive to succeed</p> <p><b>Additional Details</b> </p>  <p><b>How To Apply</b> </p> <p>Please quote job reference IRC126507 in all correspondence relating to this position</p><p> </p><p>For further information prior to submitting your application please contact <a>Lauren.allen@alta-blue.com</a></p>',
      'referencenumber' => 'IRC126507',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Life Sciences',
      'country' => 'GB',
      'location' => '52.4813679,-1.8980726',
    ),
    'attributes' => 
    array (
    ),
  ),
  10 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Ecologist',
      'date' => '20150708',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering recruitment services for our clients with a focus on relationships and collaboration. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity.</p><p>Altablue is supporting a rapidly growing environmental consultancy with a number of new vacancies across several offices throughout the UK.</p><p><b>Detailed Description</b> </p> <p>Reporting to the Principal Ecologist in project delivery and business development, the role of the Ecologist will primarily involve undertaking ecology related fieldwork at sites throughout the UK as required, reporting, managing the technical and financial performance of ecological projects and providing technical support to the wider ecology team. </p><p>The expectation of the Ecologist is also the ability to manage ecological projects independently as well as providing expertise on multidisciplinary projects.</p><p> </p><p><b>Job Requirements</b> </p> <p>         CIEEM membership</p><p>         Specialism in a field of ecology and at least one EPS survey licence (great crested newt, bats and / or dormouse)</p><p>         Advanced botanical identification skills</p><p>         Experience with monitoring less experience members of the team</p><p>         Demonstrable project management experience varying from small developments to large scale/multi-faceted schemes</p><p>         An ability to critically review their own work and experience of reviewing others work, including ecological appraisals and protected species survey reports</p><p>         PTS / CSCS card holder</p><p>         Excellent report writing skills</p><p>         Comfortable working within a team, or alone</p><p>         Excellent interpersonal skills</p><p>         Ability to manage own time, and to mentor and motivate others</p><p>         An honours degree in ecology or a closely related field</p><p>         British fauna and flora identification skills (subject to grade)</p><p>         Knowledge of UK wildlife legislation and policy</p><p>         Good communication and IT skills</p><p>         Current Full Clean Driving License</p><p><b>Additional Details</b> </p> <p>In addition to the opportunity to work with one of the fastest growing ecology teams in the UK, you will be offered a competitive salary based on experience, an annual bonus based on performance (applicable to senior staff), professional development opportunities and pay reviews in line with progression and several additional benefits related to the working environment.</p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC126506 in all correspondence relating to this position.</p><p>Please contact Lauren Allen on <a>Lauren.allen@alta-blue.com</a> or on 01224 778144 for more details. </p><p> </p>',
      'referencenumber' => 'IRC126506',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Life Sciences',
      'country' => 'GB',
      'location' => '52.4813679,-1.8980726',
    ),
    'attributes' => 
    array (
    ),
  ),
  11 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Operations Coordinator',
      'date' => '20150717',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p> </p><p>We are delighted to be supporting one of our clients with a requirement for an Operations Coordinator our client is a global organization which specialize in Rig inspection and consultancy work for the Oil and Gas industry. This position is being offered on a permanent basis and will be located in Aberdeen.</p><p>This will be an excellent opportunity for a self-motivated, enthusiastic and driven individual.</p> <p><b>Detailed Description</b> </p> <p>Working with the Operations team in Aberdeen, the successful candidate will be responsible for coordinating the company’s logistics. The role is incredibly varied and comes with high level of responsibility - every day offering a new challenge.  The areas of work include but are not limited to:  </p><ul><li>International Business Travel and Accommodation arrangements</li><li>Adhering to country specific certification requirements including Visa, Vaccination, legislative, client specific policies and procedures to facilitate the mobilization of personnel to the work location</li><li>Client interfacing to ensure meet and greet, in country logistics, security and accommodation requirements are in place</li><li>Preparing work packs and job briefs for personnel</li><li>Monitoring employee certifications</li><li>Project planning using our bespoke project management software</li></ul> <p><b>Job Requirements</b> </p> <ul><li>Excellent communication and interpersonal skills</li><li>Organizational skills</li><li>Ability to be flexible and adapt to new challenges</li><li>Experience within a similar role is preferred</li><li>Degree qualified 2:1 (or above) in a business related subject is preferred or previous experience in a similar role</li><li>Working Knowledge of Microsoft Packages is essential</li><li>Working knowledge of SharePoint is preferred</li></ul><p><b>Additional Details</b> </p>  <p><b>How To Apply</b> </p> <p>Please quote job reference IRC126765 in all correspondence relating to this position.</p><p>For further information prior to submitting your application please contact Emily Earley on <a>emily.earley@alta-blue.com</a> or 01224763662.</p> ',
      'referencenumber' => 'IRC126765',
      'postalcode' => 'AB12 3LE',
      'company' => 'Altablue',
      'discipline' => 'Logistics',
      'country' => 'GB',
      'location' => '57.115460279639,-2.078630348308',
    ),
    'attributes' => 
    array (
    ),
  ),
  12 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Recruitment Coordinator',
      'date' => '20150722',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>The Recruitment Coordinator will assist the Recruitment Manager in the delivery and implementation of a pro-active recruitment strategy.  Day to day responsibility for the recruitment process of the Southern region, ensuring suitably qualified candidates are recruited into the organisation. Making a positive impact on the organisation, using the most cost effective recruitment methods and working in partnership with the hiring managers.</p><p><b>Detailed Description</b> </p> <ul><li>Liaison with third party recruitment vendors, managing their interaction with the business and delivering feedback to them upon introduction of candidates. This does not include negotiating terms of business or altering the PSL in any way</li><li>Preparation of advertisements and job specifications with the line managers</li><li>Use of social media to directly target and recruit suitable applicants</li><li>Briefing recruitment suppliers on current requirements</li><li>Pre-screening candidates against the key criteria, prior to management review</li><li>Liaison with Universities and Careers Advisors</li><li>Assisting managers with interviews on an ad-hoc basis, if required</li><li>Ensuring all right to work checks are completed</li><li>Managing the onboarding process in conjunction with the line managers</li><li>Provide hiring managers and relevant parties with appropriate documentation pre and post interview</li><li>Directing all new starter documents to the HRIS Coordinator</li><li>Participate in and lead ad hoc process improvement projects</li></ul><p><b>Job Requirements</b> </p> <ul><li>Previous experience within a recruitment environment, preferably from in-house but agency experience is also beneficial</li><li>Knowledge of the social media channels available to assist in direct candidate attraction campaigns</li><li>Previous experience of recruiting in a technical industry</li><li>Excellent written and verbal communication skills</li><li>Confidence to work on own initiative and be self-sufficient, as well as ability to work well with others</li><li>Excellent judge of character</li><li>Be motivated and results driven</li><li>Be able to act quickly and decisively</li><li>Have excellent organisational skills</li><li>Be able to pick up technical terminology and absorb from the surroundings to develop knowledge of businesses and overall sectors</li><li>Be interested in working in the scientific sector</li></ul><p><b>Additional Details</b> </p>  <p><b>How To Apply</b> </p> <p>Please quote job reference IRC126 889 in all correspondence relating to this position</p><p>Please contact Lauren Allen on <a>Lauren.allen@alta-blue.com</a> or on 01224 778144 for more details. </p>',
      'referencenumber' => 'IRC126889',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Life Sciences',
      'country' => 'GB',
      'location' => '52.2033051,0.124862',
    ),
    'attributes' => 
    array (
    ),
  ),
  13 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Senior Design Manager',
      'date' => '20150727',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is supporting one of the UK’s leading Construction groups with a number of commercial and site based vacancies. Our client boasts a strong reputation for providing whole-life solutions, high standards of project delivery, and an ability to innovate. Our clients operations are organised into five key segments: Building, Infrastructure, Facilities Management, Investments and Integrated Solutions. We are working with our client on a large civil construction infrastructure project in the Aberdeen area.</p><p>Our client requires a Senior Design Manager to support the development of the Design strategy and implement and manage the control of the design process to ensure that the design is a winning solution and can be delivered to meet the project requirements and are in line with cost parameters.</p><p> </p><p><b>Detailed Description</b> </p> <ul><li>Lead the development and implementation of design proposals, ensuring they meet customer requirements, including budget constraints, presenting proposals for approval to the customer. </li><li>Manage and ensure effective briefing of the Design Team including M&amp;E elements. This includes the management of any 3rd parties or novated design teams to ensure that they meet all customer specifications and where necessary, checking the competence and resources of any incumbent design teams or 3rd parties.</li><li>Provide advice and guidance on the “buildability” of the design proposals to the design and construction teams on all packages.</li><li>Liaise with all statutory bodies/authorities as required – such as town planning, building control and services e.g. gas and water.</li><li>Support the safety by design &amp; engineering forum or other similar forums, representing the business stream, as required  </li><li>Lead the development and management of the design deliverables programme:</li><li>Lead the implementation of the BIM CSUK protocols, including teamwork and collaboration and the management and sharing of data relating to BIM models </li><li>Provide BIM management during the bid stage ensuring the supply chain is fully aware of BIM requirements  </li><li>Consider the application of lean construction techniques in order to save money, reduce waste and optimise construction  </li><li>Ensure that learning and best practice is captured from the bid/project with the aim of sharing it across the business stream and the CSUK business</li><li>Proactively contribute to the Risk and Value Management process</li><li>Ensure a relentless focus on Zero Harm</li><li>Support the delivery of CSUK’s Sustainability activities </li></ul><p><b>Job Requirements</b> </p> <ul><li>This role requires an individual with proven experience at a senior level in delivering design services within a work winning environment and successfully implementing design strategies as well as relevant project experience.</li><li>Leadership skills in a matrix organisation are required to deliver design services in a timely manner with a customer and business sector specific focus. Strong interpersonal and communication skills are required, coupled with an ability to form effective relationships with partners and delivery team members under time pressure. Experience of leading a team and/or coaching team members is desirable. </li><li>Numeracy and bid/report writing is a requirement. The individual will know ‘what good looks like’ in bid submissions and work winning and have hands on experience of delivering work-winning bids in a timely manner. Good working knowledge of BIM is required. The individual will have experience within the most current forms of procurement. </li><li>Excellent IT Skills relevant to the Business Stream are desirable and Microsoft Office is essential. Detailed knowledge of codes of practice and other regulatory material, applicable to the business stream.</li><li>The individual should have a desire to continuously pursue innovation and development, with a focus on the customer and the relevant business stream.</li></ul> <p><b>Additional Details</b> </p> <p>Relevant professional membership with an appropriate institute, e.g. RIBA, BIBSE, IET, aligned to the workstream is recommended.  A valid CSCS card is a requirement.</p><p><b>How To Apply</b> </p> <p>Online applications only - no email applications</p>',
      'referencenumber' => 'IRC126975',
      'postalcode' => 'AB12 3LE',
      'company' => 'Altablue',
      'discipline' => 'Infrastructure',
      'country' => 'GB',
      'location' => '57.115460279639,-2.078630348308',
    ),
    'attributes' => 
    array (
    ),
  ),
  14 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Quantity Surveyor',
      'date' => '20150729',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering recruitment services for our clients with a focus on relationships and collaboration. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p>Altablue is supporting one of the UK’s leading Housebuilding and Construction groups with a number of commercial and site based vacancies. Our client boasts a strong reputation for providing whole-life solutions, high standards of project delivery, and an ability to innovate. Our clients operations are organised into five key segments: Building, Infrastructure, Facilities Management, Investments and Integrated Solutions</p><p>Altablue are currently looking for Quantity Surveyors/ Senior Quantity Surveyors to support our client with an upcoming project.  They are expected to take responsibility for the execution of the Quantity Surveying function on allocated contract/s, within limits defined by the Projects and/or Managing Quantity Surveyor and in accordance with the Quantity Surveying Procedures Manual, in order to fulfil the requirements of the Company in maximising quality of service and profitability.</p><p> </p><p><b>Detailed Description</b> </p> <p>Key accountabilities of this role include:</p><p>Commercial &amp; Contractual</p><p>         Measurement and valuation of works (on site and from drawings) for Valuations, Variations &amp; Final Accounts by preparing detailed, accurate and orderly measurements in accordance with relevant measurement standards. </p><p>         Agreement of valuation dates and preparing, submitting and agreement of interim valuations in accordance with those dates. </p><p>         Providing detailed record and valuation of work executed with application of principles of cash maximisation and issuing of draft invoices to Sales Ledger Department and advising of agreed valuations dates.</p><p>         Measurement and valuation of subcontractors work for payment and Final Account purposes by agreement of subcontract valuations and completion of regular and timely payment certificates. </p><p>         Recognising and advising Site Manager on delays, extensions of time and loss and/or expense and ensuring relevant notices are issued.</p><p>         Ensuring adequate records are kept for the purposes of valuing variations and ascertaining loss and/or expense by maintaining records of resource allocation including labour plant and materials and reviewing subcontract correspondence for delay and loss and/or expense claims.<b></b></p><p><b></b></p><b></b><p>         Preparation of claim documentation &amp; information for submission to Clients/ Consultants within required timescales. </p><p>         Satisfying Client deadlines for estimates and quotations whilst applying principles of profit maximisation and protecting Companies interests. </p><p>         Preparation of tender enquiry documents for the procurement of subcontractors and assist with the subcontract buyers through to placement of order.</p><p>         Understanding and advising on the terms and conditions of the main contract and also the subcontract, which may also include negotiation with the subcontractor for the agreement thereof.</p><p>Financial Reporting</p><p>Provision of standard reports (as per the Quantity Surveying Procedures Manual) for all reporting tasks and providing reports in accordance the agreed times and frequency. These include:</p><p>         Internal Valuations/CVRs and subcontract liability reports in accordance with schedule of accounting period end dates and deadlines</p><p>         Margin Analysis reports</p><p>         Commercial Analysis (final margin) forecasts and Cost to Complete forecasts</p><p>         Cash Flow forecasts</p><p>         Turnover forecasts</p><p>         De-brief reports</p><p>Typical Outputs</p><p>         Ability to set up necessary systems and to efficiently file and retrieve information and to allow others to do likewise.</p><p>         Full understanding and execution of archiving procedures. </p><p>         Timely and accurate production of material delivery sheets</p><p>         Preparing detailed, accurate and orderly stock lists and applying relevant rates from orders or price books. Securing agreement to figures if required.</p><p>         Preparing daywork sheets from daily records and pricing sheets using relevant daywork rates including agreement if required.</p><p>         Completing regular and timely payment certificates and presenting to line manager for signature.</p><p>         Providing detailed record and valuation of work executed with application of principles of cash maximisation. </p><p>         Preparing detailed schedules of periodic costs from site based information. Comparing these with Head Office cost prints and resolving anomalies with Cost Department. Reviewing sub-contract liabilities.</p><p>         Daily general management of Sub-Contractors and Sub-Contract accounts</p><p>         Preparing detailed and accurate measurements of theoretical material quantities and comparing against actual deliveries to determine and explain wastage factors. </p><p>         Preparing monthly client valuations</p><p>         Daily management of the client final account including the valuing of any variations</p><p><b>Job Requirements</b> </p> <p>Competency Requirements:</p><p>         Good communication verbally and in written form.</p><p>         Report all breach of Health and Safety Rules and Regulations to the Line Manager.</p><p>         Adheres to building code rules in daily construction activities.</p><p>         Solves construction problems with building code compliance in a timely and effective manner.</p><p>         Knowledge of JCT &amp; NEC Conditions of Contract.</p><p>Technical/Professional Expertise and Qualifications Required:</p><p>         The candidate should hold a professional qualification HNC, ONC or equivalent</p><p>         The candidate should hold a qualification as a chartered quantity surveyor (RICS). (or IOB)</p><p>         Hold appropriate CSCS card</p><p>         Experience of Design and Build new build and refurbishment projects.</p><p>         Must have experience of working within the Construction industry.</p><p>         Must have experience of Oracle for processing Applications and invoices.</p><p><b>Additional Details</b> </p> <p>In summary, the Quantity Surveyor will be responsible for managing all costs relating to building and civil engineering projects. Their focus is to minimise the costs of a project and enhance value for money, while still achieving the required standards and quality. </p><p>The successful candidate will have a reputation for integrity, maturity and sound business judgement and be totally comfortable making financial business decisions and working in a team environment.</p><p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC124062 in all correspondence relating to this position.</p><p>Online applications only - no email applications</p><p> </p>',
      'referencenumber' => 'IRC127053',
      'postalcode' => 'AB12 3LE',
      'company' => 'Altablue',
      'discipline' => 'Infrastructure',
      'country' => 'GB',
      'location' => '57.115460279639,-2.078630348308',
    ),
    'attributes' => 
    array (
    ),
  ),
  15 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Sub Agent',
      'date' => '20150729',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering recruitment services for our clients with a focus on relationships and collaboration. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p> <p>Altablue is supporting one of the UK’s leading Housebuilding and Construction groups with a number of commercial and site based vacancies. Our client boasts a strong reputation for providing whole-life solutions, high standards of project delivery, and an ability to innovate. Our clients operations are organised into five key segments: Building, Infrastructure, Facilities Management, Investments and Integrated Solutions</p><p> </p><p>The Sub Agent will take full responsibility for production functions on contracts allocated by the Contract / Project Managers from award through to the issue of the Maintenance Certificate and to maintain and maximise quality of service and profitability. They will also be responsible for the following:</p><ul><li>To direct the necessary labour, plant, staff and equipment to achieve section completion on time, to specification and within budget.</li><li>Liaise with the design team to ensure the design deliverables are received in time for efficient delivery of the site works</li><li>Monitor that the Buying team and Surveying department order the correct materials and sub-contract services in good time to meet contract objectives. </li><li>Manage site teams by monitoring performance against contract targets and instigating any necessary corrective actions. </li><li>Ensure compliance with Safety Policy and Health and Safety Regulations. </li></ul><p><b>Detailed Description</b> </p> <p>Quality</p><ul><li>Quality is the cornerstone of Infrastructure Division’s strategy.  The policy is to create and maintain a culture whereby everyone is continually seeking to satisfy their customers\' requirements. </li><li>Managers will actively lead this initiative and will provide the environment, equipment, support and monitoring to continuously promote this ideal at all levels within the Company.</li></ul><p>Staff and Operative Development</p><ul><li>Plan, co-ordinate, maintain and develop staff and operative levels to effectively undertake current and prospective workloads.  </li><li>Participate and actively encourage both Company training courses and on the job training.  </li><li>Monitor and record individual performances encouraging and disciplining as and when necessary.   </li><li>Provide members of the site team with clear job descriptions defining responsibilities.</li></ul><p>Pre-Start Planning and Involvement</p><ul><li>Together with the Contract / Project Manager, make initial contacts with clients after contract award and participate in external pre-start meetings.  </li><li>Initiate and progress initial contract planning, ensuring maximum exploitation of all ‘angles\'.</li></ul><p>Site Management</p><ul><li>Co-ordinate and control initial site set-up, actively installing Company systems and procedures.</li><li>Rigorously apply effective planning techniques, methods analysis and controls to ensure the most effective and continuous use of Company resources.</li></ul><p>Financial Controls</p><ul><li>Administer the valuation system in conjunction with the Quantity Surveying Department, maximising cash collection and profitability.  </li><li>Implement strict financial controls on the payment of subcontractors, suppliers and operatives.</li></ul><p>Post Works Involvement</p><ul><li>Organise and co-ordinate the undertaking of maintenance works as directed by Contract / Project Managers to ensure the prompt issue of maintenance certificates and release of retentions. </li><li>Arrange for the provision of effective and relevant contract feedback data to the Estimating Department.</li></ul><p>Communication</p><ul><li>Liaise and co-ordinate with all Head Office Departments to improve all levels of communication so as to benefit and develop overall Company objectives.</li></ul><p>Reporting</p><ul><li>Produce data as required by Contract / Project Managers to facilitate accurate contract reporting for inclusion in Management Board Papers.</li></ul><p>Safety</p><ul><li>Undertake the role of the Site Safety Supervisor and actively maintain and enforce safety standards as required by statute and in accordance with the Company\'s Safety Policies.</li></ul><p>Security</p><ul><li>Appraise the security risk to all Company property under your control and install security levels commensurate to the relative values.</li></ul><p>Marketing</p><ul><li>Establish and maintain links with clients, determine customer satisfaction levels and develop contact points for post contract marketing.  </li><li>Actively participate in generating a wider client base and produce feedback data to the Commercial Manager.</li></ul><p> </p>Business Planning<ul><li>It is inherent on all Managers to actively participate in the Company\'s business planning process.  This will involve input to implement the Action Plan to achieve both short term objectives and long term strategic targets.  </li><li>Regular reviews and monitoring of performance against targets is to be undertaken.</li></ul><p><b></b></p><p><b> </b></p><b></b><p><b>Job Requirements</b> </p> <ul><li>Minimum of HNC/HND Civil Engineering</li><li>Must have Civil Engineering experience gained with a civil engineering main contractor rather than a sub-contractor (UK or overseas)</li><li>Ideally with roads experience </li></ul><p><b>Core Competencies:</b></p><p><b></b></p><b></b><p><b></b></p><p><b> </b></p><b></b><ul><li>Team player who can set direction and delivery.</li><li>Sound business judgment and commercially aware</li><li>Comfortable leading and working in a team environment.</li></ul><p><b>Additional Details</b> </p> <p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC124063 in all correspondence relating to this position.</p><p>Internal applicants must discuss their application with their line manager prior to applying.</p><p>Online applications only - no email applications</p>',
      'referencenumber' => 'IRC127054',
      'postalcode' => 'AB12 3LE',
      'company' => 'Altablue',
      'discipline' => 'Infrastructure',
      'country' => 'GB',
      'location' => '57.115460279639,-2.078630348308',
    ),
    'attributes' => 
    array (
    ),
  ),
  16 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Site Engineer',
      'date' => '20150729',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering recruitment services for our clients with a focus on relationships and collaboration. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p>Altablue is supporting one of the UK’s leading Housebuilding and Construction groups with a number of commercial and site based vacancies. Our client boasts a strong reputation for providing whole-life solutions, high standards of project delivery, and an ability to innovate. Our clients operations are organised into five key segments: Building, Infrastructure, Facilities Management, Investments and Integrated Solutions.</p><p>Altablue are currently looking for Site Engineers to support our client with an upcoming project.  They will be expected take responsibility for sectional or total contract setting out and site management functions allocated by the individual\'s Supervisor during site occupancy and thus maintaining and maximising quality of service and profitability.</p> <p><b>Detailed Description</b> </p> <p>The ideal candidate will be required to take charge of the following aspects:</p><p>Safety</p><p>         Actively support the site management in maintaining and enforcing safety standards as required by statute and in accordance with the Company\'s safety policies.</p><p>         Ensure you comply with all safe working practices.</p><p>Quality</p><p>         Create and maintain a culture whereby everyone is continually seeking to satisfy their customers\' requirements.  </p><p>         Actively participate in the quality initiative and provide support and monitoring to continuously promote this ideal at all levels within the Company.</p><p> </p>Staff and Operative Development<p>         Assist, when required, the site management team in planning, co coordinating, maintaining and developing staff and operative levels to effectively undertake current and prospective workloads.  </p><p>         Actively participate in both Company training courses and on the job training.</p><p>Site Management</p><p>         Assist the site management team in the initial site set up, actively installing Company systems and procedures.  </p><p>         Rigorously apply effective planning techniques, methods analysis and controls to ensure the most effective and continuous use of the Company resources.</p><p>Setting Out</p><p>         Undertake site setting out as directed by the site management team to satisfy contract programme requirements. </p><p>         Ensure standard Company procedures are adopted in carrying out setting out operations </p><p>         Ensure effective methods of communication are used in transmitting information to operatives and subcontractors.</p><p>Communication</p><p>         Liaise and co-ordinate with all Head Office departments to improve all levels of communication so as to benefit and develop overall Company objectives.</p><p>Marketing</p><p>         Assist in establishing and maintaining links with clients, ensuring customer satisfaction levels and develop contact points for post contract marketing.  </p><p><b>Additional Details</b> </p> <p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC124549 in all correspondence relating to this position.</p><p>Online applications only - no email applications</p> ',
      'referencenumber' => 'IRC127057',
      'postalcode' => 'AB12 3LE',
      'company' => 'Altablue',
      'discipline' => 'Infrastructure',
      'country' => 'GB',
      'location' => '57.115460279639,-2.078630348308',
    ),
    'attributes' => 
    array (
    ),
  ),
  17 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Section Engineer',
      'date' => '20150729',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is supporting one of the UK’s leading House building and Construction groups with a number of commercial and site based vacancies. Our client boasts a strong reputation for providing whole-life solutions, high standards of project delivery, and an ability to innovate. Our clients operations are organised into five key segments: Building, Infrastructure, Facilities Management, Investments and Integrated Solutions.</p><p>Altablue are currently looking for Section Engineers to support our client with an upcoming project.  They will be expected take responsibility for sectional or total contract setting out and site management functions allocated by the individual\'s Supervisor during site occupancy and thus maintaining and maximising quality of service and profitability.</p><p><b>Detailed Description</b> </p> <p>Core</p><p>         Ensure a relentless focus on Zero Harm</p><p>         Support the delivery of CSUK’s Sustainability activities</p><p> </p><p>Health Safety &amp; Environmental</p><p>         Produces Method Statements and Risk Assessments and undertakes periodic reviews</p><p>         Prepares and raises Temporary Works Briefs, following up to ensure timely implementation</p><p>         Carries out Visitors Induction</p><p>         Completes Accident Book and associated reports as required. Able to undertake incident investigations to required standard</p><p>         Undertakes and mentors less experienced staff in safety audits<br /><br /></p><p>Engineering Control </p><p>         Carry out complex setting out</p><p>         Provision of ‘primary’ setting out control and recognizes the optimum location for installation</p><p>         Coaches junior engineers in the use of setting out techniques<br /><br /></p><p>Quality</p><p>         Produces Activity Control Plans / Inspection and Test Plans and obtains approval</p><p>         Actively uses the Specification for reference and coaches junior team members in using the Spec</p><p>         Maintains a comprehensive filing system of quality records aligned to the Project Quality Handover requirements</p><p>         Ensures that Site Engineers complete the required quality checksheets on a timely basis for the works</p><p>         Identifies, raises and actions Non Conformance Reports. Tracks close-out of NCRs within section.</p><p>         Identifies and raises Technical Queries (RFIs) with possible solutions. Tracks closeout of RFIs within section<br /><br /></p><p>Productivity</p><p>         Produces a weekly programme and measures progress against KPI’s</p><p>         Communicates weekly programme and targets to the workforce</p><p>         Monitors as-built programme and reports reasons for difference</p><p>         Liaises with subcontractors to ensure works undertaken in line with weekly programme<br /><br /></p><p>Commercial</p><p>         Maintains accurate diary, including dialogue with external organisations (subcontractors)</p><p>         Collates Weekly Measure information and monitors wastage against budget allowances / KPI’s</p><p>         Carries out a labour and plant reconciliation and supervises less experienced team members</p><p>         Identifies items of additional work and raises the necessary notifications</p><p>         Works to specific terms, conditions and outputs of relevant subcontractors and suppliers</p><p> </p><p><b>Job Requirements</b> </p> <p>The following qualities/experience are essential:<br /><br />Core</p><p>         Able to clearly communicate the requirements of a safe system of work to ensure that these are followed accurately. Suggests improvements to systems</p><p>         Capable of taking ownership of tasks and communicating outcomes with clarity </p><p>         Strong work ethic with flexibility to work unsociable hours/nights</p><p>         Able to work on own initiative and seek out opportunities in line with level of responsibility</p><p>         Holds a current driving license and CSCS Card. SSSTS or SMSTS qualified</p><p>         Must be a strong team player, with developed communication skills <br /><br /></p><p>Health Safety &amp; Environmental</p><p>         Understands Permit System and actively raises Permits to Work</p><p>         Is able to identify potential hazards and is proactive in implementing actions and controls </p><p>         Good level of Environmental Awareness</p><p> </p><p>Engineering Control </p><p>         Able to carry out complex setting out</p><p>         Able to resolve issues with control and information provided</p><p>         Able to identify issues on drawings and suggest construction solutions</p><p> </p><p>Quality</p><p>         Understands and has experience of producing ACP</p><p>         Actively drives the production of quality handover documentation and information </p><p>         Strong understanding of construction tolerances</p><p>         Awareness of Specification and other Contract documents<br /><br /></p><p>Productivity</p><p>         Understands and can use the information provided in the Stage Programme, disseminating this to the team</p><p>         Strong knowledge of construction process in one or more of the following areas: Earthworks/Drainage, Pavement, Comms, Structures, Roadworks<br /><br /></p><p>Commercial</p><p>         Good understanding of key contract documentation and understands how to record additional work</p><p>         Understands specific terms, conditions and outputs of relevant subcontractors and suppliers</p>Understands required records for commercial recovery and supervises production of these by junior team members<p><b>Additional Details</b> </p> <p> </p><p>The following qualities/experience are desirable:</p><p>         Leadership skills advancing </p><p>         Evidence of effective independent judgement skills</p><p>         Good progression towards professional accreditation (eg ICE, IHT)<br /><br /></p><p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC125134 in all correspondence relating to this position.</p><p>Internal applicants must discuss their application with their line manager prior to applying.</p><p>Online applications only - no email applications</p>',
      'referencenumber' => 'IRC127058',
      'postalcode' => 'AB12 3LE',
      'company' => 'Altablue',
      'discipline' => 'Infrastructure',
      'country' => 'GB',
      'location' => '57.115460279639,-2.078630348308',
    ),
    'attributes' => 
    array (
    ),
  ),
  18 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'General Foremen',
      'date' => '20150730',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is supporting one of the UK’s leading Construction groups with a number of commercial and site based vacancies. Our client boasts a strong reputation for providing whole-life solutions, high standards of project delivery, and an ability to innovate. Our clients operations are organised into five key segments: Building, Infrastructure, Facilities Management, Investments and Integrated Solutions.</p><p> </p><p>Altablue are currently looking for General Foremen to support our client with an upcoming Infrastructure project in Aberdeen.  They will be expected take responsibility for sectional or total contract setting out and site management functions allocated by the individual\'s Supervisor during site occupancy and thus maintaining and maximising quality of service and profitability.</p><p> </p><p> </p><p> </p><p><b>Detailed Description</b> </p> <p>Duties and responsibilities are: </p><ul><li>Motivate team and manage performance; monitor the competency of team members and undertake training/ coaching as appropriate </li><li>Ensure a relentless focus on Safety</li><li>Carry out Safety &amp; Environment Tours as required </li><li>Contributes to Accident / Incident Investigations. </li><li>Appraises improvement options </li><li>Keeps up to date with changes in legislation and cascades information to the team</li><li>Be familiar with, and demonstrate commitment to the requirements of the Health &amp; Safety and Environmental Policies </li><li>Ensures that their staff receive or have access to copies of the Environmental Policy and are kept informed of environmental developments and issues </li><li>Ensures that the Environmental Policy and appropriate parts of Environmental Procedures are effectively communicated to the workforce</li><li>Seeks to improve levels of safety at every opportunity<br />Ensures that sufficient resources are allocated to the Works Section and that each has an appropriate level of ability</li><li>Identifies and arranges training as required</li><li>Implements systems and processes to manage the quality of the constructive works</li><li>Periodic involvement in working groups focused on Improvement Areas</li><li>Takes ownership of tasks and quality requirements</li><li>Inputs into the setting of targets and KPI’s in line with Project objectives and monitors performance</li><li>Works with the Team Lead to ensure appropriate action is taken on findings from site audits / checks</li><li>Implements relevant Business Management systems and processes</li><li>Ensures ownership of Handover Documentation for the works</li><li>Implements processes to improve quality management on site </li><li>Ensures that a culture of Right First Time is adopted</li><li>Carry out Quality Tool Box Talks </li><li>Able to carry out analysis of the project KPI’s and is able to make suggestions to improve performance</li><li>Apply skills and knowledge to assist engineering teams in planning stage programmes</li><li>Implements and tracks the requirements of three week programmes and coaches others in ensuring programme requirements are fully understood, suggesting improvements as appropriate</li><li>Involved in the procurement of subcontractors and advises on commercial aspects / preferred attendances</li><li>Ownership of budget for the directly employed labour and plant, attends and reports on actual levels of resource in weekly cost meeting. Pursues correct allocation of plant and labour to assigned tasks</li></ul><p><b>Job Requirements</b> </p> <p>The ideal candidate will be:</p><ul><li>Able to clearly communicate the requirements of a safe system of work to ensure that these are followed accurately</li><li>Capable of taking ownership of tasks and communicating outcomes with clarity; experienced at encouraging direct reports to do the same and managing a team to ensure tasks are carried out effectively and key messages cascaded</li><li>Strong work ethic with flexibility to work unsociable hours/nights, as required by the programme </li><li>Able to work on own initiative and seek out opportunities in line with level of responsibility</li><li>Holds a current driving license and CSCS Card</li><li>Must be a good team player, with experience of working with multi-discipline teams</li><li>Excellent communication skills, with strong interpersonal and influencing skills</li><li>Proficient IT skills</li><li>Understands Protocol with regard to Accident and Incident investigation</li><li>Experienced in leading Safety &amp; Environment Tours</li><li>Uncompromising approach to the development, implementation and management of safe systems of work</li><li>Good working knowledge of current H &amp; S legislation</li><li>Innovative approach to enhancing safe systems of work</li><li>Experienced in the delivery of engaging toolbox talks</li><li>Experienced in resource allocation and up skilling quality</li><li>Understands the relevant Business Management System and implements systems and processes (e.g. ordering plant, recruiting labour, disciplinary procedures)</li><li>Understanding of construction tolerances</li><li>Encourages ownership within the team and fosters a culture of Right First Time</li><li>Able to take off and schedule material quantities accurately</li><li>Experience of planning and managing project creep</li><li>Understands the specific terms, conditions and outputs of relevant subcontractors and suppliers </li><li>Produces and monitors the daily records, including holding reviews with team members to ensure proper record standards are maintained</li><li>Understands material wastage allowances and recommends improvements to save materials</li><li>Working knowledge of the NEC and other relevant forms of contract and able to develop professional relationship with Client representatives</li></ul><p> </p><p><b>Additional Details</b> </p> <p>The following qualities/experience are desirable:</p><ul><li>Excellent role model for the team and demonstrates required behaviours to align to business objectives</li><li>Highly experienced in the delivery of high profile road projects, to ensure that customer expectations are exceeded and enhanced experience for all involved in the scheme</li><li>Solution orientated individual with experience of analysing options to identified issues </li><li>Experience in taking ownership of the works from start to finish, including the handover of the works </li><li>Experience of leading a team<br /><br /><br /><br /><br /> </li></ul><p><b>How To Apply</b> </p> <p> </p>',
      'referencenumber' => 'IRC127068',
      'postalcode' => 'AB12 3LE',
      'company' => 'Altablue',
      'discipline' => 'Infrastructure',
      'country' => 'GB',
      'location' => '57.115460279639,-2.078630348308',
    ),
    'attributes' => 
    array (
    ),
  ),
  19 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Section Foreman',
      'date' => '20150730',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is supporting one of the UK’s leading Construction groups with a number of commercial and site based vacancies. Our client boasts a strong reputation for providing whole-life solutions, high standards of project delivery, and an ability to innovate. Our clients operations are organised into five key segments: Building, Infrastructure, Facilities Management, Investments and Integrated Solutions.</p><p> </p><p>Altablue are currently looking for Section Foremen to support our client with an upcoming Infrastructure project in Aberdeen.  They will be expected to provide section supervision of the works control of a team comprising Junior Foremen and Gangers as well as supervision and control of the works of Subcontractors.</p><p> </p><p><b>Detailed Description</b> </p> <p>The main accountabilities are:</p><ul><li>Ensure a relentless focus on Safety</li><li>Reviews Method Statements and makes improvements as appropriate</li><li>Plays an active role in Accident / Incident Investigation as required</li><li>Through observation and liaison with the workforce and subcontractors suggests / recommends improvement areas</li><li>Ensures plant is adequate for the task prior to being used</li><li>Monitors the performance of plant operators and appointed persons. Identifies training needs as required</li><li>Carries out Safety &amp; Environment Tours as required </li><li>Keeps up to date with changes in legislation and cascades information to the team</li><li>Be familiar with, and demonstrate commitment to the requirements of the Health &amp; Safety and Environmental Policies </li><li>Ensures that their staff receive or have access to copies of the Environmental Policy and are kept informed of environmental developments and issues </li><li>Ensures that the Environmental Policy and appropriate parts of Environmental Procedures are effectively communicated to the workforce, particularly to those engaged on activities that have been identified as likely to have a significant environmental impact </li><li>Seeks to improve levels of safety at every opportunity</li><li>Identifies and arranges training as required for workforce</li><li>Carries out Independent Inspections of their own section and of others</li><li>Ensures that the workforce / Gangers have sufficient and correct information in terms of specification, workmanship and finish</li><li>Ensures that all Non Conformances are reported as required</li><li>Monitor levels of workmanship </li><li>Carry out Quality Tool Box Talks</li><li>Ensures that a culture of Right First Time is adopted in the Supply Chain</li><li>Ensures efficiency and performance of workforce and takes appropriate action as necessary</li><li>Understands key milestones and prioritises relevant resources</li><li>Implements the requirements of three week programme, suggests improvements as appropriate</li><li>Assists in the production of a budget / resourced programme and monitors / reviews actual resources expended</li><li>Involved in the procurement of subcontractors</li><li>Plays an active role in weekly cost meetings and recommends improvement areas</li></ul> <p><b>Job Requirements</b> </p> <p>The following qualities/experience are essential:</p><ul><li>Able to clearly communicate the requirements of a safe system of work to ensure that these are followed accurately</li><li>Capable of taking ownership of tasks and communicating outcomes </li><li>Strong work ethic with flexibility to work unsociable hours/nights </li><li>Able to work on own initiative and seek out opportunities in line with level of responsibility</li><li>Holds a current driving license and CSCS Card</li><li>Must be a good team player</li><li>Good communication skills, with effective interpersonal and influencing skills </li><li>Understands the basic Company Protocols with regard to Accidents and Incidents investigations </li><li>Uncompromising approach to the development, implementation and management of safe systems of work</li><li>Good working knowledge of current H &amp; S legislation</li><li>Experienced in the delivery of engaging toolbox talks</li><li>Experienced in resource allocation</li><li>Understands the relevant Business Management System and implements systems and processes (e.g. ordering plant, recruiting labour, disciplinary procedures)</li><li>Understanding of construction tolerances</li><li>Understands and implements the requirements of Inspection and Test Plans</li><li>Encourages ownership within the team and fosters a culture of Right First Time</li><li>Experience of managing the works of direct labour and subcontractors</li><li>Able to take off and schedule material quantities accurately</li><li>Understands the specific terms, conditions and outputs of relevant subcontractors and suppliers </li><li>Produces and monitors the daily records, including holding reviews with junior team members to ensure proper record standards are maintained</li><li>Understands material wastage allowances and ability to identify improvements to save materials</li><li>Working knowledge of the NEC and other relevant forms of contract and able to develop professional relationship with Client representatives</li></ul><p><b>Additional Details</b> </p> <p>The following qualities/experience are desirable:</p><p>         Experienced in the delivery of high profile road projects, to ensure that customer expectations are exceeded and enhanced experience for all involved in the scheme.</p><p>         Solution orientated individual, with ability to analyse risks and benefits of options to resolve identified issues</p><p>         Capable of taking ownership of the works from start to finish, including the handover of the works<br /><br /></p> <p> </p><p><b>How To Apply</b> </p>  ',
      'referencenumber' => 'IRC127077',
      'postalcode' => 'AB12 3LE',
      'company' => 'Altablue',
      'discipline' => 'Construction',
      'country' => 'GB',
      'location' => '57.115460279639,-2.078630348308',
    ),
    'attributes' => 
    array (
    ),
  ),
  20 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Geo-Environmental Consultant',
      'date' => '20150730',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering recruitment services for our clients with a focus on relationships and collaboration. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity.</p><p>Altablue is supporting a rapidly growing environmental consultancy with a number of new vacancies across several offices throughout the UK. The role will require the individual to take part in the full project life cycle ranging from site works through to input on interpretive reports. Your role within the team will allow for the development of technical understanding of both geotechnical and environmental aspects enabling the successful candidate to achieve a fuller knowledge of the geo-environmental industry.</p><p> </p> <p><b>Detailed Description</b> </p> <p>The desired candidate will have a minimum of 2 years relevant experience and be in possession of Chartership status with a recognised professional body. Experience in the design, planning and delivery of contaminated land desk studies, site investigations and risk assessment is essential. Experience of geotechnical aspects and remediation projects would be advantageous but not essential. </p><p><b>Job Requirements</b> </p> <p>You should be ambitious, happy to work on your own demonstrating initiative, with a desire to contribute to the ongoing growth of the business. Strong communication skills and the ability to build effective relationships with suppliers, clients and peers at all levels are vital.</p><p><b>Additional Details</b> </p> <p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC126505 in all correspondence relating to this position.</p><p> </p><p>Please contact Lauren Allen on <a>Lauren.allen@alta-blue.com</a> or on 01224 778144 for more details</p><p> </p>',
      'referencenumber' => 'IRC127090',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Life Sciences',
      'country' => 'GB',
    ),
    'attributes' => 
    array (
    ),
  ),
  21 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Geo-Environmental Consultant',
      'date' => '20150730',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering recruitment services for our clients with a focus on relationships and collaboration. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity.</p><p>Altablue is supporting a rapidly growing environmental consultancy with a number of new vacancies across several offices throughout the UK. The role will require the individual to take part in the full project life cycle ranging from site works through to input on interpretive reports. Your role within the team will allow for the development of technical understanding of both geotechnical and environmental aspects enabling the successful candidate to achieve a fuller knowledge of the geo-environmental industry.</p><p> </p> <p><b>Detailed Description</b> </p> <p>The desired candidate will have a minimum of 5 years relevant experience and be in possession of Chartership status with a recognised professional body. Experience in the design, planning and delivery of contaminated land desk studies, site investigations and risk assessment is essential. Experience of geotechnical aspects and remediation projects would be advantageous but not essential. </p><p><b>Job Requirements</b> </p> <p>You should be ambitious, happy to work on your own demonstrating initiative, with a desire to contribute to the ongoing growth of the business. Strong communication skills and the ability to build effective relationships with suppliers, clients and peers at all levels are vital.</p><p><b>Additional Details</b> </p> <p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC126505 in all correspondence relating to this position.</p><p> </p><p>Please contact Lauren Allen on <a>Lauren.allen@alta-blue.com</a> or on 01224 778144 for more details</p><p> </p>',
      'referencenumber' => 'IRC127091',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Life Sciences',
      'country' => 'GB',
    ),
    'attributes' => 
    array (
    ),
  ),
  22 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Senior Air Quality Consultant',
      'date' => '20150730',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering recruitment services for our clients with a focus on relationships and collaboration. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity.</p><p>We are delighted to be supporting a rapidly growing environmental consultancy with vacancies across several offices throughout the UK. Due to the continued success and growth of the air quality consultancy business, there are a number of opportunities for air quality specialists of all levels to join the team.</p><p>Altablue are currently looking for a Senior Air Quality Consultant based in Manchester. The successful candidates will be qualified in a relevant environmental discipline to Degree or Masters Level and have experience and working knowledge of dispersion modelling of roads and point sources. </p><p><b>Detailed Description</b> </p> <p>         Undertaking air quality assessments</p><p>         Dispersion modelling</p><p>         Data analysis and interpretation</p><p>         Preparation of factual and interpretative reports</p><p>         Management of multiple projects</p><p>         Production of quotes and tenders</p><p>         Development of client relationships</p> <p><b>Job Requirements</b> </p> <p>         Atmospheric science or environmental based degree</p><p>         Excellent written and communication skills</p><p>         Proficiency in data analysis and interpretation</p><p>         Consultant level - at least 2-years air quality consultancy experience</p><p>         Senior Consultant level - at least 5-years air quality consultancy experience</p><p>         Able to manage own projects from initial proposal to final invoice</p><p>         Knowledge of standard methodologies for assessment of roads, industrial and dust emissions</p><p>         Membership of relevant professional body (IAQM/IEMA)</p><p>         Experience of business development/marketing would be advantageous</p><p>         Current Full Clean Driving License</p><p> </p><p><b>Additional Details</b> </p> <p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC126 498 in all correspondence relating to this position</p><p>Please contact Lauren Allen on <a>Lauren.allen@alta-blue.com</a> or on 01224 778144 for more details. </p><p> </p><p> </p>',
      'referencenumber' => 'IRC127092',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Life Sciences',
      'country' => 'GB',
      'location' => '53.4791466,-2.2447445',
    ),
    'attributes' => 
    array (
    ),
  ),
  23 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Principal Geo-Environmental Consultant',
      'date' => '20150813',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering recruitment services for our clients with a focus on relationships and collaboration. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity.</p><p>Altablue is supporting a rapidly growing environmental consultancy with a number of new vacancies across several offices throughout the UK. The role will require the individual to take part in the full project life cycle ranging from site works through to input on interpretive reports. Your role within the team will allow for the development of technical understanding of both geotechnical and environmental aspects enabling the successful candidate to achieve a fuller knowledge of the geo-environmental industry.</p><p> </p> <p><b>Detailed Description</b> </p> <p>The successful candidate will be actively involved in the day to day project management of Geo-Environmental site investigations and other projects undertaken by the team, prepare the interpretative geo-environmental and geotechnical reports and be involved with client and regulatory liaison.  In addition, the candidate will carry out environmental and geotechnical remediation options appraisals, remediation strategies, remediation plans and verification reports.</p><p> </p><p>For the role, you will need to demonstrate good experience of design, planning and delivery of contaminated land desk studies, site investigations, risk assessment (qualitative and quantitative) methodologies and remediation projects of various contaminated land scenarios.  A good understanding of current UK environmental regulation is required, experience of CLEA, P20 and RBCA would be preferred.  </p><p> </p><p>The successful candidate will work alongside our highly experienced team of geotechnical engineers, environmental scientists and geo-environmental specialists working on projects throughout northern England.  The candidate will work in full cooperation with team members to build a strong team ethos capable of delivering high quality Geo-Environmental engineering services to clients.</p><p> </p><p>Your role will be very much client-facing, so confidence in interfacing with clients on both project delivery and business development matters is essential, as is proficiency in the preparation of bids, tenders and proposals.  You will also mentor more junior staff and assist in their professional development, giving you great potential to develop your management skills.</p><p> </p><p>Candidate Specification        </p><p> </p><p>         Minimum of 10 years professional experience as a Geo-Environmental professional</p><p> </p><p>         Relevant academic/professional qualifications, to include Chartership in either geology, chemistry, engineering or environmental sciences</p><p> </p><p>         Substantial UK contaminated land experience in a professional consultancy environment</p><p> </p><p>         Strong commercial awareness</p><p> </p><p>         Ability to build relationships and to maintain appropriate high level contact with clients</p><p> </p><p>         Full driving licence</p> <p><b>Job Requirements</b> </p>  <p><b>Additional Details</b> </p> <p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC126505 in all correspondence relating to this position.</p><p> </p><p>Please contact Lauren Allen on <a>Lauren.allen@alta-blue.com</a> or on 01224 778144 for more details</p><p> </p>',
      'referencenumber' => 'IRC127157',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Life Sciences',
      'country' => 'GB',
    ),
    'attributes' => 
    array (
    ),
  ),
  24 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Asbestos Surveyor',
      'date' => '20150805',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is supporting a rapidly growing environmental consultancy with vacancies across several offices throughout the UK. We currently have an exciting opportunity for an Asbestos Surveyor based in the Manchester/Cumbria for one of the leading Asbestos management and testing consultancies. Due to continued growth within this sector, our client is keen to engage with Asbestos Surveyors.</p><p><b>Job Requirements</b> </p> <p>The ideal candidate will be highly motivated, have previous experience in a surveyor role and be in possession of P402 certification. The role involves undertaking surveys on a variety of sites as well as preparing and updating reports.</p><p>The ideal candidate must be in possession of a Full UK Driving license and be willing to travel to different office locations.</p><p><b>Additional Details</b> </p>  <p><b>How To Apply</b> </p> <p>Please quote job reference IRC126500 in all correspondence relating to this position</p><p>For further details prior to submitting your application please contact Lauren.allen@alta-blue.com</p> ',
      'referencenumber' => 'IRC127218',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Life Sciences',
      'country' => 'GB',
      'location' => '53.4791466,-2.2447445',
    ),
    'attributes' => 
    array (
    ),
  ),
  25 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Asbestos Surveyor',
      'date' => '20150805',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is supporting a rapidly growing environmental consultancy with vacancies across several offices throughout the UK. We currently have an exciting opportunity based in East Kilbride for an Asbestos Surveyor for one of the leading Asbestos management and testing consultancies. Due to continued growth within this sector, our client is keen to engage with Asbestos Surveyors.</p><p><b>Job Requirements</b> </p> <p>The ideal candidate will be highly motivated, have previous experience in a surveyor role and be in possession of P402 certification. The role involves undertaking surveys on a variety of sites as well as preparing and updating reports.</p><p>The ideal candidate must be in possession of a Full UK Driving license and be willing to travel to different office locations.</p><p><b>Additional Details</b> </p>  <p><b>How To Apply</b> </p> <p>Please quote job reference IRC126500 in all correspondence relating to this position</p><p>For further details prior to submitting your application please contact Lauren.allen@alta-blue.com</p> ',
      'referencenumber' => 'IRC127219',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Life Sciences',
      'country' => 'GB',
    ),
    'attributes' => 
    array (
    ),
  ),
  26 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Asbestos Surveyor p402',
      'date' => '20150813',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue’s client is an industry-leading specialist and oneof the few asbestos consultancies to individually own and operate UKASaccredited testing laboratories. </p><p>With 13 UK locations and 600 current personnelour client is looking to expand their sites and grow their organisation movingforward. </p><p>Our client creates tailored and bespoke asbestos solutions for theirclients within the infrastructure and house building sectors.</p><p><b>Job Requirements</b> </p> <p>p402 certificate</p><p><b>Additional Details</b> </p> <p><b>How To Apply</b> </p> <p>Please quote job reference IRC127281 in all correspondence relating to this position.</p><p>Internal applicants must discuss their application with their line manager prior to applying.</p><p>Online applications and email applications to rosy.thomson@alta-blue.com</p>',
      'referencenumber' => 'IRC127281',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Altablue',
      'country' => 'GB',
    ),
    'attributes' => 
    array (
    ),
  ),
  27 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Asbestos Surveyor p402',
      'date' => '20150813',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue’s client is an industry-leading specialist and oneof the few asbestos consultancies to individually own and operate UKASaccredited testing laboratories. </p><p>With 13 UK locations and 600 current personnelour client is looking to expand their sites and grow their organisation movingforward. </p><p>Our client creates tailored and bespoke asbestos solutions for theirclients within the infrastructure and house building sectors.</p><p><b>Job Requirements</b> </p> <p>P402 Certificate</p><p><b>Additional Details</b> </p> <p><b>How To Apply</b> </p> <p>Please quote job reference IRC127297 in all correspondence relating to this position.</p><p>Internal applicants must discuss their application with their line manager prior to applying.</p><p>Online applications and email applications to rosy.thomson@alta-blue.com </p>',
      'referencenumber' => 'IRC127297',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Life Sciences',
      'country' => 'GB',
      'location' => '50.3712659,-4.1425658',
    ),
    'attributes' => 
    array (
    ),
  ),
  28 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Asbestos Analyst p401',
      'date' => '20150813',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue’s client is an industry-leading specialist and oneof the few asbestos consultancies to individually own and operate UKASaccredited testing laboratories. </p><p>With 13 UK locations and 600 current personnelour client is looking to expand their sites and grow their organisation movingforward. </p><p>Our client creates tailored and bespoke asbestos solutions for theirclients within the infrastructure and house building sectors.</p><p><b>Job Requirements</b> </p> <p>p401 Certificate</p><p><b>Additional Details</b> </p> <p><b>How To Apply</b> </p> <p>Please quote job reference IRC127298 in all correspondence relating to this position.</p><p>Internal applicants must discuss their application with their line manager prior to applying.</p><p>Online applications and email applications to rosy.thomson@alta-blue.com</p>',
      'referencenumber' => 'IRC127298',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Life Sciences',
      'country' => 'GB',
    ),
    'attributes' => 
    array (
    ),
  ),
  29 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Operations Manager',
      'date' => '20150819',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p> </p><p>Altablue is working with an up and coming Multi-Utility Connections company who are rapidly expanding. The client has an established track record with some of the countries biggest developers and have worked on some large scale residential and commercial developments.  As part of their continued growth the client requires an Operations Manager in their recently opened Barnsley office. The Operations Manager will be responsible for the development and implementation of company procedures, policies and strategies as well as the day to day management and control of all operational activities. </p> <p><b>Detailed Description</b> </p> <p>The main duties of this role will include but not limited to:</p><ul><li>Manage multi-disciplined teams in ensuring all work is carried out to the highest standard ensuring quality, safety, cost and delivery.</li><li>Manage and increase the efficiencies of operations.</li><li>Oversee financial management of major supplier i.e. Gap, Fusion etc.</li><li>Contract/tender negotiation.</li><li>Depot building management.</li><li>Ensure quality standards are met at all times.</li><li>Develop and maintain good relationships with all customers, Project Managers and Team Leaders.</li><li>Resource planning</li><li>Provide site support for customer enquiries and assist with the successful resolution of complaints.</li><li>Ensure that safe working practices are maintained at all times.</li><li>Ensure good communication is maintained within operations.</li></ul><p><b>Job Requirements</b> </p> <p>For this role the client is looking for someone who has strong leadership and people management skills combined with an in depth knowledge of industry regulations and multiple utility networks. </p><p><b>Additional Details</b> </p> The ideal candidate will have a background in the construction industry, specifically utilities installation, and be educated to HNC or Degree level. <p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC127404 in all correspondence relating to this position.</p><p>If you would like to find out more about the role, call Richard Thompson on 01224 777813 </p><p>Online applications preferred however applications via e-mail will be accepted to <a>richard.thompson@alta-blue.com</a>.</p>',
      'referencenumber' => 'IRC127404',
      'postalcode' => 'S72 7BQ',
      'company' => 'Altablue',
      'discipline' => 'Operations',
      'country' => 'GB',
      'location' => '53.570977859672,-1.3851175929756',
    ),
    'attributes' => 
    array (
    ),
  ),
  30 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Civil Supervisor',
      'date' => '20150818',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p> </p><p>Altablue is working with an up and coming Multi-Utility Connections company who are rapidly expanding. The client has an established track record with some of the countries biggest developers and have worked on some large scale residential and commercial developments.</p><p> </p><p>The clients Multi Utility team are vital to the companies business and they require the best people to be part of this team. The Supervisor will be responsible for ensuring all Gas, Electric and Water civils works assigned are completed on time and with an emphasis on quality and safety.</p> <p><b>Detailed Description</b> </p> <p>The main duties of this role will include but not limited to:</p><ul><li>Comply with all relevant legislative and regulatory requirements.</li><li>Work within authorised safety and technical competencies.</li><li>Create a safe working environment for all works (including Gas, Water and Electric network operations), and continuously monitor, taking into consideration all potential associated risks and placing control measures where required whilst having knowledge of Emergency First Aid.</li><li>Prepare for linear excavation works by the correct identification of existing plant and use of trail holes to confirm route.</li><li>Liaise with commissioning company utility infrastructures as required but within authorised competencies (including building and testing).</li><li>Segregate the area for site works and highway works during Gas, Water and Electric network operations.</li><li>Take part in engineering activities.</li><li>Carry out excavations as required to enable installation, connection and or reinstatement works. </li><li>Protect installed company utility infrastructures by installation of sand base and sand cover as required.</li><li>Contribute to effective working relationships.</li><li>Confidently operate, maintain and ensure security of powered tools, plant and equipment.</li><li>Be confident in cable laying, winch training, Gas Meter fitting and manual handling.</li></ul><p><b>Job Requirements</b> </p> <ul><li>The ability to follow written and spoken instructions and good team-working skills</li></ul><p><b>Additional Details</b> </p> <p>The ideal qualifications, skills and experience for this role are:</p><ul><li>Full Driving licence (plus ideally CPCS Excavator up to 10 tonne, and CPCS Dumper)</li><li>CSCS Card, EU Skills Card, Gas and Water SHEA Card, Water Hygiene Card</li><li>GCSE or equivalent vocational qualifications</li><li>GN02 main layer up to 180mm or above</li><li>NCO Level 2 (Water Mains and Services)</li><li>NRSWA Ticket</li></ul> <p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC127414 all correspondence relating to this position.</p><p>Online applications preferred </p> ',
      'referencenumber' => 'IRC127414',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Civil/Structural',
      'country' => 'GB',
      'location' => '52.4813679,-1.8980726',
    ),
    'attributes' => 
    array (
    ),
  ),
  31 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Principal Geo-Environmental Consultant',
      'date' => '20150813',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering recruitment services for our clients with a focus on relationships and collaboration. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity.</p><p>Altablue is supporting a rapidly growing environmental consultancy with a number of new vacancies across several offices throughout the UK. The role will require the individual to take part in the full project life cycle ranging from site works through to input on interpretive reports. Your role within the team will allow for the development of technical understanding of both geotechnical and environmental aspects enabling the successful candidate to achieve a fuller knowledge of the geo-environmental industry.</p><p> </p> <p><b>Detailed Description</b> </p> <p>The successful candidate will be actively involved in the day to day project management of Geo-Environmental site investigations and other projects undertaken by the team, prepare the interpretative geo-environmental and geotechnical reports and be involved with client and regulatory liaison.  In addition, the candidate will carry out environmental and geotechnical remediation options appraisals, remediation strategies, remediation plans and verification reports.</p><p> </p><p>For the role, you will need to demonstrate good experience of design, planning and delivery of contaminated land desk studies, site investigations, risk assessment (qualitative and quantitative) methodologies and remediation projects of various contaminated land scenarios.  A good understanding of current UK environmental regulation is required, experience of CLEA, P20 and RBCA would be preferred.  </p><p> </p><p>The successful candidate will work alongside our highly experienced team of geotechnical engineers, environmental scientists and geo-environmental specialists working on projects throughout northern England.  The candidate will work in full cooperation with team members to build a strong team ethos capable of delivering high quality Geo-Environmental engineering services to clients.</p><p> </p><p>Your role will be very much client-facing, so confidence in interfacing with clients on both project delivery and business development matters is essential, as is proficiency in the preparation of bids, tenders and proposals.  You will also mentor more junior staff and assist in their professional development, giving you great potential to develop your management skills.</p><p> </p><p>Candidate Specification        </p><p> </p><p>         Minimum of 10 years professional experience as a Geo-Environmental professional</p><p> </p><p>         Relevant academic/professional qualifications, to include Chartership in either geology, chemistry, engineering or environmental sciences</p><p> </p><p>         Substantial UK contaminated land experience in a professional consultancy environment</p><p> </p><p>         Strong commercial awareness</p><p> </p><p>         Ability to build relationships and to maintain appropriate high level contact with clients</p><p> </p><p>         Full driving licence</p> <p><b>Job Requirements</b> </p>  <p><b>Additional Details</b> </p> <p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC126505 in all correspondence relating to this position.</p><p> </p><p>Please contact Lauren Allen on <a>Lauren.allen@alta-blue.com</a> or on 01224 778144 for more details</p><p> </p>',
      'referencenumber' => 'IRC127427',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Life Sciences',
      'country' => 'GB',
      'location' => '53.4791466,-2.2447445',
    ),
    'attributes' => 
    array (
    ),
  ),
  32 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Electrical Design Engineer',
      'date' => '20150814',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p> </p><p>We are delighted to be supporting one of our clients with a requirement for Electrical Design Engineer. </p><p>Our client is forward thinking muilti-utility connections company which specialise in (energy/utilities/civils and commercial) on a national / local) scale.  This position is being offered on a permanent basis and will be based in Midlands.</p>  <p> </p><p><b>Detailed Description</b> </p> <p>Key tasks undertaken as part of this Electrical Design Engineer role:</p><ul><li>Managing the design through to project lifecycle, to the highest quality and standards.</li><li>Working closely with our Business Development team and Project Managers to compile project bids.</li><li>Proactively taking and promoting our unique offer for utility infrastructure services to our customers.</li><li>Conducting site visits and meeting with customers, therefore excellent customer management skills (internal and external) are essential.</li></ul><p><b>Job Requirements</b> </p> <ul><li>Working effectively within an established team and understanding how to manage customer expectations at all levels. </li></ul><p><b>Additional Details</b> </p> <p>Ideal qualifications, skills and experience:</p><ul><li>Minimum HNC in an engineering discipline.</li><li>Relevant design and costing experience in the utility or similar sector.</li><li>Knowledge of the CDM Regulations and N.R.S.W.A.</li><li>Knowledge of Electricity networks.</li><li>Strong interpersonal skills.</li></ul><p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC 127243 in all correspondence relating to this position.</p><p>If you would like to find out more about the role, call Kim Hill on 01224 777856. </p><p>Online applications preferred however applications via e-mail will be accepted to <a>Kim.Hill@alta-blue.com</a></p>',
      'referencenumber' => 'IRC127423',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Electrical',
      'country' => 'GB',
      'location' => '52.4813679,-1.8980726',
    ),
    'attributes' => 
    array (
    ),
  ),
  33 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Muilti-Utility Design Engineer',
      'date' => '20150813',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p> </p><p>We are delighted to be supporting one of our clients with a requirement for Muilti-Utility Design Engineer. Our client is forward thinking muilti-utility connections company which specialise in (energy/utilities/civils and commercial) on a national / local) scale.  This position is being offered on a permanent basis and will be based in Bristol.</p><p><b>Detailed Description</b> </p> <p>Key tasks undertaken as part of this Multi Utility Engineer role:</p><ul><li>Fully design cost and prepare multi-utility proposals for a range of different new build projects to meet client requirements.</li><li>Interfacing with network asset owners and clients, bid management and safety and commercial project appraisals.</li><li>Take responsibility for managing the design through the lifecycle ensuring quality, safety and costs are controlled at all times.</li><li>Work closely with the sales team and support any client meetings as required to discuss the proposed design.</li><li>Liaise as required with developers, consultants, clients, electricity/gas network operators and IGTs, and water companies.</li><li>Liaise with various internal stakeholders particularly the Operations team</li></ul><p><b>Additional Details</b> </p> <p>Ideal qualifications, skills and experience:</p><ul><li>Minimum HNC / HND in an engineering discipline (or equivalent)</li><li>At least 2 years’ experience in a similar design role in the utility or another similar sector.</li><li>Design and costing experience on new projects and managing all aspects of contracts/tenders within cost, quality and safety parameters.</li><li>Knowledge of CDM Regulations and N.R.S.W.A.</li><li>Knowledge of Electrical, Gas and Water networks.</li><li>Experience of specification, design, maintenance and construction of system plant and apparatus.</li></ul><p><b>How To Apply</b> </p> <p>Please quote job reference IRC 127428 in all correspondence relating to this position.</p><p>If you would like to find out more about the role, call Kim Hill on 01224 777856.</p><p>Online applications preferred however applications via e-mail will be accepted to <a>Kim.Hill@alta-blue.com</a></p>',
      'referencenumber' => 'IRC127428',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Utilities',
      'country' => 'GB',
      'location' => '51.453493,-2.5973937',
    ),
    'attributes' => 
    array (
    ),
  ),
  34 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Electrical Supervisor',
      'date' => '20150813',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p> </p><p>We are delighted to be supporting one of our clients with a requirement for (input job title(s)). Our client is a (small / major / plc etc.) (Organisation / business / consultancy / firm etc.)  which specialise in (civil construction /  financial services / shipping etc.) on a (global / international / national / local) scale.  This position is being offered on a (permanent / interim / contract) basis and will be based in (input location).</p> <p>This is an exciting responsibility for an Electrical Supervisor to oversee the installation of electric mains, services and jointing in accordance health, safety and quality standards. We need you to supervise joining personnel on the increasing number of residential and commercial electrical connections projects across the North West region. </p> <p><b>Detailed Description</b> </p> <ul><li>Develop good relationships with all customers, Site Agents, Project Managers and Team Leaders.</li><li>Ensure each job has been pre-vetted including risk assessment and liaison with land owners.</li><li>Organise work schedules and maintain worker timesheets.</li><li>Conduct site surveys, audits and inspections and ensure that safe working practices are maintained at all times.</li><li>Apply design and planning procedures and requirements, and deliver projects on time, within cost, quality and safety parameters.</li><li>Assist with Jointing fault-finding.</li></ul><p> </p><p><b>Job Requirements</b> </p> <ul><li>Be flexible and work safely and efficiently on clients’/asset distribution systems.</li><li>Excellent customer service principles, and ensure good communication is maintained with the project teams, customers and Site Agents.</li></ul><p><b>Additional Details</b> </p> <ul><li>Craft Qualification, Apprenticeship.</li><li>Experienced in a complex project, utility or similar sector and in LV &amp; HV jointing processes.</li><li>Knowledge of Electrical networks, NRSWA, industrial hazards and Health &amp; Safety precautions.</li><li>Knowledge of specification, design, maintenance and construction of plant and apparatus including control and protection equipment.</li></ul><p> </p><p><b>How To Apply</b> </p> <p> </p><p>Please quote job reference <b>IRC127426 </b>in all correspondence relating to this position.</p><p>If you would like to find out more about the role, call<b> Kim Hill o</b>n<b> 0122477856 </b></p><p><b></b></p><b></b><p>Online applications preferred however applications via e-mail will be accepted to<b> <a>kim.hill@alta-blue.com</a></b></p><p> </p>',
      'referencenumber' => 'IRC127426',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Electrical',
      'country' => 'GB',
      'location' => '52.4813679,-1.8980726',
    ),
    'attributes' => 
    array (
    ),
  ),
  35 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Project Manager',
      'date' => '20150813',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p> </p><p>We are delighted to be supporting one of our clients with a requirement for Project Manager. </p><p>Our client is forward thinking muilti-utility connections company which specialise in (energy/utilities/civils and commercial) on a national / local) scale.  This position is being offered on a permanent basis and will be based in Bristol.<br /><br /></p><p>As Project Manager, you will need to oversee the successful coordination and implementation of accepted electrical, gas, water or telecommunications projects across the region, ensuring they are completed on time and within budget. As a Project Manager you will be involved in a number of exciting projects across the commercial and residential sector, including developments by some of the UK’s biggest construction firms.</p> <p> </p><p><b>Detailed Description</b> </p> <p>Key tasks undertaken as part of this Project Manager role:</p><ul><li>Ensure the job has been pre-vetted including risk assessment and liaison with land owners.</li><li>Project manage multiple multi-utility sites including Residential and Industrial and Commercial, agreeing timescales, costs and resources needed.</li><li>Develop good relationships with Architects, Consultants and Customers, negotiating with contractors and suppliers of materials and services and ensuring sub-contractors have all the relevant training tickets/qualifications, competency and health and safety training</li><li>Deliver the project at the expected level of quality, performance, safety and cost maintaining excellent commercial awareness and budget control.</li><li>Work to achieve excellent customer service principles.</li><li>Use improved working techniques to increase efficiency.</li><li>Be flexible and work safely and efficiently on customers’ asset distribution systems.</li></ul><p><b>Additional Details</b> </p> Ideal qualifications, skills and experience:<ul><li>IOSH or NEBOSH Health &amp; Safety qualification.</li><li>Knowledge of Gas, Water, LV &amp; HV Networks Health &amp; Safety and CDM regulations and Industrial hazards and Health and Safety Precautions.</li><li>Previous ESI experience in engineering role.</li><li>Understanding of legal processes with cable easements, way leaves and land acquisition.</li></ul><p><b>How To Apply</b> </p> <p>Please quote job reference IRC127249 in all correspondence relating to this position.</p><p>If you would like to find out more about the role, call Kim Hill on 01224 777856.</p><p>Online applications preferred however applications via e-mail will be accepted to <a>Kim.Hill@alta-blue.com</a></p>',
      'referencenumber' => 'IRC127429',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Project',
      'country' => 'GB',
      'location' => '51.453493,-2.5973937',
    ),
    'attributes' => 
    array (
    ),
  ),
  36 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Project Manager',
      'date' => '20150813',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p> </p><p>We are delighted to be supporting one of our clients with a requirement for Project Manager. </p><p>Our client is forward thinking muilti-utility connections company which specialise in (energy/utilities/civils and commercial) on a national / local) scale.  This position is being offered on a permanent basis and will be based in Bristol.<br /><br /></p><p>As Project Manager, you will need to oversee the successful coordination and implementation of accepted electrical, gas, water or telecommunications projects across the region, ensuring they are completed on time and within budget. As a Project Manager you will be involved in a number of exciting projects across the commercial and residential sector, including developments by some of the UK’s biggest construction firms.</p><p><b>Detailed Description</b> </p> <p>Key tasks undertaken as part of this Project Manager role:</p><ul><li>Ensure the job has been pre-vetted including risk assessment and liaison with land owners.</li><li>Project manage multiple multi-utility sites including Residential and Industrial and Commercial, agreeing timescales, costs and resources needed.</li><li>Develop good relationships with Architects, Consultants and Customers, negotiating with contractors and suppliers of materials and services and ensuring sub-contractors have all the relevant training tickets/qualifications, competency and health and safety training</li><li>Deliver the project at the expected level of quality, performance, safety and cost maintaining excellent commercial awareness and budget control.</li><li>Work to achieve excellent customer service principles.</li><li>Use improved working techniques to increase efficiency.</li><li>Be flexible and work safely and efficiently on customers’ asset distribution systems.</li></ul><p><b>Additional Details</b> </p> <p>Ideal qualifications, skills and experience:</p><ul><li>HNC – Electrical Engineering or other equivalent qualifications</li><li>IOSH or NEBOSH Health &amp; Safety qualification.</li><li>Knowledge of Gas, Water, LV &amp; HV Networks Health &amp; Safety and CDM regulations and Industrial hazards and Health and Safety Precautions.</li><li>Previous ESI experience in engineering role.</li><li>Understanding of legal processes with cable easements, way leaves and land acquisition.</li></ul> <p><b>How To Apply</b> </p> <p>Please quote job reference IRC 127431 in all correspondence relating to this position.</p><p>If you would like to find out more about the role, call Kim Hill on 01227 777856.</p><p>Online applications preferred however applications via e-mail will be accepted to <a>Kim.Hill@alta-blue.com</a></p> ',
      'referencenumber' => 'IRC127431',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Project',
      'country' => 'GB',
      'location' => '51.5111014,-0.5940682',
    ),
    'attributes' => 
    array (
    ),
  ),
  37 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Civil Supervisor',
      'date' => '20150813',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p> </p><p>Altablue is working with an up and coming Multi-Utility Connections company who are rapidly expanding. The client has an established track record with some of the countries biggest developers and have worked on some large scale residential and commercial developments.</p><p> </p><p>The clients Multi Utility team are vital to the companies business and they require the best people to be part of this team. The Supervisor will be responsible for ensuring all Gas, Electric and Water civils works assigned are completed on time and with an emphasis on quality and safety. </p><p><b>Detailed Description</b> </p> <p>The main duties of this role will include but not limited to:</p><ul><li>Comply with all relevant legislative and regulatory requirements.</li><li>Work within authorised safety and technical competencies.</li><li>Create a safe working environment for all works (including Gas, Water and Electric network operations), and continuously monitor, taking into consideration all potential associated risks and placing control measures where required whilst having knowledge of Emergency First Aid.</li><li>Prepare for linear excavation works by the correct identification of existing plant and use of trail holes to confirm route.</li><li>Liaise with commissioning company utility infrastructures as required but within authorised competencies (including building and testing).</li><li>Segregate the area for site works and highway works during Gas, Water and Electric network operations.</li><li>Take part in engineering activities.</li><li>Carry out excavations as required to enable installation, connection and or reinstatement works. </li><li>Protect installed company utility infrastructures by installation of sand base and sand cover as required.</li><li>Contribute to effective working relationships.</li><li>Confidently operate, maintain and ensure security of powered tools, plant and equipment.</li><li>Be confident in cable laying, winch training, Gas Meter fitting and manual handling.</li></ul> <p><b>Job Requirements</b> </p> <ul><li><p> The ability to follow written and spoken instructions and good team-working skills.</p></li></ul><p><b>Additional Details</b> </p> <p>The ideal qualifications, skills and experience for this role are:</p><ul><li>Full Driving license (plus ideally CPCS Excavator up to 10 tonne, and CPCS Dumper).</li><li>CSCS Card, EU Skills Card, Gas and Water SHEA Card, Water Hygiene Card.</li><li>GCSE or equivalent vocational qualifications.</li><li>GN02 main layer up to 180mm or above.</li><li>NCO Level 2 (Water Mains and Services).</li><li>NRSWA Ticket.</li></ul><p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC 127415 in all correspondence relating to this position.</p><p>If you would like to find out more about the role, call Richard Thompson on 01224 777813</p><p>Online applications preferred however applications via e-mail will be accepted to <a>Richard.Thompson@alta-blue.com</a></p>',
      'referencenumber' => 'IRC127415',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Utilities',
      'country' => 'GB',
      'location' => '51.5111014,-0.5940682',
    ),
    'attributes' => 
    array (
    ),
  ),
  38 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Project Manager',
      'date' => '20150813',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p> </p>We are delighted to be supporting one of our clients with a requirement for Water Design Engineer. <p>Our client is forward thinking muilti-utility connections company which specialise in (energy/utilities/civils and commercial) on a national / local) scale.  This position is being offered on a permanent basis and will be based in Barnsley.</p><p>You will be joining an established and welcoming Project Management team based in our recently refurbished office in Warrington Depot. The role of Project Manager is vital to oversee the successful coordination and implementation of accepted electrical, gas, water or telecommunications projects across the region, ensuring they are completed on time and within budget. As a Project Manager  you will be involved in a number of exciting projects across the commercial and residential sector, including developments by some of the UK’s biggest construction firms.</p> <p><b>Detailed Description</b> </p> <p>Key tasks undertaken as part of this Project Manager role:</p><ul><li>Ensure the job has been pre-vetted including risk assessment and liaison with land owners.</li><li>Project manage multiple multi-utility sites including Residential and Industrial and Commercial, agreeing timescales, costs and resources needed.</li><li>Develop good relationships with Architects, Consultants and Customers, negotiating with contractors and suppliers of materials and services and ensuring sub-contractors have all the relevant training tickets/qualifications, competency and</li><li>health and safety training.</li><li>Deliver the project at the expected level of quality, performance, safety and cost maintaining excellent commercial awareness and budget control.</li><li>Work to achieve excellent customer service principles.</li><li>Ensure the job has been pre-vetted including risk assessment and liaison with land owners.</li><li>Use improved working techniques to increase efficiency.</li><li>Be flexible and work safely and efficiently on customers’ asset distribution systems.</li></ul><p><b>Job Requirements</b> </p> <p><b>Additional Details</b> </p> <p>Essential qualifications, skills and experience:</p><ul><li>HNC – Electrical Engineering or other equivalent qualification.</li><li>IOSH or NEBOSH Health &amp; Safety qualification.</li><li>SMSTS (Site Managers Safety Training Scheme)</li><li>Knowledge of Gas, Water, LV &amp; HV Networks Health &amp; Safety and CDM regulations and Industrial hazards and Health and Safety Precautions.</li><li>Strong commercial acumen</li><li>Previous ESI experience in engineering role.</li><li>Understanding of legal processes with cable easements, wayleaves and land acquisition.</li></ul><p><b>How To Apply</b> </p> <p>Please quote job reference IRC127432 in all correspondence relating to this position.</p><p>If you would like to find out more about the role, call Kim Hill on 01224 777856</p><p>Online applications preferred however applications via e-mail will be accepted to <a>Kim.Hill@alta-blue.com</a></p><p> </p>',
      'referencenumber' => 'IRC127432',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Project',
      'country' => 'GB',
      'location' => '53.40180345,-2.56803697272251',
    ),
    'attributes' => 
    array (
    ),
  ),
  39 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Multi Utility Supervisor',
      'date' => '20150813',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p>We are delighted to be supporting one of our clients with a requirement for (input job title(s)). Our client is a (small / major / plc etc.) (Organisation / business / consultancy / firm etc.)  which specialise in (civil construction /  financial services / shipping etc.) on a (global / international / national / local) scale.  This position is being offered on a (permanent / interim / contract) basis and will be based in (input location).</p><p>Altablue is working with an up and coming Multi-Utility Connections company who are rapidly expanding. The client has an established track record with some of the countries biggest developers and have worked on some large scale residential and commercial developments.</p><p>The clients Multi Utility team are vital to the companies business and they require the best people to be part of this team. The Supervisor will be responsible for ensuring all Gas, Electric and Water civils works assigned are completed on time and with an emphasis on quality and safety. </p><p><b>Detailed Description</b> </p> <ul><li>Develop good relationships with all customers, site agents, project managers and team leaders, and provide site support for customer enquiries.</li><li>Create job packs by ensuring job cards are correct, method statements and generic risk assessments are included, issue blank risk assessments, issue ‘approved’ construction drawings, and as-laid records/statutory records are accurate and returned to CAD within 5 days.</li><li>Resource planning to include regular meetings with supplier (GAP) representatives, ensuring to report damages and theft in line with procedure.</li><li>Conduct site surveys/audits and inspections i.e. one audit per team, per month.</li><li>Ensure that safe working practices are maintained at all times, and ensure that all jobs have been pre-vetted including risk assessment and liaison with land owners.</li><li>Hire plant – ensure hire/off hire plant efficiently and weekly control checks and that all materials are all correctly ordered aggregates ordered in advance, 3 days’ notice.</li><li>Ensure Management of Symology Road Opening Notices.</li><li>Adhere to weekly and daily deadlines for checking and signing vehicle sheets and timesheets, and ensure that they are passed to Support Services.</li><li>Assist with the successful resolution of any complaints.</li></ul><p> </p><p><b>Job Requirements</b> </p> <ul><li>Excellent organisational skills and high quality verbal and written communication.</li><li>Knowledge of Gas, Water and Electricity networks</li><li>Minimum of 2 years supervisory / people-management experience, and excellent customer service principles.</li></ul><p><b>Additional Details</b> </p> <ul><li>Craft Qualification, Apprenticeship or equivalent.</li><li>NVQ Level 3 Supervisory</li><li>NCO(W) Level 3</li></ul><p><b>How To Apply</b> </p> <p>Please quote job reference IRC127416 in all correspondence relating to this position.</p><p>If you would like to find out more about the role, call Richard Thompson on 01224777813</p><p>Online applications preferred however applications via e-mail will be accepted to <a>Richard.thompson@alta-blue.com</a></p> ',
      'referencenumber' => 'IRC127416',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Utilities',
      'country' => 'GB',
      'location' => '53.40180345,-2.56803697272251',
    ),
    'attributes' => 
    array (
    ),
  ),
  40 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Civil Supervisor',
      'date' => '20150818',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p> </p><p>Altablue is working with an up and coming Multi-Utility Connections company who are rapidly expanding. The client has an established track record with some of the countries biggest developers and have worked on some large scale residential and commercial developments.</p><p> </p><p>The clients Multi Utility team are vital to the companies business and they require the best people to be part of this team. The Supervisor will be responsible for ensuring all Gas, Electric and Water civils works assigned are completed on time and with an emphasis on quality and safety. </p><p> </p><p><b>Detailed Description</b> </p> <p>The main duties of this role will include but not limited to:</p><ul><li>Comply with all relevant legislative and regulatory requirements.</li><li>Work within authorised safety and technical competencies.</li><li>Create a safe working environment for all works (including Gas, Water and Electric network operations), and continuously monitor, taking into consideration all potential associated risks and placing control measures where required whilst having knowledge of Emergency First Aid.</li><li>Prepare for linear excavation works by the correct identification of existing plant and use of trail holes to confirm route.</li><li>Liaise with commissioning company utility infrastructures as required but within authorised competencies (including building and testing).</li><li>Segregate the area for site works and highway works during Gas, Water and Electric network operations.</li><li>Take part in engineering activities.</li><li>Carry out excavations as required to enable installation, connection and or reinstatement works. </li><li>Protect installed company utility infrastructures by installation of sand base and sand cover as required.</li><li>Contribute to effective working relationships.</li><li>Confidently operate, maintain and ensure security of powered tools, plant and equipment.</li><li>Be confident in cable laying, winch training, Gas Meter fitting and manual handling.</li></ul><p><b>Job Requirements</b> </p> <p>The ability to follow written and spoken instructions and good team-working skills.</p><p><b>Additional Details</b> </p> <p>The ideal qualifications, skills and experience for this role are:</p><ul><li>Full Driving licence (plus ideally CPCS Excavator up to 10 tonne, and CPCS Dumper).</li><li>CSCS Card, EU Skills Card, Gas and Water SHEA Card, Water Hygiene Card.</li><li>GCSE or equivalent vocational qualifications.</li><li>GN02 main layer up to 180mm or above.</li><li>NCO Level 2 (Water Mains and Services).</li><li>NRSWA Ticket.</li></ul> <p><b>How To Apply</b> </p> <p>Please quote job reference IRC127417 in all correspondence relating to this position.</p><p>If you would like to find out more about the role, call Richard Thompson on 01224 777813. </p><p>Online applications preferred however applications via e-mail will be accepted to richard.thompson@alta-blue.com.</p> ',
      'referencenumber' => 'IRC127417',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Utilities',
      'country' => 'GB',
      'location' => '51.453493,-2.5973937',
    ),
    'attributes' => 
    array (
    ),
  ),
  41 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Multi Utility Supervisor',
      'date' => '20150813',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p> </p><p>Altablue is working with an up and coming Multi-Utility Connections company who are rapidly expanding. The client has an established track record with some of the countries biggest developers and have worked on some large scale residential and commercial developments.</p><p>The clients Multi Utility team are vital to the companies business and they require the best people to be part of this team. The Supervisor will be responsible for ensuring all Gas, Electric and Water civil works assigned are completed on time and with an emphasis on quality and safety. </p> <p><b>Detailed Description</b> </p> <p>The main duties of this role will include but not limited to:</p><ul><li>Comply with all relevant legislative and regulatory requirements</li><li>Work only within your authorised safety and technical competencies</li><li>Ensuring all work is completed with total emphasis on Safety and Quality</li><li>Create a safe working environment for all works considering all potential associated risks and placing control measure where required</li><li>Follow all given Company Procedures and Guidelines</li><li>Continuously monitor works and maintain safe working environment</li><li>Prepare for linear excavation works by the correct identification of existing plant and use of trail holes to confirm route</li><li>Carry out excavations as required to enable installation, connection and or reinstatement works</li><li>Prepare, build, test and commission company utility infrastructures as required within your authorised competencies</li><li>Protect installed company utility infrastructures by installation of sand base and sand cover as required</li><li>Carry out and assist with reinstatement activities</li><li>Contribute to an efficient and effective work environment during gas, water and electric network operations</li><li>Contribute to health, safety &amp; environment in the workplace during gas, water and electric network operations</li><li>Operate powered tools and equipment</li><li>Prepare resources &amp; segregate the area for highway works during gas, water and electric network operations</li><li>Prepare resources &amp; segregate the area for site works during gas, water and electric network operations</li><li>Prepare work areas &amp; materials for engineering activities as appropriate</li><li>Contribute to the organisation of work activities</li><li>Contribute to effective working relationships</li><li>Maintain plant and tools – ensure security of all equipment</li><li>Ensure that the Company’s safe working practices are followed at all times</li></ul><p> </p><p><b>Job Requirements</b> </p> <ul><li>Other skills and knowledge</li><li>Cable laying</li><li>The ability to follow written and spoken instructions.</li><li>Good team-working skills.</li><li>An understanding of on-site health and safety.</li><li>Winch Training</li><li>Gas Meter Fitting</li><li>Manual Handling</li></ul><p><b>Additional Details</b> </p>  The ideal qualifications, skills and experience for this role are:<ul><li>CSCS Card/EU Skills Card</li><li>GN02 main layer up to 180mm or above</li><li>Gas and Water SHEA Card</li><li>Water Hygiene Card</li><li>NCO Level 2 (Water Mains and Services)</li><li>NRSWA Ticket</li><li>First Aid</li><li>CPCS Excavator up to 10 tonne.</li><li>CPCS Dumper<br /><br /></li></ul><p><b>How To Apply</b> </p> <p> </p><p>Please quote job reference <strong>IRC</strong><strong>127420</strong> in all correspondence relating to this position.</p><p> </p><p>If you would like to find out more about the role, call Richard Thompson on<b>  01224 777813 </b></p><p><b></b></p><b></b><p><b></b></p><p><b></b></p><b></b><p> </p><p>Online applications preferred however applications via e-mail will be accepted to<b> <a>Richard.thompson@alta-blue.com</a></b></p><p> </p><p> </p>',
      'referencenumber' => 'IRC127420',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Utilities',
      'country' => 'GB',
      'location' => '51.5111014,-0.5940682',
    ),
    'attributes' => 
    array (
    ),
  ),
  42 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Water Design Engineer',
      'date' => '20150813',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p>We are delighted to be supporting one of our clients with a requirement for Water Design Engineer. Our client is forward thinking muilti-utility connections company which specialise in (energy/utilities/civils and commercial) on a national / local scale.  This position is being offered on a permanent basis and will be based in Barnsley.</p><p>.</p><p><b></b></p><p><b></b></p><b></b> <p><b>Detailed Description</b> </p> <p><strong>Job Purpose:</strong></p><ul><li>To design, cost and approve Water infrastructure for residential and non-domestic developments including flatted properties. This will include the delivery and completion of all work including variations. Other responsibilities include testing and commissioning of water mains, services and pressure reduction equipment</li></ul><p> </p><p> <b>Main Accountabilities</b></p><p><b></b></p><b></b><ul><li>Fully design and cost the water infrastructure for housing, business-to-business or Industrial and Commercial projects.</li><li>Manage the design through the lifecycle ensuring quality, safety and costs are controlled at all times.</li><li>Liaise closely with the Sales team.</li><li>Provide comprehensive operational support.</li><li>Ensure quality, safety and customer satisfaction.</li><li>Conduct water site surveys/audits and inspections</li><li>Provide water advisory support for customer enquiries and assist with the successful resolution of complaints.</li><li>Ensure that water safe working practices are maintained at all times.</li><li>Liaise with developers, consultants, customers and water companies</li><li>Ensure good communication is maintained with the Operations department.</li><li>Comply with the latest water legislation and industry standards (WIRS).</li></ul><p> </p><p>The above is not an exhaustive list of all duties.</p><p><b>Job Requirements</b> </p> <p> </p><p><b>Employee Specification/Knowledge &amp; Skills</b></p><p><b></b></p><b></b><ul><li>Authorised Engineer registration</li><li>NCO qualification</li></ul><p> </p><p><b>Additional Details</b> </p> <p><strong>Other skills and knowledge:</strong></p><p><strong></strong></p><strong></strong><ul><li>Organisational skills to manage several projects at the same time.</li><li>Strong communication skills and ability to build strong relationships.</li><li>Excellent organisation skills.</li><li>IT competent</li></ul><p> </p><p><strong>Knowledge of:</strong></p><p><strong></strong></p><strong></strong><ul><li>CDM Regulations.</li><li>NRSWA Supervisory Card.</li><li>GAS Shea (EU Skills Card).</li></ul><p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference <b>IRC127433 </b> in all correspondence relating to this position.</p><p>If you would like to find out more about the role, call<b> Kim Hill </b>on<b> Tel: 01224 777856 </b></p><p><b></b></p><b></b><p>Online applications preferred however applications via e-mail will be accepted to<b> <a>kim.hill@alta-blue.com</a></b></p><p> </p>',
      'referencenumber' => 'IRC127433',
      'postalcode' => 'S72 7BQ',
      'company' => 'Altablue',
      'discipline' => 'Designer',
      'country' => 'GB',
      'location' => '53.570977859672,-1.3851175929756',
    ),
    'attributes' => 
    array (
    ),
  ),
  43 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Business Development Manager',
      'date' => '20150824',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p> </p><p>We are delighted to be supporting one of our clients with a requirement for a Business Development Manager. Our client is a forward thinking multi-utility connections company who provide Utilities services across a range of sectors in the UK.  This company is growing fast and has plans to double their workforce and significantly increase their market share over the next 4 years. Due to this expansion they need a well-connected Business development manager to support with their growth plans in the North of England. This position will be based from home with regular travel across the region. The role will focus on developing new business to agreed objectives in the new build market sector.</p> <p><b>Detailed Description</b> </p> <ul><li><p> Identify and gather intelligence on project developments and clients</p></li><li>Strategically develop project opportunities through the sales cycle</li><li><p>Build relations with clients and other influencers on various levels to support both current and future projects</p></li><li><p>Planning and conducting CPD sales presentations to prospective clients/stakeholders</p></li><li><p>Managing relations between various external and internal stakeholders to ensure projects are progressed to client requirements and timescales</p></li><li><p>Working with the design team to write formal and compelling proposals</p></li><li><p>Indirect management of internal resources to ensure targets are understood and customer service expectations are achieved</p><p> </p></li><li><p>Own and oversee the overall client relationship</p><p>Work with marketing to help develop the brand, proposition and customer contact database and our competitive position</p></li></ul><p><b>Job Requirements</b> </p> <p>To be successful in the role, potential candidates should possess:</p><ul><li><p>Business development and strategic sales experience within a professional technical services sector</p></li><li><p>Construction, FM, Metering or utilities background is preferred, ideally in the construction/utility connections or related service sector (desirable)</p></li><li><p>Well-connected in the new build, M&amp;E consultants and principle contractor sectors is highly desirable</p></li><li><p>A well organised and focused approach to personal strategic sales planning</p></li><li><p>Action orientated, results focused with a strong drive to achieve sales targets</p></li><li><p>Understands and applies a consultative selling style that educates clients with alternative solutions to their </p><p>current products and services</p></li><li><p>Strong ability to build trust and credibility quickly whilst leading clients through the consultative sales process</p></li><li><p>Excellent communication skills, comfortable in presenting technical information in a confident and articulate manner</p></li><li><p>Good analytical skills with the ability to qualify projects both technically and commercially</p></li><li><p>Commercially astute with excellent negotiation skills</p></li></ul><p><b>Additional Details</b> </p> <p>Education and Qualifications:</p><ul><li>Educated to at least A level or equivalent standard</li><li>Power systems/Electrical Engineering knowledge and experience is preferred</li></ul><p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC 127456 in all correspondence relating to this position.</p><p>If you would like to find out more about the role, call Emily Earley on 01224 763622.</p><p>Online applications preferred however applications via e-mail will be accepted to <a>Emily.Earley@alta-blue.com</a></p>',
      'referencenumber' => 'IRC127456',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Business Development',
      'country' => 'GB',
      'location' => '53.40180345,-2.56803697272251',
    ),
    'attributes' => 
    array (
    ),
  ),
  44 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Business Development Manager',
      'date' => '20150824',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p>We are delighted to be supporting one of our clients with a requirement for a Business Development Manager. Our client is a forward thinking multi-utility connections company who provide Utilities services across a range of sectors in the UK.  This company is growing fast and has plans to double their workforce and significantly increase their market share over the next 4 years. Due to this expansion they need a well-connected Business development manager to support with their growth plans in Scotland. This position will be based from home with regular travel across the region. The role will focus on developing new business to agreed objectives.</p><p> </p><p><b>Detailed Description</b> </p> <p>Key tasks undertaken as part of this BDM role:</p><p> </p><ul><li>Proactively develop new business accounts to agree targets.</li><li>Plan, organise and prioritise sales activities to achieve agreed targets from both new and existing housing developer accounts.</li><li>Successfully manage a portfolio of key accounts and ensure high levels of repeat business and customer satisfaction.</li><li>Manage and interpret customer needs by gaining a good understanding of the customers’ development plans and by developing an excellent working relationship.</li><li>Use creativity and initiative to both manage accounts and develop new business opportunities.</li><li>Adopt a flexible, team working approach to build rapport quickly, develop excellent relations with customers and colleagues, and ensure total commitment to exceeding customer needs.</li><li>Work closely with the Design and administration team to ensure quotations are issued to agreed timescales.</li><li>Maintain and develop detailed knowledge of the service proposition’s key features and unique benefits.</li><li>Maintain a high awareness of competitor propositions and regional activity.</li><li>Input to marketing/promotional activity and supporting marketing events where required.</li><li>Use good analytical and negotiation skills to tackle any issues in a positive, proactive and practical way.</li><li>Deliver sales presentations to both internal and external audiences as required.</li><li>Provide monthly sales reports to deadlines including trends, sales performance to target, competitor intelligence etc.</li><li>Be alert to identify key business opportunities for Energetics across both sectors.</li></ul><p><b>Job Requirements</b> </p> <p>Essential qualifications, skills and experience:</p><ul><li>At least five years successful sales experience in a similar B2B role ideally within the utility connections, construction or a similar sector.</li><li>Articulate, persuasive and driven to achieve.</li><li> Well organised with excellent attention to detail.</li></ul> <p><b>Additional Details</b> </p> <ul><li>  Educated to A Level/HNC level or equivalent.</li></ul><p><b>How To Apply</b> </p> <p>Please quote job reference <b>IRC127457 </b>in all correspondence relating to this position.</p><p> </p>If you would like to find out more about the role, call<b> Emily Earley  </b>on<b> 01224 763662. </b><p><b></b></p><b></b><p>Online applications preferred however applications via e-mail will be accepted to<b> emilyearley@alta-blue.com</b></p><p><b></b></p><b></b><p> </p>',
      'referencenumber' => 'IRC127457',
      'postalcode' => 'G2 4GZ',
      'company' => 'Altablue',
      'discipline' => 'Business Development',
      'country' => 'GB',
      'location' => '55.864767279868,-4.2659738888787',
    ),
    'attributes' => 
    array (
    ),
  ),
  45 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Commercial Assistant',
      'date' => '20150817',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p> </p><p>We are delighted to be supporting one of our clients with a requirement for a Commercial Assistant located in their Warrington offices. Our client is a forward thinking multi-utility connections company who provide Utilities services across a range of sectors in the UK.  This company is growing fast and has plans to double their workforce and significantly increase their market share over the next 4 years. The position of Commercial Assistant is an exciting opportunity for an individual who already possess sound commercial and analytical skills and who is looking to gain more experience alongside a quantity surveyor within the utilities industry. In this position the individual will implement excellent organizational and project management skills whilst assisting and managing the day to day commercial administrative duties working alongside, and on behalf of the Quantity Surveyor in a busy project environment.</p><p> </p><p><b>Detailed Description</b> </p> <p> </p>Key tasks undertaken as part of this Commercial Assistant role:<ul><li>In conjunction with the Quantity Surveyor, undertake small scale accounts activity with subcontractors, including management of invoices and weekly statements, and preparation of payments for sign off by the Quantity Surveyor</li><li>Generate customer billing and account reconciliations whilst reviewing previous billing including variations within the various systems and provide an update to the Quantity Surveyor to ensure weekly targets are met</li><li>Advise and assist Design department where necessary with estimating and commercial matters<br />Prepare, update and submit aged debt report to the Quantity Surveyor on a weekly basis for analysis and submission to Project Managers and the Exec Team</li><li>In conjunction with the Quantity Surveyor, manage and resolve aged debt disputes including preparation and collation of relevant information</li><li>Maintain and manage the Sub-Contractors approval database</li><li><p>Administrate and manage project closures for sign off by the Quantity Surveyor</p><br /><br /><br /><br /><br /><br /></li></ul><p> </p><p><b>Job Requirements</b> </p> <p>Ideal qualifications, skills and experience:</p><ul><li>Academic background, demonstrating a numerate and/or analytical discipline, showing the ability to interpret and present data.</li><li>Experience within the utilities industry sector</li><li>Attention to detail whilst managing several projects at the same time</li><li>Good team collaboration as well as influencing and negotiation skills</li></ul><p><b>Additional Details</b> </p> <p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC 127458 in all correspondence relating to this position.</p><p>If you would like to find out more about the role, call Emily Earley on 01224 763662.</p><p>Online applications preferred however applications via e-mail will be accepted to <a>Emily.Earley@alta-blue.com</a></p>',
      'referencenumber' => 'IRC127458',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Commercial',
      'country' => 'GB',
      'location' => '53.40180345,-2.56803697272251',
    ),
    'attributes' => 
    array (
    ),
  ),
  46 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Planning and Support Manager',
      'date' => '20150817',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p>We are delighted to be supporting one of our clients with a requirement for a Planning &amp; Support Manager based in their Warrington Office. Our client is a forward thinking multi-utility connections company who provide Utilities services across a range of sectors in the UK.  This company is growing fast and has plans to double their workforce and significantly increase their market share over the next 4 years. Due to this expansion they need this critical position to be able to ensure the Project Management and Operational departments have the required support to allow them to deliver multi utility construction projects within the North of England. The support function includes a variety of disciplines covering all aspects of Planning, scheduling, process implementation and administrative duties throughout the full cycle of the project. This will be an ideal positon for someone with exceptional customer care principles who has the experience in managing teams focused on delivery within a busy project environment.  </p><p> </p><p> </p><p><b>Detailed Description</b> </p> <p>Key tasks undertaken as part of this Planning &amp; Support Manager role:</p><ul><li>Day to Day Management of a multi skilled Operational Support Team including Administrators, Schedulers, Co-ordinators and CAD Operators</li><li>Provide the essential support to the Project Management and Operations department to allow them to deliver projects safely, on time and within budget</li><li>Support the Regional Manager in the preparation of company budgets, strategic plans and contribute to the development, communication and ultimate delivery of these plans within your area of responsibility</li><li>Mentor and motivate your direct reports conducting periodic performance evaluations to ensure they are achieving individual targets and contributing to the overall regional and company performance</li><li>Address any customer concern in a timely and effective manner and also ensure that these high standards are maintained across your entire team</li><li>Coordinate all support functions and ensure the team are undertaking their roles in accordance with Energetics requirements, whilst managing Reports workloads and working with the Senior Project Manager and Operations Manager to forecast resource requirements</li><li>Manage any project issues and support Reports in the resolution of challenges within their roles</li><li>Responsible for the implementation of best practice processes and procedures and driving continual improvement within the team</li><li>Monthly &amp; Quarterly Reporting inclusive of planning lead times, forecast work bank, non-conformances, missed appointments, performance and escalations</li><li>Ensure all asset adopting companies are notified of proposed works through detailed and accurate whereabouts</li><li>Manage and control all road opening notices to ensure compliance with NRSWA and section 74</li><li>Develop good relationships with the customer and the customer’s representatives including architects, consultants and site managers</li><li>Ensure that safe working practices are maintained at all times</li><li>Responsible for and take lead role for health, safety and work quality</li><li>Use improved working techniques to increase efficiency</li></ul><p><b>Job Requirements</b> </p> <p>Ideal qualifications, skills and experience:</p><ul><li>Proven experience of operating within in a multi-utility, or similar industry and managing a support team including Administrators, Schedulers, Co-ordinators and CAD Operators</li><li>A good understanding of business and budget control with excellent organisational planning, time management skills and communication</li><li>Logical thinking with creative problem solving ability and confidence to control various factions simultaneous</li></ul><p> </p><p><b>Additional Details</b> </p> Knowledge of Computer Aided Design (CAD)Understanding of NRSWA noticing including permit schemes (desirable)<p><b>How To Apply</b> </p> <p>Please quote job reference <b>IRC127459 </b>in all correspondence relating to this position.</p><p>If you would like to find out more about the role, call<b> Emily Earley  </b>on<b>  Tel: 01224 763662 </b></p><p><b></b></p><b></b><p>Online applications preferred however applications via e-mail will be accepted to<b> Emily.earley@alta-blue.com</b></p><p><b></b></p><b></b><p> </p>',
      'referencenumber' => 'IRC127459',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Planning',
      'country' => 'GB',
      'location' => '53.40180345,-2.56803697272251',
    ),
    'attributes' => 
    array (
    ),
  ),
  47 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Marketing Coordinator',
      'date' => '20150817',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p> <p>We are delighted to be supporting one of our clients with a requirement for a Marketing Coordinator located in their Midlands office. Our client is a forward thinking multi-utility connections company who provide Utilities services across a range of sectors in the UK.  This company is growing fast and has plans to double their workforce and significantly increase their market share over the next 4 years. Due to this expansion they need an individual who has previous experience in working as part of a busy marketing team to support with both internal and external communications. This will be ab excellent opportunity for an individual who is up to date with all of the latest marketing tools and who is looking to develop their career in marketing within a busy project environment. </p><p><b>Detailed Description</b> </p> <ul><li>Social Media set up and day-to-day management e.g. LinkedIn news updates, encouraging employee listing in the correct format</li><li>Research potential internal news stories for the website/social channels and write up and distribute accordingly</li><li>PR – Work with internal staff and the management team to generate news stories</li><li>Prepare PR story brief for the PR agency, working with them to edit and finalise releases</li><li>Set up and manage brand/marketing library allowing staff easy content access for presentations, case studies, brochures, mailing lists etc.</li><li>Research and write up internal and customer newsletters</li><li>Oversee implementation of brand templates – forms, presentations etc. and work with operations to refresh van livery and PPE etc.</li><li>Copywriting support for literature, direct mail, support to bid management etc.</li><li>Working with design agencies to produce communication pieces and provide copywriting and design support as required</li><li>Internal communications – supporting the Group Marketing Manager in delivering the content and design interface for the intranet</li><li>Day-to-day editing and content management of the group intranet</li><li>Regional marketing support for Design Managers</li><li>General Ad-hoc support to the marketing team as required<br /></li></ul><p><br /> </p><p><b>Job Requirements</b> </p> <p> </p> Previous experience of working in a marketing support role, particularly external/internal communicationsGood working knowledge of marketing communication principles and techniquesGood writing capability and experience in preparing and writing internal news and external communication for web updates and social mediaUp to date with modern communication tools and digital marketing channelsExperience in Construction or Utilities Sector (preferred)<p><b>Additional Details</b> </p> <p> </p><ul><li>Education and Qualifications Educated to at least GCSCE standard</li><li>A degree in a marketing business or other relevant subject would be preferred </li></ul><p> </p> <p> </p><p> </p><p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference IRC127461 in all correspondence relating to this position.</p><p>If you would like to find out more about the role, call Emily Earley on 01224 763662.</p><p>Online applications preferred however applications via e-mail will be accepted to <a>Emily.Earley@alta-blue.com</a></p>',
      'referencenumber' => 'IRC127461',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Utilities',
      'country' => 'GB',
      'location' => '52.4813679,-1.8980726',
    ),
    'attributes' => 
    array (
    ),
  ),
  48 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Administrator',
      'date' => '20150817',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p> <p>We are delighted to be supporting one of our clients with a requirement for an Administrator located in their Barnsley office. Our client is a forward thinking multi-utility connections company who provide Utilities services across a range of sectors in the UK.  This company is growing fast and has plans to double their workforce and significantly increase their market share over the next 4 years. Due to this expansion they need an administrator who can work as part of a team to provide clerical support to all parts of the business relating to project activities. This will be a great opportunity for some with great administrative skills who is looking to develop their career within the utilities sector. </p><p><b>Detailed Description</b> </p> <p>Main Accountabilities:</p><ul><li>Excellent customer service principals</li><li>Manage correspondence and communication within the business</li><li>Undertake data input/retrieval and run routine reports to support various parts of the business, and in accordance within business processes</li><li>Provide clerical support by producing a range of documents according to business templates/formats</li><li>Update and maintain relevant filing systems, paperwork, databases and repots during the lifespan of the project</li><li>Ensure all paperwork is processed efficiently and accurately to a high standard</li><li>Ability to work as part of a team including sharing information/knowledge and training of existing/new members of staff</li><li>Ensure quality standards are maintained in all activities performed</li><li>Produce reports at the key dates within the business calendar</li><li>Answer the telephone and respond to ensure a prompt answer to queries and requests for information</li></ul><p><b>Job Requirements</b> </p> <p>Employee Specification/Knowledge &amp; Skills:</p><ul><li><p>Must have previous experience within a similar role</p></li><li><p>Experience within the utilities or construction industry is preferred </p></li><li><p>Proficient in the use of all Microsoft packages</p></li></ul><p><b>Additional Details</b> </p> <p>Education and Qualifications:</p><ul><li>5 GCSE’s at Grade C or above or equivalent</li></ul><p><b>How To Apply</b> </p> <p>Please quote job reference IRC127472 in all correspondence relating to this position.</p><p> </p><p>If you would like to find out more about the role, call Emily Earley on 01224 763662.</p><p> </p><p>Online applications preferred however applications via e-mail will be accepted to <a>Emily.Earley@alta-blue.com</a></p> ',
      'referencenumber' => 'IRC127472',
      'postalcode' => 'S72 7BQ',
      'company' => 'Altablue',
      'discipline' => 'Administration',
      'country' => 'GB',
      'location' => '53.570977859672,-1.3851175929756',
    ),
    'attributes' => 
    array (
    ),
  ),
  49 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Training and Organisational Development Officer',
      'date' => '20150818',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p>Altablue is working with an up and coming Multi-Utility Connections company who are rapidly expanding. The client has an established track record with some of the country’s biggest developers and have worked on large scale residential and commercial developments.</p><p><b>Detailed Description</b> </p> <p>As a result of the growth of the organisation they are keen to engage with Training, Learning and Development professionals with at least 2 years’ experience working in a similar role. The role will involve the creation and delivery of training services in line with the strategic aims of the organisation. Training objectives will include the provision of “on the job” training, inductions and apprenticeship schemes.</p><p><b>Job Requirements</b> </p> <p>The ideal candidate will have experience of a full training life cycle including design, delivery and evaluation. This is an exciting opportunity to get involved with a growing organisation to assist in shaping their development culture. </p><p> </p><ul><li>The ideal candidate would possess the following:</li><li>Previous experience working in the utilities or construction sector</li><li>At least two years’ experience working in a similar role</li><li>Confident in communicating with different stakeholders</li><li>Organised and methodical</li><li>Flexible to Travel</li></ul><p>The organisation is looking for a candidate with the ability to adapt to different working environments and react to change within a fast paced environment.</p> <p><b>Additional Details</b> </p> <ul><li>CIPD Qualified</li><li>Excellent working knowledge of Microsoft Office and ability to learn new system approaches</li></ul><p> </p><p><b>How To Apply</b> </p> <p>Please quote job reference <b>IRC127509 </b>in all correspondence relating to this position.</p><p>If you would like to find out more about the role, call<b> Lauren Allen  </b>on<b> 01224  778144 </b></p><p><b></b></p><b></b><p><b></b></p><p><b></b></p><b></b><p>Online applications preferred however applications via e-mail will be accepted to<b> lauren.allen@alta-blue.com</b></p><p><b></b></p><b></b><p> </p>',
      'referencenumber' => 'IRC127509',
      'postalcode' => 'G2 4GZ',
      'company' => 'Altablue',
      'discipline' => 'Training &amp; Competence',
      'country' => 'GB',
      'location' => '55.864767279868,-4.2659738888787',
    ),
    'attributes' => 
    array (
    ),
  ),
  50 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Business Development Manager',
      'date' => '20150818',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p>We are delighted to be supporting one of our clients with a requirement for a Business Development Manager. Our client is a forward thinking muilti-utility connections company who provide Utilities services across a range of sectors in the UK.  This company is growing fast and has plans to double their workforce and significantly increase their market share over the next 4 years. Due to this expansion they need a well-connected Business development manager to support with their growth plans in the Midlands. This position will be based from home with regular travel across the region. The role will focus on developing new business to agreed objectives.</p><p> </p><p><b>Detailed Description</b> </p> <p>Key tasks undertaken as part of this BDM role:</p><p> </p><ul><li>Proactively develop new business accounts to agree targets.</li><li>Plan, organise and prioritise sales activities to achieve agreed targets from both new and existing housing developer accounts.</li><li>Successfully manage a portfolio of key accounts and ensure high levels of repeat business and customer satisfaction.</li><li>Manage and interpret customer needs by gaining a good understanding of the customers’ development plans and by developing an excellent working relationship.</li><li>Use creativity and initiative to both manage accounts and develop new business opportunities.</li><li>Adopt a flexible, team working approach to build rapport quickly, develop excellent relations with customers and colleagues, and ensure total commitment to exceeding customer needs.</li><li>Work closely with the Design and administration team to ensure quotations are issued to agreed timescales.</li><li>Maintain and develop detailed knowledge of the service proposition’s key features and unique benefits.</li><li>Maintain a high awareness of competitor propositions and regional activity.</li><li>Input to marketing/promotional activity and supporting marketing events where required.</li><li>Use good analytical and negotiation skills to tackle any issues in a positive, proactive and practical way.</li><li>Deliver sales presentations to both internal and external audiences as required.</li><li>Provide monthly sales reports to deadlines including trends, sales performance to target, competitor intelligence etc.</li><li>Be alert to identify key business opportunities for Energetics across both sectors.</li></ul><p><b>Job Requirements</b> </p> <p>Essential qualifications, skills and experience:</p><ul><li>At least five years successful sales experience in a similar B2B role ideally within the utility connections, construction or a similar sector.</li><li>Articulate, persuasive and driven to achieve.</li><li> Well organised with excellent attention to detail.</li></ul> <p><b>Additional Details</b> </p> <ul><li>  Educated to A Level/HNC level or equivalent.</li></ul><p><b>How To Apply</b> </p> <p>Please quote job reference <b>IRC127457 </b>in all correspondence relating to this position.</p><p> </p>If you would like to find out more about the role, call<b> Emily Earley  </b>on<b> 01224 763662. </b><p><b></b></p><b></b><p>Online applications preferred however applications via e-mail will be accepted to<b> emilyearley@alta-blue.com</b></p><p><b></b></p><b></b><p> </p>',
      'referencenumber' => 'IRC127515',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Business Development',
      'country' => 'GB',
      'location' => '52.4813679,-1.8980726',
    ),
    'attributes' => 
    array (
    ),
  ),
  51 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Business Development Manager',
      'date' => '20150818',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p>We are delighted to be supporting one of our clients with a requirement for a Business Development Manager. Our client is a forward thinking muilti-utility connections company who provide Utilities services across a range of sectors in the UK.  This company is growing fast and has plans to double their workforce and significantly increase their market share over the next 4 years. Due to this expansion they need a well-connected Business development manager to support with their growth plans in the southern region with a main base at their Bristol office. This position will be based from home with regular travel across the region. The role will focus on developing new business to agreed objectives.</p><p> </p><p><b>Detailed Description</b> </p> <p>Key tasks undertaken as part of this BDM role:</p><p> </p><ul><li>Proactively develop new business accounts to agree targets.</li><li>Plan, organise and prioritise sales activities to achieve agreed targets from both new and existing housing developer accounts.</li><li>Successfully manage a portfolio of key accounts and ensure high levels of repeat business and customer satisfaction.</li><li>Manage and interpret customer needs by gaining a good understanding of the customers’ development plans and by developing an excellent working relationship.</li><li>Use creativity and initiative to both manage accounts and develop new business opportunities.</li><li>Adopt a flexible, team working approach to build rapport quickly, develop excellent relations with customers and colleagues, and ensure total commitment to exceeding customer needs.</li><li>Work closely with the Design and administration team to ensure quotations are issued to agreed timescales.</li><li>Maintain and develop detailed knowledge of the service proposition’s key features and unique benefits.</li><li>Maintain a high awareness of competitor propositions and regional activity.</li><li>Input to marketing/promotional activity and supporting marketing events where required.</li><li>Use good analytical and negotiation skills to tackle any issues in a positive, proactive and practical way.</li><li>Deliver sales presentations to both internal and external audiences as required.</li><li>Provide monthly sales reports to deadlines including trends, sales performance to target, competitor intelligence etc.</li><li>Be alert to identify key business opportunities for Energetics across both sectors.</li></ul><p><b>Job Requirements</b> </p> <p>Essential qualifications, skills and experience:</p><ul><li>At least five years successful sales experience in a similar B2B role ideally within the utility connections, construction or a similar sector.</li><li>Articulate, persuasive and driven to achieve.</li><li> Well organised with excellent attention to detail.</li></ul> <p><b>Additional Details</b> </p> <ul><li>  Educated to A Level/HNC level or equivalent.</li></ul><p><b>How To Apply</b> </p> <p>Please quote job reference <b>IRC127457 </b>in all correspondence relating to this position.</p><p> </p>If you would like to find out more about the role, call<b> Emily Earley  </b>on<b> 01224 763662. </b><p><b></b></p><b></b><p>Online applications preferred however applications via e-mail will be accepted to<b> emilyearley@alta-blue.com</b></p><p><b></b></p><b></b><p> </p>',
      'referencenumber' => 'IRC127516',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Business Development',
      'country' => 'GB',
      'location' => '51.453493,-2.5973937',
    ),
    'attributes' => 
    array (
    ),
  ),
  52 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Planning and Support Manager',
      'date' => '20150818',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>Altablue is a people consultancy, delivering resourcing services for our clients with a focus on relationships and resourcing excellence. We are head quartered in Aberdeen, UK, with client operations globally enabling us to place candidates all over the world in either a staff or contract capacity. </p><p>We are delighted to be supporting one of our clients with a requirement for a Planning &amp; Support Manager based in their Slough Office. Our client is a forward thinking multi-utility connections company who provide Utilities services across a range of sectors in the UK.  This company is growing fast and has plans to double their workforce and significantly increase their market share over the next 4 years. Due to this expansion they need this critical position to be able to ensure the Project Management and Operational departments have the required support to allow them to deliver multi utility construction projects within the Slough area. The support function includes a variety of disciplines covering all aspects of Planning, scheduling, process implementation and administrative duties throughout the full cycle of the project. This will be an ideal positon for someone with exceptional customer care principles who has the experience in managing teams focused on delivery within a busy project environment.  </p><p> </p><p> </p><p><b>Detailed Description</b> </p> <p>Key tasks undertaken as part of this Planning &amp; Support Manager role:</p><ul><li>Day to Day Management of a multi skilled Operational Support Team including Administrators, Schedulers, Co-ordinators and CAD Operators</li><li>Provide the essential support to the Project Management and Operations department to allow them to deliver projects safely, on time and within budget</li><li>Support the Regional Manager in the preparation of company budgets, strategic plans and contribute to the development, communication and ultimate delivery of these plans within your area of responsibility</li><li>Mentor and motivate your direct reports conducting periodic performance evaluations to ensure they are achieving individual targets and contributing to the overall regional and company performance</li><li>Address any customer concern in a timely and effective manner and also ensure that these high standards are maintained across your entire team</li><li>Coordinate all support functions and ensure the team are undertaking their roles in accordance with Energetics requirements, whilst managing Reports workloads and working with the Senior Project Manager and Operations Manager to forecast resource requirements</li><li>Manage any project issues and support Reports in the resolution of challenges within their roles</li><li>Responsible for the implementation of best practice processes and procedures and driving continual improvement within the team</li><li>Monthly &amp; Quarterly Reporting inclusive of planning lead times, forecast work bank, non-conformances, missed appointments, performance and escalations</li><li>Ensure all asset adopting companies are notified of proposed works through detailed and accurate whereabouts</li><li>Manage and control all road opening notices to ensure compliance with NRSWA and section 74</li><li>Develop good relationships with the customer and the customer’s representatives including architects, consultants and site managers</li><li>Ensure that safe working practices are maintained at all times</li><li>Responsible for and take lead role for health, safety and work quality</li><li>Use improved working techniques to increase efficiency</li></ul><p><b>Job Requirements</b> </p> <p>Ideal qualifications, skills and experience:</p><ul><li>Proven experience of operating within in a multi-utility, or similar industry and managing a support team including Administrators, Schedulers, Co-ordinators and CAD Operators</li><li>A good understanding of business and budget control with excellent organisational planning, time management skills and communication</li><li>Logical thinking with creative problem solving ability and confidence to control various factions simultaneous</li></ul><p> </p><p><b>Additional Details</b> </p> Knowledge of Computer Aided Design (CAD)Understanding of NRSWA noticing including permit schemes (desirable)<p><b>How To Apply</b> </p> <p>Please quote job reference <b>IRC127459 </b>in all correspondence relating to this position.</p><p>If you would like to find out more about the role, call<b> Emily Earley  </b>on<b>  Tel: 01224 763662 </b></p><p><b></b></p><b></b><p>Online applications preferred however applications via e-mail will be accepted to<b> Emily.earley@alta-blue.com</b></p><p><b></b></p><b></b><p> </p>',
      'referencenumber' => 'IRC127517',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Planning',
      'country' => 'GB',
      'location' => '51.5111014,-0.5940682',
    ),
    'attributes' => 
    array (
    ),
  ),
  53 => 
  array (
    'name' => '{}job',
    'value' => 
    array (
      'title' => 'Business Development Manager',
      'date' => '20150821',
      'description' => '<p><b>Brief Posting Description</b> </p> <p>We are currently recruiting for a large UK based Environmental Consultancy. They have 13 sites based throughout the country and are looking to grow over the next few months. Our client specialises in providing bespoke Asbestos solutions for their customers. They cover surveying and analytical lab based work and have their own lab for these services.</p><p><b>Detailed Description</b> </p> <p>The Business Development Manager role is a staff role based in Manchester and would involve national travel to promote the company. </p>Our client is looking for candidates whom are currently within a proven sales position for a similar company. Candidates should have extensive knowledge of the waste management market with a specialised focus on RDF / SRF and also have knowledge of odour &amp; anaerobic digestion. The successful candidate will have good commercial acumen and be provided with a company car, mobile phone and laptop. There is a base salary on offer along with a commission scheme which would be discussed at interview stage.<br /><p><b>Additional Details</b> </p> <p><b>How To Apply</b> </p> <p>Please quote job reference IRC127601 and job title in all correspondence relating to this position.</p><p>Internal applicants must discuss their application with their line manager prior to applying.</p><p>Online applications and email applications to rosy.thomson@alta-blue.com </p>',
      'referencenumber' => 'IRC127601',
      'postalcode' => '',
      'company' => 'Altablue',
      'discipline' => 'Business Development',
      'country' => 'GB',
      'location' => '53.4791466,-2.2447445',
    ),
    'attributes' => 
    array (
    ),
  ),
);