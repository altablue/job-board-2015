<?php $disc= array (
  0 => 
  array (
    'name' => 'Infrastructure',
    'value' => 'infrastructure',
  ),
  1 => 
  array (
    'name' => 'Utilities',
    'value' => 'utilities',
  ),
  2 => 
  array (
    'name' => 'Life Sciences',
    'value' => 'lifesciences',
  ),
  3 => 
  array (
    'name' => 'Logistics',
    'value' => 'logistics',
  ),
  4 => 
  array (
    'name' => 'Construction',
    'value' => 'construction',
  ),
  5 => 
  array (
    'name' => 'Operations',
    'value' => 'operations',
  ),
  6 => 
  array (
    'name' => 'Civil/Structural',
    'value' => 'civil/structural',
  ),
  7 => 
  array (
    'name' => 'Electrical',
    'value' => 'electrical',
  ),
  8 => 
  array (
    'name' => 'Project',
    'value' => 'project',
  ),
  9 => 
  array (
    'name' => 'Designer',
    'value' => 'designer',
  ),
  10 => 
  array (
    'name' => 'Business Development',
    'value' => 'businessdevelopment',
  ),
  11 => 
  array (
    'name' => 'Commercial',
    'value' => 'commercial',
  ),
  12 => 
  array (
    'name' => 'Planning',
    'value' => 'planning',
  ),
  13 => 
  array (
    'name' => 'Administration',
    'value' => 'administration',
  ),
  14 => 
  array (
    'name' => 'Training & Competence',
    'value' => 'training&competence',
  ),
);