<?php

class Pages extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $pagetitle;

    /**
     *
     * @var string
     */
    public $temp;

    /**
     *
     * @var enum
     */
    public $type;

    /**
     *
     * @var string
     */
    public $url;

    /**
     *
     * @var integer
     */
    public $parent;

}
