<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Validator\InclusionIn;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class Oauthclients extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $client_id;

    /**
     *
     * @var string
     */
    public $client_secret;

    /**
     *
     * @var string
     */
    public $redirect_uri;

    /**
     *
     * @var integer
     */
    public $grant_types;

    public $scope;

    public $user_id;

    public function initialize()
    {
        $this->setSource("oauth_clients");
    }


}
