<?php

class Modules extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $type;

    /**
     *
     * @var int
     */
    public $pageid;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $data;


}
