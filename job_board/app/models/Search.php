<?php

class Search extends \Phalcon\Mvc\Model
{

    public $client;

    public $esParams;

    public function onConstruct()
    {

    }

    public function initialize()
    {
    }

    public function searchExecute($data, $sortBy=NULL, $term=NULL)
    {

        $this->client = new Elasticsearch\Client();
        $this->esParams['index'] = 'abjb';
        $this->esParams['type'] = 'jobs';

        $statement = array();

        //if(empty($data['from'])){ $statement['from'] = 0; }else{ $statement['from'] = $data['from']; unset($data['from']); }
        //if(empty($data['limit'])){ $statement['size'] = 10; }else{ $statement['size'] = $data['limit']; unset($data['limit']); }


        if(!empty($data['s'])){
            $statement['size'] = $data['s'];
            unset($data['s']);
            if(!empty($data['f'])){
                $statement['from'] = ($data['f']-1)*$statement['size'];
                unset($data['f']);
            }else{
                $statement['from'] = 0;
            }
        }else{
            $statement['size'] = 10;
        }

        unset($data['_url']);
        unset($data['access_token']);

        if(!empty($data['sb'])){
            $sortBy = $data['sb'];
            $statement['track_scores'] = 'true';
            switch($sortBy){
                case 'new':
                    $statement['sort'][]['date'] = 'desc';
                    break;
                case 'old':
                    $statement['sort'][]['date'] = 'asc';
                    break;
            }
            unset($data['sb']);
        }else{
            $sortBy = false;
        }

        $loc = $filter = $wildcard = array();

        $miles = 10;
        $pageBase = 10;
        $km = intval($miles)/0.621371192;
        if(!empty($extra['radius']) && is_array($extra['radius'])):
            $rad = array();
            $rad['from'] = intval($extra['radius']['from'])/0.621371192;
            $rad['to'] = intval($extra['radius']['to'])/0.621371192;
        endif;

        if(!empty($data['exclude'])){
            $statement['query']['bool']['must_not'][]['match']['_id'] = $data['exclude'];
        }

        foreach($data as $term => $query):
            if($term == 'keyword'){
                $statement['query']['bool']['should'][]['wildcard']['_all'] = strtolower($query).'*';
            }else if(!empty($query)){
                switch($term){ 
                    case 'location':
                        if($query == 'bounding_box'):
                            $latlons = explode(',',$data['geo']);
                            $loc['boundBox'] = array(
                                'left' => $latlons[0],
                                'bottom' => $latlons[1],
                                'right' => $latlons[2],
                                'top' => $latlons[3]
                            );
                            if(!empty($filter['filter']) && is_array($filter['filter'])){
                                $filter['filter']['and'][]['geo_bounding_box']['location'] = array(
                                    'top_left' => array('lat'=>floatval($loc['boundBox']['top']),'lon'=>floatval($loc['boundBox']['left'])),
                                    'bottom_right' => array('lat'=>floatval($loc['boundBox']['bottom']),'lon'=>floatval($loc['boundBox']['right']))
                                );
                            }else{
                                $filter['filter']['geo_bounding_box']['location']['top_left']['lat'] = floatval($loc['boundBox']['top']);
                                $filter['filter']['geo_bounding_box']['location']['top_left']['lon'] = floatval($loc['boundBox']['left']);
                                $filter['filter']['geo_bounding_box']['location']['bottom_right']['lat'] = floatval($loc['boundBox']['bottom']);
                                $filter['filter']['geo_bounding_box']['location']['bottom_right']['lon'] = floatval($loc['boundBox']['right']);                
                            }
                        else:
                            /*$latlons = explode(',',$data['geo']);
                            $loc['geo'] = array(
                                'left' => $latlons[0],
                                'bottom' => $latlons[1]
                            );*/
                            if(!empty($filter['filter']) && is_array($filter['filter'])){
                                $filter['filter']['and'][]['geo_distance'] = array('distance' => $km.'km', 'location' => $loc['geo']);
                            }else{
                                $filter['filter']['geo_distance']['distance'] = $km.'km';
                                $filter['filter']['geo_distance']['location'] = $data['geo'];
                            }
                        endif;
                        /*elseif(!empty($loc['address']) && !empty($rad) && is_array($rad)):
                            if(!empty($filter['filter']) && is_array($filter['filter'])){
                                $filter['filter']['and'][]['geo_distance_range'] = array('from'=>$rad['from'].'km','to'=>$rad['to'].'km','location'=>$loc['address']);
                            }else{
                                $filter['filter']['geo_distance_range']['from'] = $rad['from'].'km';
                                $filter['filter']['geo_distance_range']['to'] = $rad['to'].'km';
                                $filter['filter']['geo_distance_range']['location'] = $loc['address'];             
                            }
                        endif;*/
                        $wildcard['query']['match_all'] = array();
                        //$filter['geo_distance'] = array('distance' => '10km', 'location' => urldecode($data['location']));
                        break;
                    /*case 'category':
                        $statement['query']['bool']['should'][]['match']['category'] = strtolower($query);
                        $statement['query']['bool']['should'][]['wildcard']['category'] = strtolower($query).'*';
                        break;*/
                    case 'company':
                        $statement['query']['bool']['should'][]['match']['company'] = strtolower($query);
                        $statement['query']['bool']['should'][]['wildcard']['company'] = strtolower($query).'*';
                        break;
                    case 'disciplines':
                        $statement['query']['bool']['should'][]['match']['discipline'] = strtolower($query);
                        $statement['query']['bool']['should'][]['wildcard']['discipline'] = strtolower($query).'*';
                        break;
                    case 'country':
                        $statement['query']['bool']['should'][]['match']['country'] = strtolower($query);
                        $statement['query']['bool']['should'][]['wildcard']['country'] = strtolower($query).'*';
                        break;
                }
            }
        endforeach;

        if($sortBy){
            $statement['track_scores'] = 'true';
            switch($sortBy){
                case 'Alphabetical - A-Z':
                    $statement['sort'][]['title'] = 'asc';
                    break;
                case 'Alphabetical - Z-A':
                    $statement['sort'][]['title'] = 'desc';
                    break;
                case 'Location - Nearest':
                    $statement['sort'][]['_geo_distance'] = array('latlng' => $loc['address'],'order' => 'asc', 'unit' => 'km');
                    break;
                case 'Location - Furthest':
                    $statement['sort'][]['_geo_distance'] = array('latlng' => $loc['address'],'order' => 'desc', 'unit' => 'km');
                    break;
            }
        }

        if(empty($statement['query']) && !empty($wildcard['query'])){
            $statement['query'] = $wildcard['query'];
        }

        if(!empty($filter)) $statement['filter'] = $filter['filter'];

        /*print_r(json_encode($statement));
        die();
        return true;*/

        $this->esParams['body'] = $statement;
        $results = $this->client->search($this->esParams);
        $results['searchTerms'] = json_encode($statement);
        return $results;        
    }

}
