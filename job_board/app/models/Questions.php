<?php

class Questions extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $condition;

    /**
     *
     * @var int
     */
    public $question;

    /**
     *
     * @var enum
     */
    public $answers;

    /**
     *
     * @var string
     */
    public $conditional;

}
