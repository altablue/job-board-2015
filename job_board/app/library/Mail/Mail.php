<?php
namespace AB\Mail;

use Phalcon\Mvc\User\Component;
use Swift_Message as Message;
use Swift_Mailer as Mailer;
use Swift_SmtpTransport;
use Swift_Attachment;

/**
 *
 * Sends e-mails based on pre-defined templates
 */
class Mail extends Component
{

	protected $_transport;

	/**
	 * Applies a template to be used in the e-mail
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function getTemplate($name, $params)
	{

		$parameters = array_merge(array(
			'site_url' => 'https://jobs.alta-blue.com',
		), $params);

        $view = $this->getDI()->get('\Phalcon\Mvc\View\Simple');
        $view->setViewsDir(APP_PATH.'app/views/emailTemplates/');
        $view->registerEngines(array(
            ".volt" => 'Phalcon\Mvc\View\Engine\Volt'
        ));

        foreach($params['data'] as $key => $param){
            $view->{$key} = $param;
        }

        $view->site_url = 'https://jobs.alta-blue.com';

        $content = $view->render($name.'.volt');

        return $content;

	}

	/**
	 * Sends e-mails via gmail based on predefined templates
	 *
	 * @param array $to
	 * @param string $subject
	 * @param string $name
	 * @param array $params
	 */
	public function send($to, $subject, $params)
	{

        if(!empty($params['template'])){
            $template = $this->getTemplate($params['template'], $params);
            $plain = $this->getTemplate($params['template'].'-plain', $params);
        }else{
            $template = 'see attached';
            $plain = 'see attached';
        }

        $transport = Swift_SmtpTransport::newInstance('smtp.mailgun.org', 465, "ssl")
          ->setUsername('postmaster@jobs.alta-blue.com')
          ->setPassword('5cb1642a56753dfbd93af6febe5755fb')
          ;

        $message = Message::newInstance()
            ->setSubject($subject)
            ->setTo($to)
            ->setFrom(array(
                'noreply@jobs.alta-blue.com' => 'Altablue Job Board System'
            ))
            ->setBody($plain, 'text/plain')
            ->addPart($template, 'text/html');

        if(!empty($params['attach']) && is_array($params['attach'])){
            $message->attach(
                Swift_Attachment::fromPath($params['attach']['path'])->setFilename($params['attach']['attachName'])
            );
        }

        $mailer = Mailer::newInstance($transport);

        $sender = $mailer->send($message);

    }
}
