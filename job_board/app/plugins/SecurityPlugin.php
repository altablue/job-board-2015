<?php

use Phalcon\Acl;
use Phalcon\Acl\Role;
use Phalcon\Acl\Resource;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Acl\Adapter\Memory as AclList;

/**
 * SecurityPlugin
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 */
class SecurityPlugin extends Plugin
{

	/**
	 * Returns an existing or new access control list
	 *
	 * @returns AclList
	 */
	public function getAcl()
	{

		//throw new \Exception("something");

		//if (!isset($this->persistent->acl)) {

		//if (!is_file(APP_PATH ."app/security/acl.data")) {

			$acl = new AclList();

			$acl->setDefaultAction(Acl::DENY);

			//Register roles
			$roles = array(
				'users'  => new Role('Users'),
				'candidates' => new Role('Candidates'),
				'userAdmins' => new Role('userAdmins'),
				'ABstaff' => new Role('ABstaff'),
				'superAdmins' => new Role('superAdmins'),
				'guests' => new Role('Guests')
			);

			foreach ($roles as $role) {
				$acl->addRole($role);
			}

			//Private area resources
			$privateResources = array(
				'users'	    => array('index', 'create'),
				'api'   	 => array('index', 'updateUser','createUser','authUser', 'checkAuth', 'createCandidate', 'search', 'createJob', 'helpFillEmail', 'contactRequest', 'jobApply', 'candReminder','addCandNote'),
				'jobs'		=> array('index', 'get', 'create', 'update', 'getJobCandidates'),
				'candidates' => array('index', 'get', 'create'),
				'board'		=> array('index', 'candidates', 'jobs', 'users', 'uncomplete'),
				'register'   => array('index','addUsers'),
				'signup'	 => array('index','registerUser'),
				'index'      => array('index'),
				'search'	=> array('index', 'results')
			);

			foreach ($privateResources as $resource => $actions) {
				$acl->addResource(new Resource($resource), $actions);
			}

			$privateBAdminsResources = array(
				'users'	   => array('index')
			);

			foreach ($privateBAdminsResources as $resource => $actions) {
				$acl->addResource(new Resource($resource), $actions);
			}

			//Private area resources
			$candidatesResources = array(
				'users'	    => array('index', 'create'),
				'api'   	 => array('index', 'updateUser','authUser', 'checkAuth', 'updateCandidate', 'search', 'updateJobStatus', 'helpFillEmail', 'contactRequest', 'jobApply', 'candReminder','addCandNote'),
				'candidates' => array('index', 'create', 'update', 'createProfile', 'get', 'getJobs'),
				'jobs'		=> array('index', 'get'),
				'index'      => array('index'),
				'search'	=> array('index', 'results')
			);

			foreach ($candidatesResources as $resource => $actions) {
				$acl->addResource(new Resource($resource), $actions);
			}

			$privateAbStaffResources = array(
				'users'	    => array('index', 'create'),
				'api'   	 => array('index', 'createCompany','updateUser','createUser', 'checkAuth','search', 'updateJobStatus', 'updateCandStatus', 'updateFillStatus', 'helpFillEmail', 'contactRequest', 'jobApply', 'candReminder','addCandNote'),
				'jobs'		=> array('index', 'get', 'create','update', 'getJobCandidates'),
				'candidates' => array('index', 'create', 'update','get', 'getJobs'),
				'board'		=> array('index', 'candidates', 'jobs', 'users', 'uncomplete'),
				'register'   => array('index','addUsers'),
				'signup'	 => array('index','registerUser'),
				'index'      => array('index'),
				'recruiter'  => array('index', 'getCompanies', 'getCandidates'),
				'search'	=> array('index', 'results')
			);

			foreach ($privateAbStaffResources as $resource => $actions) {
				$acl->addResource(new Resource($resource), $actions);
			}

			$privateSupaResources = array(
				'users'	    => array('index', 'create'),
				'api'   	 => array('index', 'createCompany','updateUser','createUser','authUser', 'checkAuth','search', 'registerRecruiter', 'updateJobStatus', 'updateCandStatus', 'updateFillStatus', 'helpFillEmail', 'contactRequest', 'jobApply', 'candReminder','addCandNote'),
				'jobs'		=> array('index', 'get', 'update'),
				'candidates' => array('index', 'create', 'update', 'createProfile', 'get', 'getJobs'),
				'board'		=> array('index', 'candidates', 'jobs','users','uncomplete'),
				'register'   => array('index','addUsers','addRecruiter'),
				'signup'	 => array('index','registerUser'),
				'index'      => array('index'),
				'recruiter'  => array('index', 'getCompanies', 'getCandidates'),
				'search'	=> array('index', 'results')
			);

			foreach ($privateSupaResources as $resource => $actions) {
				$acl->addResource(new Resource($resource), $actions);
			}

			//Public area resources
			$publicResources = array(
				'index'      => array('index', 'terms', 'privacy', 'cookies', 'sitemap'),
				'api'   	 => array('authUser', 'checkAuth', 'updateUser', 'contactSubmitForm','addCandNote','resetPassword'),
				'login'		 => array('index', 'logout', 'passwordReset'),
				'user'	 	 => array('index'),
				'signup'	 => array('registerUser'),
				'errors'     => array('show404', 'show500', 'show401')
			);

			foreach ($publicResources as $resource => $actions) {
				$acl->addResource(new Resource($resource), $actions);
			}

			//Grant access to public areas to both users and guests
			foreach ($roles as $role) {
				foreach ($publicResources as $resource => $actions) {
					foreach ($actions as $action){
						$acl->allow($role->getName(), $resource, $action);
					}
				}
			}

			//Grant acess to users area to role Users
			foreach ($privateResources as $resource => $actions) {
				foreach ($actions as $action){
					$acl->allow('Users', $resource, $action);
					$acl->allow('userAdmins', $resource, $action);
					$acl->allow('superAdmins', $resource, $action);
					$acl->allow('ABstaff', $resource, $action);
				}
			}

			//Grant acess to candidate area to role Users
			foreach ($candidatesResources as $resource => $actions) {
				foreach ($actions as $action){
					$acl->allow('Candidates', $resource, $action);
					$acl->allow('userAdmins', $resource, $action);
					$acl->allow('superAdmins', $resource, $action);
					$acl->allow('ABstaff', $resource, $action);
				}
			}

			//Grant acess to private area to role Users
			foreach ($privateBAdminsResources as $resource => $actions) {
				foreach ($actions as $action){
					$acl->allow('userAdmins', $resource, $action);
					$acl->allow('superAdmins', $resource, $action);
				}
			}



			//Grant acess to private area to role Users
			foreach ($privateAbStaffResources as $resource => $actions) {
				foreach ($actions as $action){
					$acl->allow('ABstaff', $resource, $action);
					$acl->allow('superAdmins', $resource, $action);
				}
			}

			//Grant acess to private area to role Users
			foreach ($privateSupaResources as $resource => $actions) {
				foreach ($actions as $action){
					$acl->allow('superAdmins', $resource, $action);
				}
			}

			//The acl is stored in session, APC would be useful here too
			file_put_contents(APP_PATH ."app/security/acl.data", serialize($acl));
		//}else{
			// $acl = unserialize(file_get_contents(APP_PATH ."app/security/acl.data"));
		//}

		return $acl;
	}

	/**
	 * This action is executed before execute any action in the application
	 *
	 * @param Event $event
	 * @param Dispatcher $dispatcher
	 */
	public function beforeDispatch(Event $event, Dispatcher $dispatcher)
	{



		$auth = $this->session->get('auth');
		if (!$auth){
			$role = 'Guests';
		} else {
			if($auth['userLevel'] >= 99){
				if($auth['userLevel'] == 99){
					$role = 'ABstaff';
				}else{
					$role = 'superAdmins';
				}
			}else{
				if($auth['type'] >= 2){
					$role = 'Users';
				}else{
					$role = 'Candidates';
				}
				
			}
		}


		$controller = $dispatcher->getControllerName();
		$action = $dispatcher->getActionName();

		$acl = $this->getAcl();

		/*echo $role;
		echo $controller;
		echo $action;
		die();*/

		$allowed = $acl->isAllowed($role, $controller, $action);
		if ($allowed != Acl::ALLOW) {
			$dispatcher->forward(array(
				'controller' => 'errors',
				'action'     => 'show401'
			));
			return false;
		}
	}
}
