{{ content() }}

<div ng-controller="AdminController" class="login pages">

    <div class="jumbotron">
        <div class="container">
            <p>Collaboratively administrate empowered markets via plug-and-play networks.</p>
        </div>
    </div>

    <form class="form-horizontal" method="post" action="#" ng-submit="return false;">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="checkbox">
                    <label>
                        <input type="checkbox"> Remember me
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary ladda-button" ladda="loginLoading" ng-click="login()" onClick="return false;" data-style="expand-left"><span class="ladda-label">Sign in</span></button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <a href="/admin/reset-password">Reset Password &rarr;</a>
            </div>
        </div>
    </form>

</div>