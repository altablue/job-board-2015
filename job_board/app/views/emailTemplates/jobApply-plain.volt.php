Altablue Job Board (https://jobs.alta-blue.com)
Application Submission
<?php echo $data->name; ?>, You have successfully applied for position <?php echo $data->irc; ?> on the Altablue Job Board(https://jobs.alta-blue.com). If you have any questions about your application, please contact us at info@alta-blue.com and please quote the job id number mentioned above when referencing the position you have applied for.
Following us on social media to get real time news of our latest jobs
Facebook - http://www.facebook.com/altablueglobal
Twitter - http://www.twitter.com/alta_blue
Linkedin - https://www.linkedin.com/company/altablue
Google+ - https://plus.google.com/116991851462340448399/posts
You have recieved this email as your email address was submitted during a job application on the Altablue Job Board(https://jobs.alta-blue.com). If you do not believe this to be correct, please contact info@alta-blue.com to log a complaint.