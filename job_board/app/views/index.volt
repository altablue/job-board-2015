<!DOCTYPE>
<!--[if lt IE 7 ]> <html class="ie6" id="root" lang="en"> <![endif]-->
<!--[if IE 7 ]> <html class="ie7" id="root" lang="en"> <![endif]-->
<!--[if IE 8 ]> <html class="ie8" id="root" lang="en"> <![endif]-->
<!--[if gt IE 8 ]> <html class="ie9" id="root" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html id="root" lang="en"> <!--<![endif]-->
    <head>
        <base href="https://jobs.alta-blue.com/">
        <meta content="local" />
        <meta name="fragment" content="!" />
        {{ stylesheet_link('css/main.min.css') }}
        {% include 'layouts/meta.volt' %}
        {{ assets.outputCss() }}
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700,700italic|Noto+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
        <link href='https://api.tiles.mapbox.com/mapbox.js/v2.1.9/mapbox.css' rel='stylesheet' />
        <!--[if lte IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <style>
                .ng-hide {
                    display: none !important;
                }
            </style>
            <script>
                //optional need to be loaded before angular-file-upload-shim(.min).js
                FileAPI = {
                    //only one of jsPath or jsUrl.
                    jsUrl: 'https://jobs.alta-blue.com/js/libs/ng-file-upload/FileAPI.min.js',

                    flashUrl: 'https://jobs.alta-blue.com/js/libs/ng-file-upload/FileAPI.flash.swf',

                    //forceLoad: true, html5: false //to debug flash in HTML5 browsers
                    //noContentTimeout: 10000 (see #528)
                }
            </script>
            <script src="https://jobs.alta-blue.com/js/libs/ng-file-upload/ng-file-upload-shim.js"></script>
            <script type="text/javascript" src="//platform.linkedin.com/in.js">
                api_key: 77aoj7smcv9zg2
                authorize: true
                onLoad: onLinkedInLoad
            </script>
        <![endif]-->
        <script>
        var $buoop = {vs:{i:9,f:25,o:12.1,s:7},c:2};
        function $buo_f(){
         var e = document.createElement("script");
         e.src = "//browser-update.org/update.min.js";
         document.body.appendChild(e);
        };
        try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
        catch(e){window.attachEvent("onload", $buo_f)}
        </script>
        <meta name="google-site-verification" content="lA9LJwp9rIQDwWMIVwGGLg6JDbnaUm-cyUgWg4XZ7bQ" />
    </head>
    <body id="ng-app" class="bootstrap">
        <div class="container" id="#" ng-controller="CoreController">

            {% include 'layouts/header.volt' %}

  		    <div ui-view autoscroll="true"><div id="loading" ng-show="searchLoading"><svg width="100" height="100" viewBox="0 0 38 38" xmlns="http://www.w3.org/2000/svg" stroke="#0099cc">
            <g fill="none" fill-rule="evenodd">
                <g transform="translate(1 1)" stroke-width="2">
                    <circle stroke-opacity=".5" cx="18" cy="18" r="18"/>
                    <path d="M36 18c0-9.94-8.06-18-18-18">
                        <animateTransform
                            attributeName="transform"
                            type="rotate"
                            from="0 18 18"
                            to="360 18 18"
                            dur="1s"
                            repeatCount="indefinite"/>
                    </path>
                </g>
            </g>
        </svg></div></div>

        </div>

        <footer class="footer">
            <div class="container">
                <p class="text-muted col-xs-6">Copyright &copy; <?php echo date('Y'); ?> Altablue LTD</p><nav class="text-muted col-xs-6"><a href="http://www.alta-blue.com/terms-of-use.html">Terms of Service</a>|<a href="http://www.alta-blue.com/privacy-policy.html">Privacy Policy</a>|<a href="http://www.alta-blue.com/cookies-policy.html">Cookie Policy</a></nav>
            </div>
        </footer>

        <!--[if lte IE 9]>
            <script src="/js/libs/ie8.polyfill.js"></script>
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28/angular.js"></script>
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28/angular-route.js"></script>
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28/angular-resource.js"></script>
        <![endif]-->

        {{ javascript_include('js/dist/libs.js') }}
        {{ javascript_include('js/dist/all.min.js') }}

        <script type="text/javascript" id="cookiebanner" data-moreinfo="http://www.alta-blue.com/cookies-policy.html" data-bg="#0099cc" src="/js/libs/jquery.cookieBanner.js"></script>
        <!--[if lt IE 9]><div id="buorg" class="buorg"><div><strong>WARNING:</strong>  This website requires a newer browser version: Your version of Internet Explorer Browser is <b>out of date</b>. Please <a target="_blank" href="//browser-update.org/update-browser.html#18:browser-update.org">Update your browser</a> and return to this website.<div id="buorgclose">x</div></div></div><![endif]-->

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-52411705-1', 'auto');
      ga('send', 'pageview');

    </script>

    </body>
</html>
