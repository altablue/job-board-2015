    <title>Altablue Job Board | [{[ title ]}]</title>

    <meta name="description" content="Altablue Job Board | [{[ desc ]}]">

    <meta itemprop="name" content="Altablue Job Board | [{[ title ]}]">
    <meta itemprop="description" content="Altablue Job Board | [{[ desc ]}]">
    <meta itemprop="image" content="http://jobs.alta-blue.com/imgs/site-prev.jpg">

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Altablue Job Board | [{[ title ]}]">
    <meta name="twitter:description" content="Altablue Job Board | [{[ desc ]}]">
    <meta name="twitter:image:src" content="http://jobs.alta-blue.com/imgs/site-prev.jpg">

    <meta property="og:title" content="Altablue Job Board | [{[ title ]}]" />
    <meta property="og:type" content="company" />
    <meta property="og:url" content="http://jobs.alta-blue.com" />
    <meta property="og:image" content="http://jobs.alta-blue.com/imgs/site-prev.jpg" />
    <meta property="og:description" content="Altablue Job Board | [{[ desc ]}]" />
    <meta property="og:site_name" content="Altablue Job Board" />

    <meta name="author" content="Altablue"><meta name='robots' content='index,follow'>    <meta name='copyright' content='Altablue'><meta name='language' content='EN'><meta name='coverage' content='Worldwide'><meta name='distribution' content='Global'><meta name='HandheldFriendly' content='True'><meta http-equiv='Expires' content='0'><meta http-equiv='Pragma' content='no-cache'><meta http-equiv='Cache-Control' content='no-cache'><meta http-equiv='imagetoolbar' content='no'><meta http-equiv='x-dns-prefetch-control' content='off'>