
		<nav class="navbar navbar-default navbar-inverse navbar-fixed-top">
		    <div class="container-fluid">
		        <div class="navbar-header">
		            <button ng-init="navCollapsed = true" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		                <span class="sr-only">Toggle navigation</span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		            </button>
		            <a class="navbar-brand col-sm-4 col-xs-6" href="/"><span><img src="/imgs/svgs-pngs/altablue-white.png"  /></span></a>
		        </div>


				<ul class="hidden-xs nav navbar-nav navbar-right">

					<li role="presentation" class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
						  Altablue Network<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
	                        <li ><a href="http://www.alta-blue.com" target="_blank">Website</a></li>
	                        <li ><a href="https://jobs.alta-blue.com" target="_blank">Job Board</a></li>
	                        <li ><a href="http://blog.alta-blue.com" target="_blank">Blog</a></li>
						</ul>
					</li>
		        </ul>

		        <div collapse="navCollapsed" class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

					<ul class="nav navbar-nav navbar-right  hidden-md hidden-sm hidden-lg hidden-xlg">
		                        <li ><a href="http://www.alta-blue.com" target="_blank">Website</a></li>
		                        <li ><a href="https://jobs.alta-blue.com" target="_blank">Job Board</a></li>
		                        <li ><a href="http://blog.alta-blue.com" target="_blank">Blog</a></li>

			        </ul>
		        </div>
		    </div>
		</nav>