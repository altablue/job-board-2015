<?php

use Phalcon\Mvc\Controller;
use OAuth2\Autoloader;
use Phalcon\Http\Response\Cookies;

class ControllerBase extends Controller
{
	public $siteUrl;

	protected function initialize()
    {

    	ini_set('display_errors',1);
    	error_reporting(E_ALL);

    	$this->siteUrl = $this->publicURL; 

    	$this->tag->setTitle('Altablue Job Board | ');
    	$this->view->setTemplateAfter('index');

		if (!$this->cookies->has('publicAccessToken')) {
        	$accessToken = json_decode($this->getPublicAccessToken());
        	$this->view->accessToken = $accessToken->token;
        	setcookie('publicAccessToken', $accessToken->token, time() + $accessToken->expires_in);
            setcookie('publicRefreshToken', $accessToken->refresh_token, time() + 1209600);
        }
    }

    protected function loadJS()
    {
        $this->assets->addJs("<script>alert('hello world');</script>");
    }

    public function getPublicAccessToken()
    {
        //echo session_id();
        //echo "curl -XGET https://localhost/api/1.0/token/gen/?response_type=token&client_id=gsQtGEr70bHTSn1HOY3nioWdEsBgb2vQqD3FJspxtv8&redirect_uri=https://jobs.alta-blue.com&state=123 -k";
        $returnCodeReq = exec("curl -XGET 'https://localhost/api/1.0/token/auth/?response_type=code&client_id=k6iIxfjCZ462AWJuxi9eXDx6ocmAfnB2Lh2iUCts9q49m46Q0O&redirect_uri=jobs.alta-blue.com&state=123&scope=read' -k");
        $returnCodeReq = json_decode($returnCodeReq);

        preg_match_all('/code=([^&]+)/', $returnCodeReq->token, $code);
        //print_r($returnCodeReq);

        //echo "curl -XPOST -u k6iIxfjCZ462AWJuxi9eXDx6ocmAfnB2Lh2iUCts9q49m46Q0O:LAGHikK3rmHGBzoEvmcKamPIgu8p3X79D5yfauq01cZBmRzJmw 'https://localhost/api/1.0/token/gen/' -d 'grant_type=authorization_code&client_id=k6iIxfjCZ462AWJuxi9eXDx6ocmAfnB2Lh2iUCts9q49m46Q0O&redirect_uri=jobs.alta-blue.com&".$code[0][0]."' -k";
        //die();

        //$returnTokenReq = exec("curl -XPOST -u k6iIxfjCZ462AWJuxi9eXDx6ocmAfnB2Lh2iUCts9q49m46Q0O:LAGHikK3rmHGBzoEvmcKamPIgu8p3X79D5yfauq01cZBmRzJmw 'https://localhost/api/1.0/token/gen/' -d 'grant_type=authorization_code&client_id=k6iIxfjCZ462AWJuxi9eXDx6ocmAfnB2Lh2iUCts9q49m46Q0O&redirect_uri=jobs.alta-blue.com&".$code[0][0]."' -k");
        $returnTokenReq = exec("curl -XPOST 'https://localhost/api/1.0/token/gen/' -d 'grant_type=authorization_code&client_id=k6iIxfjCZ462AWJuxi9eXDx6ocmAfnB2Lh2iUCts9q49m46Q0O&redirect_uri=jobs.alta-blue.com&".$code[0][0]."' -k");

        $returnTokenReq = json_decode($returnTokenReq);


        //$returnTokenReq = json_decode(exec("curl -XPOST -u k6iIxfjCZ462AWJuxi9eXDx6ocmAfnB2Lh2iUCts9q49m46Q0O:u4ZgaR6bEVJqcqQQ6syyGaE2haCSrHIw7wpPBBEdg8o9B62U6x https://localhost/api/1.0/token/gen -d 'grant_type=client_credentials' -k"));
	    return json_encode(array('token' => $returnTokenReq->access_token, 'expires_in' => $returnTokenReq->expires_in, 'refresh_token' => $returnTokenReq->refresh_token));
    }

}
