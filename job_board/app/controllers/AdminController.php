<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class AdminController extends ControllerBase
{

    private $submitData;

    public function initialize()
    {        
    	parent::initialize();
    }
    
    public function indexAction()
    {
    	$this->tag->appendTitle(' Altablue Job Board Admin login');
        $this->persistent->parameters = null;
    }

    public function resetPageAction()
    {
    	$this->tag->appendTitle(' Reset your password');
        $this->persistent->parameters = null;
    }

    public function resetRequestAction()
    {

    }

    public function holding()
    {
        
    }

}
