<?php

use Phalcon\Mvc\Controller;
use Phalcon\Filter;
use Phalcon\Http\Request;
use Phalcon\DI\InjectionAwareInterface;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use OAuth2\Autoloader;
use mikehaertl\wkhtmlto\Pdf;

class ApiController extends TokenController
{

    public $submitData;

    public $siteUrl;

    public $htmlPConfig;

    public $htmlPurifier;

    public $cache;

    public $params;

    public $access_token;

    private $tempPath;

    public function initialize()
    {

        parent::initialize();
        switch ($_SERVER['REQUEST_METHOD']) {
            case "GET":
                $this->getRequestSetup();
                break;
            case "POST":
                $this->postRequestSetup();
                break;
            case "PUT":
                $this->putRequestSetup();
                break;
            case 'DELETE':
                $this->deleteRequestSetup();
                break;
        }

        $this->tempPath = APP_PATH . 'public/templates/';

        $this->params = $this->dispatcher->getParams();

        /*if(empty($this->submitData->render) || !isset($this->submitData->render)){
            $this->view->disable();
        }*/

        /*$ultraFastFrontend = new DataFrontend(array(
            "lifetime" => 3600
        ));

        $fastFrontend = new DataFrontend(array(
            "lifetime" => 14400
        ));

        $slowFrontend = new DataFrontend(array(
            "lifetime" => 86400
        ));

        $this->cache = new Multiple(array(
            new ApcCache($ultraFastFrontend, array(
                "prefix" => 'cache',
            )),
            new MemcacheCache($fastFrontend, array(
                "prefix" => 'cache'
            )),
            new FileCache($slowFrontend, array(
                "prefix" => 'cache',
                "cacheDir" => "/tmp/"
            ))
        ));*/
    }

    private function getRequestSetup()
    {
        if(!empty($_SERVER['X_ACCESS_TOKEN'])){
            $this->access_token = $this->filter->sanitize($_SERVER['X_ACCESS_TOKEN'], "string");
        }
        $this->getParams = $_GET;
    }

    private function deleteRequestSetup()
    {
        if(!empty($_SERVER['X_ACCESS_TOKEN'])){
            $this->access_token = $this->filter->sanitize($_SERVER['X_ACCESS_TOKEN'], "string");
        }
        $this->getParams = $this->filter->sanitize($_GET, "alphanum");
    }

    private function postRequestSetup()
    {
        $request = new Phalcon\Http\Request();

        $this->submitData = json_decode($request->getRawBody(), true);

        if(!empty($_SERVER['X_ACCESS_TOKEN'])){
            $this->submitData['access_token'] = $this->filter->sanitize($_SERVER['X_ACCESS_TOKEN'], "string");
        }

        $this->view->disable();
    }

    private function putRequestSetup()
    {
        $this->submitData = json_decode(file_get_contents("php://input"), true);
    }

    /**
     * Action to register a new user
     */
    public function indexAction()
    {

    }

    public function createCampAction()
    {
        $data = array('access_token' => $this->access_token);
        $response = json_decode($this->apiPreCheckAction($data, 'write'));
        if(!empty($response->status) && $response->status == 'success'){
            $pages = new Pages;
            $values = array(
                        'pagetitle' => $this->submitData['name']['$modelValue'],
                        'url' => $this->submitData['url']['$modelValue'],
                        'parent' => 0,
                        'temp' => $this->submitData['temp']['$modelValue']['value'],
                        'type' => $this->submitData['type']['$modelValue']['value']
                    );
            $campCreate = $pages->create($values);
            if($campCreate == false){
                foreach ($pages->getMessages() as $message) {
                    echo json_encode(array('status'=>'error', 'message' => $message));
                }
            }else{
                $recordId = $pages->id;
                echo json_encode(array('status'=>'success', 'campaignId' => $recordId));
            }
        }else{
            echo json_encode(array('status'=>'error', 'message' => $response->message));
        }
    }

    public function editCampAction()
    {
        $data = array('access_token' => $this->access_token);
        $response = json_decode($this->apiPreCheckAction($data, 'write'));
        if(!empty($response->status) && $response->status == 'success'){
            $pages = new Pages;
            $values = array(
                        'id' => $this->submitData['id'],
                        'pagetitle' => $this->submitData['pagetitle'],
                        'url' => $this->submitData['url'],
                        'parent' => 0,
                        'temp' => $this->submitData['temp']['value'],
                        'type' => $this->submitData['type']['value']
                    );
            $campCreate = $pages->update($values);
            if($campCreate == false){
                foreach ($pages->getMessages() as $message) {
                    echo json_encode(array('status'=>'error', 'message' => $message));
                }
            }else{
                $recordId = $pages->id;
                echo json_encode(array('status'=>'success', 'campaignId' => $recordId));
            }
        }else{
            echo json_encode(array('status'=>'error', 'message' => $response->message));
        }
    }

    public function listCampAction()
    {
        $data = array('access_token' => $this->access_token);
        $response = json_decode($this->apiPreCheckAction($data,'write'));
        if(!empty($response->status) && $response->status == 'success'){
            $pages = new Pages;
            $campList = $pages->find();
            if($campList != false){
                $camps = array();
                foreach($campList as $camp){
                    $camps[] =  array(
                        'id' => $camp->id,
                        'pagetitle' => $camp->pagetitle,
                        'temp' => $camp->temp,
                        'url' => $camp->url,
                        'parent' => $camp->parent,
                        'type' => $camp->type
                    );
                }
                echo json_encode(array('status' => 'success', 'campaigns' => $camps));
            }
        }else{
            print_r($response);
            die();
            echo json_encode(array('status' => 'error', 'message' => $response->message));
        }
    }

    public function getCampAction($search=null)
    {
        if(empty($search) || !is_array($search)){
            $id = $this->params;
            $search = array(
                "id = :id:",
                'bind' => array('id' => $id['id'])
            );
        }
        $data = array('access_token' => $this->access_token);
        $response = json_decode($this->apiPreCheckAction($data));
        if(!empty($response->status) && $response->status == 'success'){
            $pages = new Pages;
            $campData = $pages->findFirst($search);
            if($campData != false){
                $camps = array(
                    'id' => $campData->id,
                    'pagetitle' => $campData->pagetitle,
                    'temp' => $campData->temp,
                    'url' => $campData->url,
                    'parent' => $campData->parent,
                    'type' => $campData->type
                );
                echo json_encode(array('status' => 'success', 'campaigns' => $camps));
            }
        }else{
            echo json_encode(array('status' => 'error', 'message' => $response->message));
        }
    }

    public function getCampModsAction($search=null)
    {
        if(empty($search) || !is_array($search)){
            $id = $this->params;
            $search = array(
                "pageid = :id:",
                'bind' => array('id' => $id['id'])
            );
        }
        $data = array('access_token' => $this->access_token);
        $response = json_decode($this->apiPreCheckAction($data));
        if(!empty($response->status) && $response->status == 'success'){
            $modules = new Modules;
            $campModData = $modules->find($search);
            if($campModData != false){
                $modules = array();
                foreach($campModData as $mod){
                    $modules[$mod->moduleid] = array(
                        'id' => $mod->id,
                        'type' => $mod->type,
                        'moduleid' => $mod->moduleid,
                        'pageid' => $mod->pageid,
                        'title' => $mod->title,
                        'data' => (@unserialize($mod->data)? unserialize($mod->data) : $mod->data)
                    );
                }
                //print_r($modules);
                echo json_encode(array('status' => 'success', 'modules' => $modules));
            }
        }else{
            echo json_encode(array('status' => 'error', 'message' => $response->message));
        }
    }

    public function saveCampsModsAction()
    {
        $data = array('access_token' => $this->access_token);
        $response = json_decode($this->apiPreCheckAction($data, 'write'));
        if(!empty($response->status) && $response->status == 'success'){
            $id = $this->params;
            $returnArray = array();
            foreach($this->submitData['modules'] as $modid => $mod):
                $values = array(
                    'moduleid' => strtolower($modid),
                    'type' => $mod['type'],
                    'pageid' => $id['id'],
                    'title' => $mod['title']
                );
                if(!empty($mod['data']) && is_array($mod['data'])){
                    $values['data'] = serialize($mod['data']);
                }else{
                    $values['data'] = $mod['data'];
                }
                if(!empty($mod['id'])){
                    $values['id'] = $mod['id'];
                }
                $modules = new Modules;
                $contCreate = $modules->update($values);
                if($contCreate == false){
                    foreach ($modules->getMessages() as $message) {
                        json_encode(array('status'=>'error', 'message' => $message));
                    }
                }else{
                    $recordId = $modules->id;
                    $returnArray[] = array('moduleId' => $recordId);
                }
            endforeach;
            echo json_encode(array('status'=>'success', $returnArray));
        }else{
            echo json_encode(array('status' => 'error', 'message' => $response->message));
        }
    }

    public function getTemplatesAction()
    {
        $data = array('access_token' => $this->access_token);
        $response = json_decode($this->apiPreCheckAction($data));
        if(!empty($response->status) && $response->status == 'success'){
            $temps = array();
            $results = scandir($this->tempPath);
            foreach ($results as $result) {
                if ($result === '.' or $result === '..') continue;
                if (is_dir($this->tempPath . '/' . $result)) {
                    $file = json_decode(file_get_contents($this->tempPath . $result."/template.json"));
                    $temps[] = array('name' => $file->name, 'value' => $this->tempPath . $result);
                }
            }
            echo json_encode(array('status' => 'success', 'templates' => $temps));
        }else{
            echo json_encode(array('status' => 'error', 'message' => $response->message));
        }
    }

    public function getTempDataAction()
    {
        $data = array('access_token' => $this->access_token);
        $response = json_decode($this->apiPreCheckAction($data));
        if(!empty($response->status) && $response->status == 'success'){
            $id = $this->params;
            $search = array(
                "id = :id:",
                'bind' => array('id' => $id['id'])
            );
            $pages = new Pages;
            $templateData = $pages->findFirst($search);
            if($templateData != false){
                $file = json_decode(file_get_contents($templateData->temp.'/template.json'));
                echo json_encode(array('status' => 'success', 'template_data' => $file));
            }else{
                echo json_encode(array('status' => 'error', 'message' => 'no campign data found'));
            }        
        }else{
            echo json_encode(array('status' => 'error', 'message' => $response->message));
        }
    }

    public function getPageAction()
    {
        $data = array('access_token' => $this->access_token);
        $response = json_decode($this->apiPreCheckAction($data));
        if(!empty($response->status) && $response->status == 'success'){
            $params = $this->params;
            ob_start();$this->getCampAction(array("url = :url:",'bind' => array('url' => $params['url'])));$camp = json_decode(ob_get_clean(), true);
            ob_start();$this->getCampModsAction(array("pageid = :id:",'bind' => array('id' => $camp['campaigns']['id']))); $modules = json_decode(ob_get_clean(), true);
            $file = json_decode(file_get_contents($camp['campaigns']['temp'].'/template.json'));
            $template = array('status' => 'success', 'template_data' => $file);
            //ob_start();$this->getTempDataAction($camp['campaigns']['temp']); $template = json_decode(ob_get_clean(), true);
            if($camp['status'] == 'success'){
                echo json_encode(array('status' => 'success', 'data' => array('campaign_data' => $camp['campaigns'], 'module_data' => $modules['modules'], 'template_data' => $template['template_data'])));
            }else{
                echo json_encode(array('status' => 'error', 'message' => 'campaign not found'));
            }
        }else{
            echo json_encode(array('status' => 'error', 'message' => $response->message));
        }     
    }

    public function jobSearchAction()
    {
        //print_r($_SERVER);
        //die();
        $data = array('access_token' => $this->access_token);
        $response = json_decode($this->apiPreCheckAction($data));
        if(!empty($response->status) && $response->status == 'success'){
            $search = new Search;
            //$search = json_encode($this->getParams);
            $searchResults = $search->searchExecute($this->getParams);
            if($searchResults['hits']['total'] > 0){
                echo json_encode(array('status' => 'success', 'total' => $searchResults['hits']['total'], 'results' => $searchResults['hits']['hits'], 'statement' => $searchResults['searchTerms']));
            }else{
                echo json_encode(array('status' => 'error', 'message' => 'no results found', 'statement' => $searchResults['searchTerms']));
            }
        }else{
            echo json_encode(array('status' => 'error', 'message' => $response->message));
        } 
    }

    public function jobSearchFieldsAction()
    {
        $data = array('access_token' => $this->access_token);
        $response = json_decode($this->apiPreCheckAction($data));
        if(!empty($response->status) && $response->status == 'success'){
            //include_once( APP_PATH . "app/files/companies.php");
            //include_once( APP_PATH . "app/files/countries.php");
            //include_once( APP_PATH . "app/files/categories.php");
            include_once( APP_PATH . "app/files/disciplines.php");
            //$companies = json_encode($company, true);
            //$countries = json_encode($country, true);
            $disciplines = json_encode($disc, true);
            //$categories = json_encode($category, true);
            //echo json_encode(array('status' => 'success', 'countries' => array($countries), 'companies' => array($companies), 'disciplines' => array($disciplines), 'categories' => array($categories)));
            echo json_encode(array('status' => 'success', 'disciplines' => array($disciplines)));
        }else{
            echo json_encode(array('status' => 'error', 'message' => $response->message));
        }  
    }

    public function getJobDataAction()
    {
        $data = array('access_token' => $this->access_token);
        $response = json_decode($this->apiPreCheckAction($data));
        if(!empty($response->status) && $response->status == 'success'){
            if($this->getParams['html']){
                include(APP_PATH . "app/files/jobs.php");
                $jobs = $array;
            }else{
                $jobs = json_decode(file_get_contents( APP_PATH . "app/files/jobs.json"), true);
            }
            $jobFound = false;
            foreach ($jobs as $jobKey => $job) { 
                if(!empty($job['value']['referencenumber']) && $job['value']['referencenumber'] == $this->params['id']){
                    $jobFound = $job['value'];
                    break;
                }
            }
            if($jobFound && is_array($jobFound)){
                $jobFound['description'] = rawurlencode($jobFound['description']);
                echo json_encode(array('status' => 'success', 'job' => $jobFound));
            }else{
                echo json_encode(array('status' => 'error', 'message' => 'no job found'));
            }
        }else{
            echo json_encode(array('status' => 'error', 'message' => $response->message));
        }         
    }

    public function getQuestionsAction(){
        $data = array('access_token' => $this->access_token);
        print_r($data);
        $response = json_decode($this->apiPreCheckAction($data));
        if(!empty($response->status) && $response->status == 'success'){
            $questions = new Questions;
            $questionsData = $questions->find();
            if($questionsData != false){
                $qs = array();
                foreach($questionsData as $q){
                    $qs[$q->id] = array(
                        'id' => $q->id,
                        'query' => (@unserialize($q->condition)?unserialize($q->condition):false),
                        'conditional' => (!empty($q->conditional)? true:false),
                        'answers' => (@unserialize($q->answers)?unserialize($q->answers):array()),
                        'freetext' => (@unserialize($q->freetext)?unserialize($q->freetext):false),
                        'irecid' => $q->irecid,
                        'question' => $q->question
                    );
                }
                echo json_encode(array('status' => 'success', 'questions' => $qs));
            }
        }else{
            echo json_encode(array('status' => 'error', 'message' => $response->message));
        }      
    }

    public function removeQuestionAction(){
        $data = array('access_token' => $this->access_token);
        $response = json_decode($this->apiPreCheckAction($data, 'write'));
        if(!empty($response->status) && $response->status == 'success'){
            $questions = new Questions;
            $values = array(
                        'id' => $this->getParams['qid']
                    );
            $questionData = $questions->findFirst($values);
            if($questionData != false){
                $delete = $questionData->delete();
                if($delete != false){
                    echo json_encode(array('status' => 'success', 'question' => $this->getParams['qid']));
                }else{
                    foreach ($questions->getMessages() as $message) {
                        echo json_encode(array('status'=>'error', 'message' => $message));
                    }
                }
            }else{
                foreach ($questions->getMessages() as $message) {
                    echo json_encode(array('status'=>'error', 'message' => $message));
                } 
            }
        }else{
            echo json_encode(array('status' => 'error', 'message' => $response->message));
        }      
    }

    public function createQuestionAction(){
        $data = array('access_token' => $this->submitData['access_token']);
        $response = json_decode($this->apiPreCheckAction($data, 'write'));
        if(!empty($response->status) && $response->status == 'success'){
            $questions = new Questions;
            if(empty($this->submitData['question']['query'])){ $condition = false; }else{ $condition = serialize($this->submitData['question']['query']); }
            $values = array(
                        'question' => $this->submitData['question']['question'],
                        'condition' => $condition,
                        'answers' => (!empty($this->submitData['question']['answers'])? serialize($this->submitData['question']['answers']):false),
                        'conditional' => $this->submitData['question']['conditional'],
                        'irecid' => $this->submitData['question']['irecid'],
                        'freetext' => (!empty($this->submitData['question']['freetext'])? serialize($this->submitData['question']['freetext']):false),
                    );
            $qCreate = $questions->create($values);
            if($qCreate == false){
                foreach ($questions->getMessages() as $message) {
                    echo json_encode(array('status'=>'error', 'message' => $message));
                }
            }else{
                $recordId = $questions->id;
                echo json_encode(array('status'=>'success', 'questionId' => $recordId));
            }
        }else{
            echo json_encode(array('status' => 'error', 'message' => $response->message));
        }
    }

    public function editQuestionAction(){
        $data = array('access_token' => $this->submitData['access_token']);
        $response = json_decode($this->apiPreCheckAction($data, 'write'));
        if(!empty($response->status) && $response->status == 'success'){
            $questions = new Questions;
            if(empty($this->submitData['question']['query'])){ $condition = false; }else{ $condition = serialize($this->submitData['question']['query']); }
            $values = array(
                        'id' => $this->submitData['question']['id'],
                        'question' => $this->submitData['question']['question'],
                        'condition' => $condition,
                        'answers' => (!empty($this->submitData['question']['answers'])? serialize($this->submitData['question']['answers']):false),
                        'conditional' => $this->submitData['question']['conditional'],
                        'irecid' => $this->submitData['question']['irecid'],
                        'freetext' => (!empty($this->submitData['question']['freetext'])? serialize($this->submitData['question']['freetext']):false),
                    );
            $qUpdate = $questions->update($values);
            if($qUpdate == false){
                foreach ($questions->getMessages() as $message) {
                    echo json_encode(array('status'=>'error', 'message' => $message));
                }
            }else{
                $recordId = $questions->id;
                echo json_encode(array('status'=>'success', 'questionId' => $recordId));
            }
        }else{
            echo json_encode(array('status' => 'error', 'message' => $response->message));
        }
    }  

    public function generateCVPDFAction(){


        $data = array('access_token' => $this->access_token);
        $response = json_decode($this->apiPreCheckAction($data));
        if(!empty($response->status) && $response->status == 'success'){
            if(!empty($this->submitData['profile'])){
                $linkedIn = $this->submitData['profile'];
                ob_start();
                include_once(APP_PATH . 'app/views/cv/ab_template.phtml');
                $template = ob_get_contents(); 
                ob_end_clean(); 
                $pdf = new Pdf;
                $pdf->addPage($template);
                $pdf->saveAs('/tmp/cv-'.$this->submitData['profile']['id'].'.pdf');

                if(!empty($this->submitData['track']) && $this->submitData['track'] != 'false'){
                    $this->getDI()->getMail()->send(array(
                        'linkedin.woodgroup@resumemirror.net' => 'Resume Mirror'
                    ), $this->submitData['irc'].' with bid id', array(
                        'attach' => array('path' => '/tmp/cv-'.$this->submitData['profile']['id'].'.pdf', 'attachName' => 'CV.pdf')
                    ));
                }else{
                    $this->getDI()->getMail()->send(array(
                        'careers.woodgroup@resumemirror.net' => 'Resume Mirror'
                    ), $this->submitData['irc'], array(
                        'attach' => array('path' => '/tmp/cv-'.$this->submitData['profile']['id'].'.pdf', 'attachName' => 'CV.pdf')
                    ));
                }

                $this->getDI()->getMail()->send(array(
                    $linkedIn['emailAddress'] => $linkedIn['firstName'].' '.$linkedIn['lastName']
                ), 'You have successfully applied for position '.$this->submitData['irc'], array(
                    'attach' => array('path' => '/tmp/cv-'.$this->submitData['profile']['id'].'.pdf', 'attachName' => 'CV.pdf'),
                    'template' => 'jobApply',
                    'data' =>  array('irc' => $this->submitData['irc'], 'name' => $linkedIn['firstName'].' '.$linkedIn['lastName'])
                ));

                echo json_encode(array('status' => 'success', 'message' => 'application sucessfully' ));
            }else{
                echo json_encode(array('status' => 'error', 'message' => 'there was an error submitting your application, please try again later.' )); 
            }
        }else{
            echo json_encode(array('status' => 'error', 'message' => 'there was an error submitting your application, please try again later.' )); 
        }
    }

    public function submitAdditionalQsAction()
    {
        if(empty($_SERVER['HTTP_REFERER'])) die(json_encode(array('status' => 'error', 'message' => 'access token error' )));
        $_POST['redirect_uri'] = 'https://'.parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST).'/';
        $data = array('access_token' => $this->submitData['access_token']);
        $response = json_decode($this->apiPreCheckAction($data));
        if(!empty($response->status) && $response->status == 'success'){
            if(!empty($this->submitData['answers']['question'])){
                $stamp = date('d-m-Y-h', time());
                foreach($this->submitData['answers']['question'] as $keys => $answers){
                    $csv = "";
                    $title = "";
                    $csvBody = "";

                    $title .= $keys." | ";
                    $csvBody .= $answers." | ";

                    $title = rtrim($title, " | ");
                    $csvBody = rtrim($csvBody, " | ");
                    $title .= "\n";
                    $csv .= $csvBody."\n";

                    if(!file_exists(APP_PATH."public/files/csvfile-".$stamp.".csv")){
                        touch(APP_PATH."public/files/csvfile-".$stamp.".csv");
                        $csv_handler = fopen (APP_PATH."public/files/csvfile-".$stamp.".csv",'w');
                        fwrite ($csv_handler,$title);
                        fclose ($csv_handler);
                    }

                    $csv_handler = fopen (APP_PATH."public/files/csvfile-".$stamp.".csv",'a');
                    fwrite ($csv_handler,$csv);
                    fclose ($csv_handler);
                }
            }else{
               echo json_encode(array('status' => 'error', 'message' => 'No questions have been answered' )); 
            }
        }else{
             echo json_encode(array('status' => 'error', 'message' => 'access token error' ));
        }
    }

    public function uploadCVAction()
    {
        $data = array('access_token' => $this->access_token);
        if(empty($data['access_token'])){
            if(!empty($_COOKIE['publicAccessToken'])){
                $data = array('access_token' => $_COOKIE['publicAccessToken']);
            }
        }
        $response = json_decode($this->apiPreCheckAction($data));
        if(!empty($response->status) && $response->status == 'success'){
            $name = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,5 ) ,1 ) .substr( md5( time() ), 1);
            $info = pathinfo($_FILES['file']['name']);
            $ext = $info['extension']; // get the extension of the file
            $newname = $name.'.'.$ext; 

            $target = '/tmp/'.$newname;
            move_uploaded_file( $_FILES['file']['tmp_name'], $target);

            if(!empty($_POST['track']) && $_POST['track'] != 'false'){
                $this->getDI()->getMail()->send(array(
                    'linkedin.woodgroup@resumemirror.net' => 'Resume Mirror'
                ), $_POST['irc'].' with bid id', array(
                    'attach' => array('path' => $target, 'attachName' => $newname)
                ));
            }else{
                $this->getDI()->getMail()->send(array(
                    'careers.woodgroup@resumemirror.net' => 'Resume Mirror'
                ), $_POST['irc'], array(
                    'attach' => array('path' => $target, 'attachName' => $newname)
                ));
            }

            $this->getDI()->getMail()->send(array(
                $_POST['email'] => $_POST['email']
            ), 'You have successfully applied for position '.$_POST['irc'], array(
                'attach' => array('path' => $target, 'attachName' => $newname),
                'template' => 'jobApply',
                'data' => array('irc' => $_POST['irc'], 'name' => $_POST['email'])
            ));

            echo json_encode(array('status' => 'success', 'message' => 'application sucessfully' ));

        }else{
            echo json_encode(array('status' => 'error', 'message' => 'access token error' ));
        }
    }

    public function resetPasswordAction()
    {

        /*$di = $this->getDI();

        $user = new Users;

        $users = $user->findFirst('username = "'.$this->submitData->email.'"');

        if($users == false){
            $this->flash->error('Could not find a user with the submitted email address');
            $this->flash->output();
            $response_array['flash'] = ob_get_contents(); ob_end_clean();
            $response_array['status'] = 'error'; 
        }else{

            $email = $di->getMail()->send(array(
                $this->submitData->email => $users->name
            ), 'Request to Reset Altablue Job Board Password', 'passReset', array(
                'name'=>$users->name, 'site_url' => $this->siteUrl, 'reset' => 'https://'.$this->siteUrl.'/admin/password-reset/'.$this->security->hash(rtrim(md5(microtime()),"="));,
            ));

            if(empty($email)){
                $this->flash->success('Password reset email sent');
                $this->flash->output();
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
                $response_array['status'] = 'success'; 
            }else{
                $this->flash->error('Could not send a password reset email');
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
                $response_array['status'] = 'error';
            }

        }

        header('Content-type: application/json');
        echo json_encode($response_array);*/

    }

}
