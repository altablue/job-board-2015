<?php

use Phalcon\Mvc\Controller;
use Phalcon\Filter;
use Phalcon\Http\Request;
use Phalcon\DI\InjectionAwareInterface;
use Phalcon\Http\Response\Cookies;

class IndexController extends ControllerBase
{

	public function initialize()
    {        
        parent::initialize();
    }

    public function indexAction()
    {
    	$this->tag->appendTitle(' Altablue Job Board Page Title');
    }

    public function testAction()
    {
    	$this->tag->appendTitle(' test Altablue Job Board Page Title');
    }

}

